APP_PLATFORM := android-34

ifeq ($(ENABLE_DEBUG), 1)
    APP_OPTIM := debug
    APP_CFLAGS := -g3 -O0
    APP_CPPFLAGS := -g3 -O0
    APP_CXXFLAGS := -g3 -O0
else
    APP_OPTIM := release
    APP_CFLAGS := -g3 -O0 -DNDEBUG -fvisibility=hidden
    APP_CPPFLAGS := -g3 -O0 -DNDEBUG -fvisibility=hidden
    APP_CXXFLAGS := -g3 -O0 -DNDEBUG -fvisibility=hidden
endif

APP_STL := c++_shared
APP_CFLAGS += -DUSE_PKCS1_PSS
APP_CPPFLAGS += -DUSE_PKCS1_PSS -DSTDC_HEADERS -frtti -std=c++11 -fexceptions
APP_CXXFLAGS += -DUSE_PKCS1_PSS -DSTDC_HEADERS -frtti -std=c++11 -fexceptions