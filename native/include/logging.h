#ifndef NATIVE_BRIDGE_LOGGING_H
#define NATIVE_BRIDGE_LOGGING_H


#include <android/log.h>


#define LogDebug(...) __android_log_print(ANDROID_LOG_DEBUG, "NATIVE_BRIDGE", __VA_ARGS__)
#define LogError(...) __android_log_print(ANDROID_LOG_ERROR, "NATIVE_BRIDGE", __VA_ARGS__)
#define LogInfo(...) __android_log_print(ANDROID_LOG_INFO, "NATIVE_BRIDGE", __VA_ARGS__)
#define LogVerbose(...) __android_log_print(ANDROID_LOG_VERBOSE, "NATIVE_BRIDGE", __VA_ARGS__)
#define LogWarning(...) __android_log_print(ANDROID_LOG_WARN, "NATIVE_BRIDGE", __VA_ARGS__)


#endif //NATIVE_BRIDGE_LOGGING_H