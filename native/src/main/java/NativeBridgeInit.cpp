#include <jni.h>


extern int nativeBridge_registerNatives(JNIEnv *env);


extern "C"
JNIEXPORT jint JNI_OnLoad(JavaVM *vm,
                          void   *reserved)
{
    JNIEnv *env;

    if (vm->GetEnv(reinterpret_cast<void **> (&env), JNI_VERSION_1_6) != JNI_OK)
    {
        return JNI_ERR;
    }

    int rc = nativeBridge_registerNatives(env);

    if (rc != JNI_OK)
    {
        return rc;
    }

    return JNI_VERSION_1_6;
}