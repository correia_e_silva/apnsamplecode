#include <jni.h>
#include <ctime>
#include "logging.h"


#define VALIDITY 1L * 365L * 24L * 60L * 60L//seconds


extern "C" jint
Java_common_nativeBridge_NativeBridge_nativeInitialize(JNIEnv  *env,
                                                       jobject clazz)
{

    LogVerbose("%s Entering", __func__);
    LogVerbose("%s Exiting", __func__);

    return -1;
}


static void throwRuntimeException(JNIEnv     *env,
                                  const char *message)
{
    jclass clazz = env->FindClass("java/lang/RuntimeException");

    if (clazz == nullptr)
    {
        LogError("Could not find 'java/lang/RuntimeException' class!");
        return;
    }

    env->ThrowNew(clazz, message);
}


static void dumpTime(char   *tag,
                     time_t time)
{

    LogVerbose("%s local time: %s", tag, ctime(&time));

    struct tm *timeCalendarTime = localtime(&time);

    int timeDay = timeCalendarTime->tm_mday;
    int timeMonth = timeCalendarTime->tm_mon + 1;
    int timeYear = timeCalendarTime->tm_year + 1900;
    int timeHours = timeCalendarTime->tm_hour;
    int timeMinutes = timeCalendarTime->tm_min;
    int timeSeconds = timeCalendarTime->tm_sec;

    LogVerbose("%s date/time: %02d/%02d/%d %02d:%02d:%02d", tag, timeDay, timeMonth, timeYear, timeHours, timeMinutes, timeSeconds);
}


static time_t getTimestampTime(JNIEnv *env,
                               char   *buildConfigFieldClass)
{
    jclass clazz = env->FindClass(buildConfigFieldClass);

    if (clazz == nullptr)
    {
        throwRuntimeException(env, "Could not find 'everyPurposeNotes/noteScreens/BuildConfig' class!");
    }

    jfieldID fieldId = env->GetStaticFieldID(clazz, "TIMESTAMP", "J");

    if (fieldId == nullptr)
    {
        throwRuntimeException(env, "Could not find 'TIMESTAMP' field");
    }

    auto timeStampTime = (time_t) (env->GetStaticLongField(clazz, fieldId) / 1000);

    dumpTime((char *) "Timestamp", timeStampTime);

    return timeStampTime;
}


static time_t getExpirationTime(time_t timestampTime)
{
    time_t expirationTime = timestampTime + ((time_t) VALIDITY);

    dumpTime((char *) "Expiration", expirationTime);

    return expirationTime;
}


static time_t getCurrentTime()
{
    time_t currentTime = time(nullptr);

    dumpTime((char *) "Current", currentTime);

    return currentTime;
}


extern "C" void
Java_common_nativeBridge_NativeBridge_nativeTimeBomb(JNIEnv  *env,
                                                     jobject clazz,
                                                     jstring buildConfigFieldClass)
{

    LogVerbose("%s Entering", __func__);

    auto *nativeBuildConfigFieldClass = (char *) env->GetStringUTFChars(buildConfigFieldClass, nullptr);

    if (!nativeBuildConfigFieldClass)
    {
        throwRuntimeException(env, "'buildConfigFieldClass' is NULL!");
    }

    time_t timestampTime = getTimestampTime(env, nativeBuildConfigFieldClass);
    time_t expirationTime = getExpirationTime(timestampTime);
    time_t currentTime = getCurrentTime();
    time_t remainingTime = difftime(expirationTime, currentTime);

    LogVerbose("Remaining time: %ld days", remainingTime / (60L * 60L * 24L));

    if (remainingTime <= 0)
    {
        throwRuntimeException(env, "Application has expired, please update to the latest version!");
    }

    env->ReleaseStringUTFChars(buildConfigFieldClass, (char *) nativeBuildConfigFieldClass);

    LogVerbose("%s Exiting", __func__);
}


static JNINativeMethod clsMethods[] =
{
        {"nativeInitialize", "()I", (void *) &Java_common_nativeBridge_NativeBridge_nativeInitialize},
        {"nativeTimeBomb", "(Ljava/lang/String;)V", (void *) &Java_common_nativeBridge_NativeBridge_nativeTimeBomb}
};


static int registerMethods(JNIEnv *env)
{
    jclass cls = env->FindClass("common/nativeBridge/NativeBridge");

    if (cls == nullptr)
    {
        return JNI_ERR;
    }

    return env->RegisterNatives(cls, clsMethods, sizeof(clsMethods) / sizeof(clsMethods[0]));
}


extern int nativeBridge_registerNatives(JNIEnv *env)
{

    return registerMethods(env);
}