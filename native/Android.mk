LOCAL_PATH := $(call my-dir)


include $(CLEAR_VARS)


NATIVE_BRIDGE_LIB_DIR := $(LOCAL_PATH)


##########################################################
##### Native bridge source files, flags and includes #####
##########################################################

NATIVE_BRIDGE_C_INCLUDES := \
    $(NATIVE_BRIDGE_LIB_DIR)/include \


NATIVE_BRIDGE_SRCS := \
    $(NATIVE_BRIDGE_LIB_DIR)/src/main/java/NativeBridge.cpp \
    $(NATIVE_BRIDGE_LIB_DIR)/src/main/java/NativeBridgeInit.cpp


NATIVE_BRIDGE_CFLAGS := -DPOSIX -Wno-deprecated-declarations


##########################################################
############## Native bridge shared library ##############
##########################################################

include $(CLEAR_VARS)


LOCAL_MODULE := native_bridge_jni
LOCAL_MODULE_FILENAME := libnative_bridge_jni


LOCAL_ARM_MODE := arm


LOCAL_C_INCLUDES := $(NATIVE_BRIDGE_C_INCLUDES)


LOCAL_SRC_FILES := $(NATIVE_BRIDGE_SRCS)


LOCAL_CFLAGS := $(NATIVE_BRIDGE_CFLAGS)


LOCAL_CXXFLAGS := $(NATIVE_BRIDGE_CFLAGS)


LOCAL_LDLIBS := -fuse-ld=gold -llog -ldl -lz -latomic


include $(BUILD_SHARED_LIBRARY)