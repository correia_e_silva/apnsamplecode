Android Java/Kotlin/native layer sample code
============================================

Some Android Java/Kotlin/native layer sample code, taken from the business version of my *AllPurposeNotes* Android app:

Common Java/Kotlin layer modules that can also be used in other applications:
----------------------------------------------------------------------------
* app/src/main/java/common/commonAdapters/ - *GridView* UI/data handling
* app/src/main/java/common/commonUtilities/ - Utilities
* app/src/main/java/common/cryptography/ - AES/RSA/ECDSA data encryption/decryption/signing handling
* app/src/main/java/common/fragments/ - Image gallery using *Kotlin flows*
* app/src/main/java/common/launcher/ - Application launcher with PIN/biometric authentication handling using *abstract class*
* app/src/main/java/common/messaging/ - HTTP/Google Firebase/SignalR message handling using *singleton*, *interface* and optional *callbacks*
* app/src/main/java/common/nativeBridge/ - Application time-bomb Java/native layer bridge
* app/src/main/java/common/webView/ - Client authentication using *abstract class* and *WebView*
* app/src/main/java/common/zipDataHandler/ - Asynchronous file compression/decompression using *interface*, *ExecutorService*, *Future*, *Callable* and optional *callbacks*

Application specific Java/Kotlin layer modules:
----------------------------------------------
* app/src/main/java/everyPurposeNotes/messaging/ - Client/server message handling using *singleton*, *builder* and *generic types*
* app/src/main/java/everyPurposeNotesBusiness/noteScreens/ - Application UI using *abstract class*, *GridView* and *RxJava*
* app/src/main/java/internal/cloudManager/ - Asynchronous cloud data management using *singleton*, *interface*, *abstract class*, *ExecutorService*, *Future*, *Callable*, *Runnable* and optional *callbacks*

Application specific native layer modules:
-----------------------------------------
* native/src/main/java - Application time-bomb Java/native layer bridge