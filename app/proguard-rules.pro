-dontwarn com.squareup.okhttp.Callback
-dontwarn com.squareup.okhttp.RequestBody
-dontwarn java.lang.ClassValue
-dontwarn okhttp3.RequestBody
-dontwarn okhttp3.Callback
-dontwarn okio.ForwardingSink

-keep class everyPurposeNotes.note.Note
{
    public java.lang.String getSubject();
}

-keep class everyPurposeNotesBusiness.noteScreens.BuildConfig
{
    <fields>;
}

-keepnames class * implements java.io.Serializable

-keepclassmembers class * implements java.io.Serializable
{
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    !static !transient <fields>;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

-keepclassmembers class * extends java.lang.Enum
{
    <fields>;
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keepclasseswithmembernames,includedescriptorclasses class common.nativeBridge.NativeBridge
{
    native <methods>;
}

-keepattributes LineNumberTable,SourceFile

-renamesourcefileattribute SourceFile