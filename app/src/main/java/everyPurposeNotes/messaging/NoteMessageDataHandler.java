package everyPurposeNotes.messaging;


import android.content.Context;
import androidx.core.util.Pair;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONObject;
import java.lang.reflect.Type;
import java.util.AbstractMap;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;


import common.CommonConstants;
import common.commonUtilities.CommonUtilities;
import everyPurposeNotesBusiness.noteScreens.R;


public class NoteMessageDataHandler implements CommonConstants
{
    public enum DATA_ACTION
    {
        CREATE("CREATE"),
        DELETE("DELETE"),
        LIST_FILES("LIST_FILES"),
        LOGIN("LOGIN"),
        LOGOUT("LOGOUT");


        private String label;


        DATA_ACTION(String label)
        {

            this.label = label;
        }
        
        
        public static DATA_ACTION fromLabel(String label)
        {
            
            for (DATA_ACTION dataAction : DATA_ACTION.values())
            {
                if (dataAction.label.equals(label))
                {
                    return dataAction;
                }
            }
            
            return null;
        }
    }

    public enum DATA_ENTITY
    {
        NOTE("NOTE"),
        NOTES_CATS("NOTES_CATS"),
        NOTES_FLDS("NOTES_FLDS");


        private String label;


        DATA_ENTITY(String label)
        {

            this.label = label;
        }


        public static DATA_ENTITY fromLabel(String label)
        {

            for (DATA_ENTITY dataEntity : DATA_ENTITY.values())
            {
                if (dataEntity.label.equals(label))
                {
                    return dataEntity;
                }
            }

            return null;
        }
    }

    public enum DATA_RESULT
    {
        ACK_MESSAGE_VALID("ACK_MESSAGE_VALID"),
        ACK_MESSAGE_INVALID("ACK_MESSAGE_INVALID"),
        APN_FAILURE_ERROR_CREATING_NOTE("APN_FAILURE_ERROR_CREATING_NOTE"),
        APN_FAILURE_ERROR_CREATING_NOTES_CATS("APN_FAILURE_ERROR_CREATING_NOTES_CATS"),
        APN_FAILURE_ERROR_CREATING_NOTES_FLDS("APN_FAILURE_ERROR_CREATING_NOTES_FLDS"),
        APN_FAILURE_ERROR_DELETING_NOTE("APN_FAILURE_ERROR_DELETING_NOTE"),
        APN_FAILURE_ERROR_DELETING_NOTES_CATS("APN_FAILURE_ERROR_DELETING_NOTES_CATS"),
        APN_FAILURE_ERROR_DELETING_NOTES_FLDS("APN_FAILURE_ERROR_DELETING_NOTES_FLDS"),
        APN_FAILURE_NOT_LOGGED_IN("APN_FAILURE_NOT_LOGGED_IN"),
        APN_FAILURE_NOTE_NOT_FOUND("APN_FAILURE_NOTE_NOT_FOUND"),
        APN_FILES_LIST("APN_FILES_LIST"),
        APN_SUCCESS("APN_SUCCESS");


        private String label;


        DATA_RESULT(String label)
        {

            this.label = label;
        }


        public static DATA_RESULT fromLabel(String label)
        {

            for (DATA_RESULT dataResult : DATA_RESULT.values())
            {
                if (dataResult.label.equals(label))
                {
                    return dataResult;
                }
            }

            return null;
        }
    }


    public static class NoteMessageData
    {

        public long        mTimeStamp;
        public DATA_ACTION mDataAction;
        public String      mInfo;


        private NoteMessageData(long        timeStamp,
                                DATA_ACTION dataAction,
                                String      info)
        {

            mTimeStamp = timeStamp;
            mDataAction = dataAction;
            mInfo = info;
        }
    }


    private static final int              MAX_RECORDS = 100;

    private static final Gson             MAP_GSON = new Gson();
    private static final Type             MAP_TYPE = new TypeToken<Map<String, NoteMessageData>>(){}.getType();


    private static NoteMessageDataHandler mNoteMessageDataHandler = null;
    private Context                       mContext;
    private volatile NoteMessageData      mAuthenticationData;
    private Map<String, NoteMessageData>  mUploadDataNotProcessed, mUploadDataProcessed, mDownloadDataNotProcessed, mDownloadDataProcessed;


    private void loadData()
    {
        String data;

        if (!(data = CommonUtilities.getPreference(mContext, NoteBaseMessage.UPLOAD_DATA_NOT_PROCESSED_TAG)).isEmpty())
        {
            mUploadDataNotProcessed.putAll((Map<String, NoteMessageData>) MAP_GSON.fromJson(data, MAP_TYPE));
        }

        if (!(data = CommonUtilities.getPreference(mContext, NoteBaseMessage.UPLOAD_DATA_PROCESSED_TAG)).isEmpty())
        {
            mUploadDataProcessed.putAll((Map<String, NoteMessageData>) MAP_GSON.fromJson(data, MAP_TYPE));
        }

        if (!(data = CommonUtilities.getPreference(mContext, NoteBaseMessage.DOWNLOAD_DATA_NOT_PROCESSED_TAG)).isEmpty())
        {
            mDownloadDataNotProcessed.putAll((Map<String, NoteMessageData>) MAP_GSON.fromJson(data, MAP_TYPE));
        }

        if (!(data = CommonUtilities.getPreference(mContext, NoteBaseMessage.DOWNLOAD_DATA_PROCESSED_TAG)).isEmpty())
        {
            mDownloadDataProcessed.putAll((Map<String, NoteMessageData>) MAP_GSON.fromJson(data, MAP_TYPE));
        }
    }


    private NoteMessageDataHandler(Context context)
    {

        mContext = context;

        mAuthenticationData = new NoteMessageData(-1, null, null);

        mUploadDataNotProcessed = Collections.synchronizedMap(new LinkedHashMap<String, NoteMessageData>()
        {


            @Override
            protected boolean removeEldestEntry(Entry<String, NoteMessageData> eldest)
            {

                return size() > MAX_RECORDS;
            }
        });

        mUploadDataProcessed = Collections.synchronizedMap(new LinkedHashMap<String, NoteMessageData>()
        {


            @Override
            protected boolean removeEldestEntry(Entry<String, NoteMessageData> eldest)
            {

                return size() > MAX_RECORDS;
            }
        });

        mDownloadDataNotProcessed = Collections.synchronizedMap(new LinkedHashMap<String, NoteMessageData>()
        {


            @Override
            protected boolean removeEldestEntry(Entry<String, NoteMessageData> eldest)
            {

                return size() > MAX_RECORDS;
            }
        });

        mDownloadDataProcessed = Collections.synchronizedMap(new LinkedHashMap<String, NoteMessageData>()
        {


            @Override
            protected boolean removeEldestEntry(Entry<String, NoteMessageData> eldest)
            {

                return size() > MAX_RECORDS;
            }
        });

        loadData();
    }


    public static synchronized NoteMessageDataHandler getInstance(Context context)
    {

        if (mNoteMessageDataHandler == null)
        {
            mNoteMessageDataHandler = new NoteMessageDataHandler(context);
        }

        return mNoteMessageDataHandler;
    }


    public void addDataToSend(String      id,
                              long        timeStamp,
                              DATA_ACTION dataAction,
                              String      info)
    {
        NoteMessageData noteMessageData;

        noteMessageData = mUploadDataNotProcessed.get(id);

        if (!mUploadDataProcessed.containsKey(id) &&
            !mDownloadDataProcessed.containsKey(id) &&
            (noteMessageData != null) &&
            (noteMessageData.mTimeStamp < timeStamp))
        {
            if ((dataAction == DATA_ACTION.DELETE) &&
                (noteMessageData.mDataAction == DATA_ACTION.CREATE))
            {
                //A not processed client create request with the same key and a lower timestamp is already stored
                mUploadDataNotProcessed.remove(id);
                CommonUtilities.setPreference(mContext, NoteBaseMessage.UPLOAD_DATA_NOT_PROCESSED_TAG, MAP_GSON.toJson(mUploadDataNotProcessed, MAP_TYPE));
                return;
            }
        }

        mUploadDataNotProcessed.put(id, new NoteMessageData(timeStamp, dataAction, info));

        CommonUtilities.setPreference(mContext, NoteBaseMessage.UPLOAD_DATA_PROCESSED_TAG, MAP_GSON.toJson(mUploadDataProcessed, MAP_TYPE));
        CommonUtilities.setPreference(mContext, NoteBaseMessage.UPLOAD_DATA_NOT_PROCESSED_TAG, MAP_GSON.toJson(mUploadDataNotProcessed, MAP_TYPE));
    }


    public void removeDataToSend(String id)
    {

        mUploadDataProcessed.put(id, mUploadDataNotProcessed.remove(id));

        CommonUtilities.setPreference(mContext, NoteBaseMessage.UPLOAD_DATA_PROCESSED_TAG, MAP_GSON.toJson(mUploadDataProcessed, MAP_TYPE));
        CommonUtilities.setPreference(mContext, NoteBaseMessage.UPLOAD_DATA_NOT_PROCESSED_TAG, MAP_GSON.toJson(mUploadDataNotProcessed, MAP_TYPE));
    }


    public Map<String, NoteMessageData> getDataToSend()
    {

        return mUploadDataNotProcessed;
    }


    public void addDataToReceive(String      id,
                                 long        timeStamp,
                                 DATA_ACTION dataAction,
                                 String      info)
    {
        NoteMessageData noteMessageData;

        noteMessageData = mUploadDataProcessed.get(id);

        if ((noteMessageData != null) &&
            (noteMessageData.mTimeStamp > timeStamp))
        {
            //A processed client request with the same key and a with higher timestamp is already stored
            return;
        }

        noteMessageData = mUploadDataNotProcessed.get(id);

        if ((noteMessageData != null) &&
            (noteMessageData.mTimeStamp < timeStamp))
        {
            if (noteMessageData.mDataAction == DATA_ACTION.DELETE)
            {
                //A not processed client delete request with the same key and a lower timestamp is already stored
                mUploadDataNotProcessed.remove(id);
                CommonUtilities.setPreference(mContext, NoteBaseMessage.UPLOAD_DATA_NOT_PROCESSED_TAG, MAP_GSON.toJson(mUploadDataNotProcessed, MAP_TYPE));
            }
            if (dataAction == DATA_ACTION.DELETE)
            {
                //Discard redundant server delete request
                return;
            }
        }

        noteMessageData = mDownloadDataProcessed.get(id);

        if ((noteMessageData != null) &&
            (noteMessageData.mTimeStamp > timeStamp))
        {
            //A processed server request with the same key and a with higher timestamp is already stored
            return;
        }

        noteMessageData = mDownloadDataNotProcessed.get(id);

        if ((noteMessageData != null) &&
            (noteMessageData.mTimeStamp > timeStamp))
        {
            if ((dataAction == DATA_ACTION.CREATE) &&
                (noteMessageData.mDataAction == DATA_ACTION.DELETE))
            {
                //A not processed server delete request with the same key and a higher timestamp is already stored
                mDownloadDataNotProcessed.remove(id);
                CommonUtilities.setPreference(mContext, NoteBaseMessage.DOWNLOAD_DATA_NOT_PROCESSED_TAG, MAP_GSON.toJson(mDownloadDataNotProcessed, MAP_TYPE));
            }
            return;
        }

        if ((noteMessageData != null) &&
            (noteMessageData.mTimeStamp < timeStamp))
        {
            if ((dataAction == DATA_ACTION.DELETE) &&
                (noteMessageData.mDataAction == DATA_ACTION.CREATE))
            {
                //A not processed server create request with the same key and a lower timestamp is already stored
                mDownloadDataNotProcessed.remove(id);
                CommonUtilities.setPreference(mContext, NoteBaseMessage.DOWNLOAD_DATA_NOT_PROCESSED_TAG, MAP_GSON.toJson(mDownloadDataNotProcessed, MAP_TYPE));
            }
            return;
        }

        mDownloadDataNotProcessed.put(id, new NoteMessageData(timeStamp, dataAction, info));

        CommonUtilities.setPreference(mContext, NoteBaseMessage.DOWNLOAD_DATA_PROCESSED_TAG, MAP_GSON.toJson(mDownloadDataProcessed, MAP_TYPE));
        CommonUtilities.setPreference(mContext, NoteBaseMessage.DOWNLOAD_DATA_NOT_PROCESSED_TAG, MAP_GSON.toJson(mDownloadDataNotProcessed, MAP_TYPE));
    }


    public void removeDataToReceive(String id)
    {

        mDownloadDataProcessed.put(id, mDownloadDataNotProcessed.remove(id));

        CommonUtilities.setPreference(mContext, NoteBaseMessage.DOWNLOAD_DATA_PROCESSED_TAG, MAP_GSON.toJson(mDownloadDataProcessed, MAP_TYPE));
        CommonUtilities.setPreference(mContext, NoteBaseMessage.DOWNLOAD_DATA_NOT_PROCESSED_TAG, MAP_GSON.toJson(mDownloadDataNotProcessed, MAP_TYPE));
    }


    public Map<String, NoteMessageData> getDataToReceive()
    {

        return mDownloadDataNotProcessed;
    }


    private DATA_ACTION handleRequestMessage(String action,
                                             String noteCreationDate,
                                             String entity,
                                             String files,
                                             String noteSubject,
                                             long   timeStamp)
    {

        if ((action != null) &&
            (entity != null) &&
            (noteCreationDate != null) &&
            (noteSubject != null) &&
            (timeStamp > 0))
        {
            if ((DATA_ACTION.fromLabel(action) == DATA_ACTION.CREATE) ||
                (DATA_ACTION.fromLabel(action) == DATA_ACTION.DELETE))
            {
                if (DATA_ENTITY.fromLabel(entity) == DATA_ENTITY.NOTE)
                {
                    if (!CommonUtilities.cnvDbDateToDisplayFormat(noteCreationDate).isEmpty())
                    {
                        if (!noteSubject.isEmpty())
                        {
                            return DATA_ACTION.fromLabel(action);
                        }
                    }
                }
            }
        }

        if ((action != null) &&
            (entity != null) &&
            (timeStamp > 0))
        {
            if ((DATA_ACTION.fromLabel(action) == DATA_ACTION.CREATE) ||
                (DATA_ACTION.fromLabel(action) == DATA_ACTION.DELETE))
            {
                if ((DATA_ENTITY.fromLabel(entity) == DATA_ENTITY.NOTES_CATS) ||
                    (DATA_ENTITY.fromLabel(entity) == DATA_ENTITY.NOTES_FLDS))
                {
                    return DATA_ACTION.fromLabel(action);
                }
            }
        }

        return null;
    }


    private DATA_RESULT handleReplyMessage(String result,
                                           String authenticationUrl,
                                           long   sourceTimeStamp,
                                           long   timeStamp)
    {

        if ((result != null) &&
            (authenticationUrl != null) &&
            (sourceTimeStamp > 0) &&
            (timeStamp > 0))
        {
            if (DATA_RESULT.fromLabel(result) == DATA_RESULT.APN_FAILURE_NOT_LOGGED_IN)
            {
                if (CommonUtilities.isRemoteUrl(authenticationUrl))
                {
                    return DATA_RESULT.fromLabel(result);
                }
            }
        }

        if ((result != null) &&
            (sourceTimeStamp > 0) &&
            (timeStamp > 0))
        {
            if (DATA_RESULT.fromLabel(result) != DATA_RESULT.APN_FAILURE_NOT_LOGGED_IN)
            {
                return DATA_RESULT.fromLabel(result);
            }
        }

        return null;
    }


    public Pair<Map.Entry<String, NoteMessageDataHandler.NoteMessageData>, DATA_RESULT> processAuthenticationMessage(String message)
    {
        JSONObject jsonObject;
        String authenticationUrl, result;
        long sourceTimeStamp, timeStamp;
        DATA_RESULT dataResult;

        try
        {
            jsonObject = new JSONObject(message);
            authenticationUrl = jsonObject.optString(NoteBaseMessage.DATA_AUTHENTICATION_URL, null);
            result = jsonObject.optString(NoteBaseMessage.DATA_RESULT_FLD, null);
            sourceTimeStamp = jsonObject.optLong(NoteBaseMessage.DATA_SOURCE_TIMESTAMP_FLD, -1);
            timeStamp = jsonObject.getLong(NoteBaseMessage.DATA_TIMESTAMP_FLD);

            dataResult = handleReplyMessage(result, authenticationUrl, sourceTimeStamp, timeStamp);

            if (dataResult != null)
            {
                if (mAuthenticationData.mTimeStamp == sourceTimeStamp)
                {
                    mAuthenticationData.mInfo = authenticationUrl;
                    return new Pair<Map.Entry<String, NoteMessageDataHandler.NoteMessageData>, DATA_RESULT>(new AbstractMap.SimpleEntry<String, NoteMessageDataHandler.NoteMessageData>(null, mAuthenticationData), dataResult);
                }
            }
        }

        catch (Exception exception)
        {
            exception.printStackTrace();
            throw new RuntimeException(mContext.getString(R.string.common_invalid_message_received) + exception.toString() + ")");
        }

        return null;
    }


    public Object processNonAuthenticationMessage(String message)
    {
        JSONObject jsonObject;
        String action, authenticationUrl, noteCreationDate, entity, files, result, noteSubject;
        long sourceTimeStamp, timeStamp;
        DATA_ACTION dataAction;
        DATA_RESULT dataResult;

        try
        {
            jsonObject = new JSONObject(message);
            action = jsonObject.optString(NoteBaseMessage.DATA_ACTION_FLD, null);
            authenticationUrl = jsonObject.optString(NoteBaseMessage.DATA_AUTHENTICATION_URL, null);
            noteCreationDate = jsonObject.optString(NoteBaseMessage.NOTE_CREATION_DATE_FLD, null);
            entity = jsonObject.optString(NoteBaseMessage.DATA_ENTITY_FLD, null);
            files = jsonObject.optString(NoteBaseMessage.DATA_FILES_FLD, null);
            result = jsonObject.optString(NoteBaseMessage.DATA_RESULT_FLD, null);
            sourceTimeStamp = jsonObject.optLong(NoteBaseMessage.DATA_SOURCE_TIMESTAMP_FLD, -1);
            noteSubject = jsonObject.optString(NoteBaseMessage.NOTE_SUBJECT_FLD, null);
            timeStamp = jsonObject.getLong(NoteBaseMessage.DATA_TIMESTAMP_FLD);

            dataAction = handleRequestMessage(action, noteCreationDate, entity, files, noteSubject, timeStamp);

            if (dataAction != null)
            {
                addDataToReceive(DATA_ENTITY.fromLabel(entity) == DATA_ENTITY.NOTE ? noteCreationDate : entity,
                                 timeStamp,
                                 DATA_ACTION.fromLabel(action),
                                 noteSubject);
                return timeStamp;
            }

            dataResult = handleReplyMessage(result, authenticationUrl, sourceTimeStamp, timeStamp);

            if (dataResult != null)
            {
                if (mAuthenticationData.mTimeStamp == sourceTimeStamp)
                {
                    return null;
                }

                for (Map.Entry<String, NoteMessageDataHandler.NoteMessageData> uploadDataNotProcessedElement : mUploadDataNotProcessed.entrySet())
                {
                    if  (uploadDataNotProcessedElement.getValue().mTimeStamp == sourceTimeStamp)
                    {
                        return new Pair<Map.Entry<String, NoteMessageDataHandler.NoteMessageData>, DATA_RESULT>(uploadDataNotProcessedElement, dataResult);
                    }
                }
            }
        }

        catch (Exception exception)
        {
            exception.printStackTrace();
            throw new RuntimeException(mContext.getString(R.string.common_invalid_message_received) + exception.toString() + ")");
        }

        throw new RuntimeException(mContext.getString(R.string.common_invalid_message_received) + message + ")");
    }


    public String buildAuthenticationRequestMessage(long        timeStamp,
                                                    DATA_ACTION dataAction)
    {
        NoteRequestMessage.Builder builder;

        mAuthenticationData.mTimeStamp = timeStamp;
        mAuthenticationData.mDataAction = dataAction;

        builder = new NoteRequestMessage.Builder(mAuthenticationData.mTimeStamp,
                                                 CommonUtilities.getPreference(mContext, FIREBASE_REFRESHED_TOKEN_TAG),
                                                 mAuthenticationData.mDataAction);

        return builder.build().toString();
    }


    public String buildNoteRequestMessage(String creationDate)
    {
        NoteMessageData noteMessageData;
        NoteRequestMessage.Builder builder;

        noteMessageData = mUploadDataNotProcessed.get(creationDate);

        builder = new NoteRequestMessage.Builder(noteMessageData.mTimeStamp,
                                                 CommonUtilities.getPreference(mContext, FIREBASE_REFRESHED_TOKEN_TAG),
                                                 noteMessageData.mDataAction).setEntity(DATA_ENTITY.NOTE);

        builder.addField(NoteBaseMessage.NOTE_CREATION_DATE_FLD, creationDate);

        return builder.build().toString();
    }


    public String buildAcknowledgementMessage(long        timeStamp,
                                              long        sourceTimeStamp,
                                              String      errorDescription,
                                              DATA_RESULT dataResult)
    {
        NoteReplyMessage.Builder builder;

        builder = new NoteReplyMessage.Builder(timeStamp,
                                               CommonUtilities.getPreference(mContext, FIREBASE_REFRESHED_TOKEN_TAG),
                                               dataResult).setSourceTimeStamp(sourceTimeStamp)
                                                          .setErrorDescription(errorDescription);

        return builder.build().toString();
    }


    public String buildReplyMessage(String      id,
                                    long        timeStamp,
                                    DATA_RESULT dataResult)
    {
        NoteMessageData noteMessageData;
        NoteReplyMessage.Builder builder;

        noteMessageData = (dataResult == DATA_RESULT.APN_SUCCESS ? mDownloadDataProcessed : mDownloadDataNotProcessed).get(id);

        builder = new NoteReplyMessage.Builder(timeStamp,
                                               CommonUtilities.getPreference(mContext, FIREBASE_REFRESHED_TOKEN_TAG),
                                               dataResult).setSourceTimeStamp(noteMessageData.mTimeStamp);

        return builder.build().toString();
    }


    @Override
    protected Object clone() throws CloneNotSupportedException
    {

        throw new CloneNotSupportedException("Clone is not allowed.");
    }
}