package everyPurposeNotes.messaging;


import org.json.JSONObject;


public class NoteReplyMessage extends NoteBaseMessage
{
    private NoteMessageDataHandler.DATA_RESULT mResult;
    private long                               mSourceTimeStamp;
    private String                             mErrorDescription;


    public static class Builder extends NoteBaseMessage.Builder<Builder>
    {
        private NoteMessageDataHandler.DATA_RESULT _result;
        private long                               _sourceTimeStamp;
        private String                             _errorDescription;


        public Builder(long                               timeStamp,
                       String                             firebaseToken,
                       NoteMessageDataHandler.DATA_RESULT result)
        {

            super(timeStamp, firebaseToken);

            _result = result;
            _sourceTimeStamp = -1;
            _errorDescription = null;
        }


        public Builder setSourceTimeStamp(long sourceTimeStamp)
        {

            _sourceTimeStamp = sourceTimeStamp;

            return this;
        }


        public Builder setErrorDescription(String errorDescription)
        {

            _errorDescription = errorDescription;

            return this;
        }


        @Override
        public NoteReplyMessage build()
        {

            return new NoteReplyMessage(_timeStamp, _firebaseToken, _result, _sourceTimeStamp, _errorDescription);
        }
    }


    private NoteReplyMessage(long                               timeStamp,
                             String                             firebaseToken,
                             NoteMessageDataHandler.DATA_RESULT result,
                             long                               sourceTimeStamp,
                             String                             errorDescription)
    {

        super(timeStamp, firebaseToken);

        mResult = result;
        mSourceTimeStamp = sourceTimeStamp;
        mErrorDescription = errorDescription;
    }


    @Override
    public String toString()
    {

        try
        {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(DATA_TIMESTAMP_FLD, mTimeStamp);
            jsonObject.put(DATA_FIREBASE_TOKEN_FLD, mFirebaseToken);
            jsonObject.put(DATA_RESULT_FLD, mResult);

            if (mSourceTimeStamp != -1)
            {
                jsonObject.put(DATA_SOURCE_TIMESTAMP_FLD, mSourceTimeStamp);
            }

            if (mErrorDescription != null)
            {
                jsonObject.put(DATA_ERROR_DESCRIPTION_FLD, mErrorDescription);
            }

            return jsonObject.toString();
        }

        catch (Exception exception)
        {
            exception.printStackTrace();
            throw new RuntimeException("Unexpected error " + exception.toString());
        }
    }
}