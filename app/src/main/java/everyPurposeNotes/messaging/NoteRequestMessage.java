package everyPurposeNotes.messaging;


import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;


public class NoteRequestMessage extends NoteBaseMessage
{
    private NoteMessageDataHandler.DATA_ACTION mAction;
    private NoteMessageDataHandler.DATA_ENTITY mEntity;
    private Map<String, String>                mFields;


    public static class Builder extends NoteBaseMessage.Builder<Builder>
    {
        private NoteMessageDataHandler.DATA_ACTION _action;
        private NoteMessageDataHandler.DATA_ENTITY _entity;
        private Map<String, String>                _fields;


        public Builder(long                               timeStamp,
                       String                             firebaseToken,
                       NoteMessageDataHandler.DATA_ACTION action)
        {

            super(timeStamp, firebaseToken);

            _action = action;
            _entity = null;
            _fields = new HashMap<String, String>();
        }


        public Builder setEntity(NoteMessageDataHandler.DATA_ENTITY entity)
        {

            _entity = entity;

            return this;
        }


        public Builder addField(String key,
                                String value)
        {

            _fields.put(key, value);

            return this;
        }


        @Override
        public NoteRequestMessage build()
        {

            return new NoteRequestMessage(_timeStamp, _firebaseToken, _action, _entity, _fields);
        }
    }


    private NoteRequestMessage(long                               timeStamp,
                               String                             firebaseToken,
                               NoteMessageDataHandler.DATA_ACTION action,
                               NoteMessageDataHandler.DATA_ENTITY entity,
                               Map<String, String>                fields)
    {

        super(timeStamp, firebaseToken);

        mAction = action;
        mEntity = entity;
        mFields = fields;
    }


    @Override
    public String toString()
    {

        try
        {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(DATA_TIMESTAMP_FLD, mTimeStamp);
            jsonObject.put(DATA_FIREBASE_TOKEN_FLD, mFirebaseToken);
            jsonObject.put(DATA_ACTION_FLD, mAction);

            if (mEntity != null)
            {
                jsonObject.put(DATA_ENTITY_FLD, mEntity);
            }

            for (Map.Entry<String, String> field : mFields.entrySet())
            {
                jsonObject.put(field.getKey(), field.getValue());
            }

            return jsonObject.toString();
        }

        catch (Exception exception)
        {
            exception.printStackTrace();
            throw new RuntimeException("Unexpected error " + exception.toString());
        }
    }
}