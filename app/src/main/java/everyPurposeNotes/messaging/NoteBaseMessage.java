package everyPurposeNotes.messaging;


public class NoteBaseMessage
{
    public static final String    DATA_ACTION_FLD = "action";
    public static final String    DATA_AUTHENTICATION_URL = "authenticationUrl";
    public static final String    DATA_ENTITY_FLD = "entity";
    public static final String    DATA_ERROR_DESCRIPTION_FLD = "errorDescription";
    public static final String    DATA_FILES_FLD = "files";
    public static final String    DATA_FIREBASE_TOKEN_FLD = "firebaseToken";
    public static final String    DATA_RESULT_FLD = "result";
    public static final String    DATA_SOURCE_TIMESTAMP_FLD = "sourceTimeStamp";
    public static final String    DATA_TIMESTAMP_FLD = "timeStamp";

    public static final String    NOTE_CREATION_DATE_FLD = "creationDate";
    public static final String    NOTE_SUBJECT_FLD = "subject";

    protected static final String UPLOAD_DATA_NOT_PROCESSED_TAG = "uploadDataNotProcessed";
    protected static final String UPLOAD_DATA_PROCESSED_TAG = "uploadDataProcessed";
    protected static final String DOWNLOAD_DATA_NOT_PROCESSED_TAG = "downloadDataNotProcessed";
    protected static final String DOWNLOAD_DATA_PROCESSED_TAG = "downloadDataProcessed";
    
    
    protected long                mTimeStamp;
    protected String              mFirebaseToken;


    protected abstract static class Builder<T extends Builder<T>>
    {
        protected long   _timeStamp;
        protected String _firebaseToken;


        protected Builder(long   timeStamp,
                          String firebaseToken)
        {

            _timeStamp = timeStamp;
            _firebaseToken = firebaseToken;
        }


        protected abstract NoteBaseMessage build();
    }


    protected NoteBaseMessage(long   timeStamp,
                              String firebaseToken)
    {

        mTimeStamp = timeStamp;
        mFirebaseToken = firebaseToken;
    }
}