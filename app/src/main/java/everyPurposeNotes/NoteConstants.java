package everyPurposeNotes;


import android.Manifest;
import android.os.Environment;
import java.io.File;


import everyPurposeNotesBusiness.NoteApplication;
import everyPurposeNotesBusiness.noteScreens.R;


/**
 * Interface to be implemented by classes sharing common constants.
 */
public interface NoteConstants
{
 public enum RELEASE_TYPE
 {
  LITE("Lite"),
  FULL(""),
  BUSINESS("Business");


  private String label;


  RELEASE_TYPE(String label)
  {

   this.label = label;
  }
 }


 public static final int                     VERSION_LABEL = R.string.note_constants_version;
 public static final int                     VERSION_VALUE_MAJOR = 1;
 public static final int                     VERSION_VALUE_MINOR = 0;
 public static final int                     VERSION_VALUE_REVISION = 0;
 public static final String                  VERSION_VALUE = VERSION_VALUE_MAJOR + "." + VERSION_VALUE_MINOR + "." + VERSION_VALUE_REVISION;
 public static final int                     AUTHOR_LABEL = R.string.note_constants_by_correia_e_silva;
 public static final int                     BUILD_DATE_LABEL = R.string.note_constants_built_on;

 public static final String[]                APP_PERMISSIONS = {Manifest.permission.INTERNET,
    	                                                        Manifest.permission.ACCESS_FINE_LOCATION,
    	                                                        Manifest.permission.ACCESS_COARSE_LOCATION,
    	                                                        Manifest.permission.RECORD_AUDIO,
    	                                                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
    	                                                        Manifest.permission.CAMERA,
    	                                                        Manifest.permission.VIBRATE,
    	                                                        Manifest.permission.WAKE_LOCK,
    	                                                        Manifest.permission.ACCESS_NETWORK_STATE,
                                                                Manifest.permission.BLUETOOTH,
                                                                Manifest.permission.GET_ACCOUNTS};

 public static final int                     CREDITS_AND_PRIVACY_POLICY_LABEL = R.string.common_credits_and_privacy_policy;

 public static final String                  CREDITS_MATERIAL_DESIGN_ICONS_LABEL = "<a href=https://design.google.com/icons/>Material Design icons</a> by Google / <a href=https://creativecommons.org/licenses/by/4.0/>CC BY</a>";
 public static final String                  CREDITS_HAWCONS_GESTURE_STROKE_ICONS_LABEL = "<a href=https://www.iconfinder.com/iconsets/hawcons-gesture-stroke>Hawcons Gesture Stroke icons</a> by Yannick Lung / <a href=http://creativecommons.org/licenses/by/3.0/>CC BY</a>";
 public static final String                  CREDITS_DROPBOX_LABEL = "Copyright (c) 2009-2011 Dropbox Inc., <a href=http://www.dropbox.com/>http://www.dropbox.com</a>" +
                                                                     "<br/><br/>" +
                                                                     "Permission is hereby granted, free of charge, to any person obtaining\n" +
                                                                     "a copy of this software and associated documentation files (the\n" +
                                                                     "\"Software\"), to deal in the Software without restriction, including\n" +
                                                                     "without limitation the rights to use, copy, modify, merge, publish,\n" +
                                                                     "distribute, sublicense, and/or sell copies of the Software, and to\n" +
                                                                     "permit persons to whom the Software is furnished to do so, subject to\n" +
                                                                     "the following conditions:" +
                                                                     "<br/><br/>" +
                                                                     "The above copyright notice and this permission notice shall be\n" +
                                                                     "included in all copies or substantial portions of the Software." +
                                                                     "<br/><br/>" +
                                                                     "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,\n" +
                                                                     "EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\n" +
                                                                     "MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND\n" +
                                                                     "NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE\n" +
                                                                     "LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION\n" +
                                                                     "OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION\n" +
                                                                     "WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.";
 public static final String                  CREDITS_EXOPLAYER_LABEL = "<a href=https://github.com/google/ExoPlayer>Google ExoPlayer</a> by Google / <a href=https://www.apache.org/licenses/LICENSE-2.0>CC BY</a>";                                                                     

 public static final String                  PRIVACY_POLICY_PREFIX_LABEL = "<a href=https://sites.google.com/view/allpurposenotes/p%C3%A1gina-inicial>";
 public static final String                  PRIVACY_POLICY_SUFFIX_LABEL = "</a>";

 public static final int                     PRIVACY_POLICY_LABEL = R.string.note_constants_privacy_policy_label;

 public static final int                     NOTE_RELEASE_LITE_MAX_RECS = 3;

 public static final String                  NOTES_DB = "notes";

 public static final String                  NOTES_TABLE = "notes";
 public static final String                  NOTE_DATA_TABLE = "note_data";
 public static final String                  CATS_TABLE = "cats";
 public static final String                  FLDS_TABLE = "flds";
 public static final String                  FLDS_DATA_TABLE ="flds_data";
 public static final String                  FLDS_CATS_TABLE ="flds_cats";
 public static final String                  NOTE_FREE_TEXT_FLDS_TABLE = "note_free_text_flds";
 public static final String                  NOTE_SELECTABLE_FLDS_TABLE = "note_selectable_flds";

 public static final String                  DATE_FLD = "date";
 public static final String                  UPDATE_DATE_FLD = "update_date";
 public static final String                  LAT_FLD = "lat";
 public static final String                  LONG_FLD = "long";
 public static final String                  ALTITUDE_FLD = "altitude";
 public static final String                  LOC_FLD = "location";
 public static final String                  CAT_FLD = "cat";
 public static final String                  ID_FLD = "id";
 public static final String                  PRIO_FLD = "priority";
 public static final String                  SUBJ_FLD = "subject";
 public static final String                  BODY_FLD = "body";
 public static final String                  ALARM_DATE_FLD = "alarm_date";
 public static final String                  FILE_NAME_FLD = "file_name";
 public static final String                  TYPE_FLD = "type";
 public static final String                  COMMENTS_FLD = "comments";
 public static final String                  DESC_FLD = "description";
 public static final String                  CONTENT_FLD = "content";
 public static final String                  FLD_ID_FLD = "fld_id";
 public static final String                  FLD_DATA_ID_FLD = "fld_data_id";

 public static final int[]                   COLUMNS_LABELS = {R.string.note_constants_priority,
                                                               R.string.note_constants_created,
                                                               R.string.note_constants_updated,
                                                               R.string.note_constants_distance,
                                                               R.string.note_constants_subject};

 public static final int                     COLUMNS_HEADER_PRIO_COLUMN = 0;
 public static final int                     COLUMNS_HEADER_CREATED_COLUMN = 1;
 public static final int                     COLUMNS_HEADER_UPDATED_COLUMN = 2;
 public static final int                     COLUMNS_HEADER_DIST_COLUMN = 3;
 public static final int                     COLUMNS_HEADER_SUBJ_COLUMN = 4;

 public static final int                     COLUMNS_DATA_PRIO_COLUMN = 0;
 public static final int                     COLUMNS_DATA_CREATED_COLUMN = 1;
 public static final int                     COLUMNS_DATA_UPDATED_COLUMN = 2;
 public static final int                     COLUMNS_DATA_CAT_COLUMN = 3;
 public static final int                     COLUMNS_DATA_LOCATION_COLUMN = 4;
 public static final int                     COLUMNS_DATA_DIST_COLUMN = 5;
 public static final int                     COLUMNS_DATA_TOTAL_PICTURES_COLUMN = 6;
 public static final int                     COLUMNS_DATA_1ST_PICTURE_COLUMN = 7;
 public static final int                     COLUMNS_DATA_TOTAL_TRACKS_COLUMN = 8;
 public static final int                     COLUMNS_DATA_TOTAL_PHOTOS_COLUMN = 9;
 public static final int                     COLUMNS_DATA_1ST_PHOTO_COLUMN = 10;
 public static final int                     COLUMNS_DATA_TOTAL_VIDEOS_COLUMN = 11;
 public static final int                     COLUMNS_DATA_1ST_VIDEO_COLUMN = 12;
 public static final int                     COLUMNS_DATA_SUBJ_COLUMN = 13;
 public static final int                     COLUMNS_DATA_BODY_COLUMN = 14;
 public static final int                     COLUMNS_DATA_COORDS_COLUMN = 15;
 public static final int                     COLUMNS_DATA_ALTITUDE_COLUMN = 16;
 public static final int                     COLUMNS_DATA_ALARM_DATE_COLUMN = 17;
 public static final int                     COLUMNS_DATA_SEL_COLUMN = 18;
 public static final int                     COLUMNS_DATA_BYTES_COLUMN = 19;
 public static final int                     COLUMNS_DATA_ICON_COLUMN = 20;
 public static final int                     COLUMNS_DATA_TOTAL_DOCUMENTS_COLUMN = 21;
 public static final int                     COLUMNS_DATA_1ST_DOCUMENT_COLUMN = 22;

 public static final int                     MSG_TAG_USE_NOTES_DB = 0;
 public static final int                     MSG_TAG_CLOSE_DBS = 1;
 public static final int                     MSG_TAG_TRANSACTION_START = 2;
 public static final int                     MSG_TAG_TRANSACTION_END = 3;
 public static final int                     MSG_TAG_TRANSACTION_ROLLBACK = 4;
 public static final int                     MSG_TAG_LOAD_NOTES_IDS = 5;
 public static final int                     MSG_TAG_LOAD_NOTE = 6;
 public static final int                     MSG_TAG_LOAD_NOTE_DATA = 7;
 public static final int                     MSG_TAG_SAVE_NOTE = 8;
 public static final int                     MSG_TAG_UPDATE_NOTE = 9;
 public static final int                     MSG_TAG_DEL_NOTE = 10;
 public static final int                     MSG_TAG_LOAD_NOTES_CATS = 11;
 public static final int                     MSG_TAG_LOAD_NOTES_CATS_IN_USE = 12;
 public static final int                     MSG_TAG_SAVE_NOTES_CAT = 13;
 public static final int                     MSG_TAG_UPDATE_NOTES_CAT = 14;
 public static final int                     MSG_TAG_DEL_NOTES_CATS = 15;
 public static final int                     MSG_TAG_DEL_NOTES_CAT = 16;
 public static final int                     MSG_TAG_LOAD_NOTES_FLDS = 17;
 public static final int                     MSG_TAG_LOAD_NOTES_FLD_DATA = 18;
 public static final int                     MSG_TAG_LOAD_NOTES_FLD_CATS = 19;
 public static final int                     MSG_TAG_LOAD_NOTES_FLDS_IN_USE = 20;
 public static final int                     MSG_TAG_LOAD_NOTES_SUB_FLDS_IN_USE = 21;
 public static final int                     MSG_TAG_LOAD_NOTES_FLD_CATS_IN_USE = 22;
 public static final int                     MSG_TAG_SAVE_NOTES_FLD = 23;
 public static final int                     MSG_TAG_UPDATE_NOTES_FLD = 24;
 public static final int                     MSG_TAG_DEL_NOTES_FLDS = 25;
 public static final int                     MSG_TAG_DEL_NOTES_FLD = 26;
 public static final int                     MSG_TAG_LOAD_NOTE_FREE_TEXT_FLDS = 27;
 public static final int                     MSG_TAG_LOAD_NOTE_SELECTABLE_FLDS = 28;

 public static final int                     EXECUTE_UNDEF = -1;
 public static final int                     EXECUTE_INIT = 0;
 public static final int                     EXECUTE_INSTALL_TTS = 1;
 public static final int                     EXECUTE_START_TTS = 2;
 public static final int                     EXECUTE_STOP_TTS = 3;
 public static final int                     EXECUTE_SRCH_NOTE = 4;
 public static final int                     EXECUTE_NEW_NOTE = 5;
 public static final int                     EXECUTE_EDIT_NOTE = 6;
 public static final int                     EXECUTE_SAVE_NOTE = 7;
 public static final int                     EXECUTE_DEL_NOTES = 8;
 public static final int                     EXECUTE_MAP_NOTES = 9;
 public static final int                     EXECUTE_AR_NOTES = 10;
 public static final int                     EXECUTE_FILTER_NOTES = 11;
 public static final int                     EXECUTE_ABOUT = 12;
 public static final int                     EXECUTE_CLOSE = 13;
 public static final int                     EXECUTE_ENABLE_HYPERLINK = 14;
 public static final int                     EXECUTE_DISABLE_HYPERLINK = 15;
 public static final int                     EXECUTE_COMMENT_PICTURE = 16;
 public static final int                     EXECUTE_DRAW_ON_PICTURE = 17;
 public static final int                     EXECUTE_COMMENT_PHOTO = 18;
 public static final int                     EXECUTE_DRAW_ON_PHOTO = 19;
 public static final int                     EXECUTE_SET_NOTE_COORDS = 20;
 public static final int                     EXECUTE_NEW_NOTE_WITHOUT_DATA = 21;
 public static final int                     EXECUTE_NEW_NOTE_WITH_PICTURES = 22;
 public static final int                     EXECUTE_NEW_NOTE_WITH_TRACKS = 23;
 public static final int                     EXECUTE_NEW_NOTE_WITH_PHOTOS = 24;
 public static final int                     EXECUTE_NEW_NOTE_WITH_VIDEOS = 25;
 public static final int                     EXECUTE_RECOGNIZE_SPEECH = 26;
 public static final int                     EXECUTE_SHARE_PICTURE = 27;
 public static final int                     EXECUTE_COMMENT_TRACK = 28;
 public static final int                     EXECUTE_SHARE_TRACK = 29;
 public static final int                     EXECUTE_SHARE_PHOTO = 30;
 public static final int                     EXECUTE_COMMENT_VIDEO = 31;
 public static final int                     EXECUTE_SHARE_VIDEO = 32;
 public static final int                     EXECUTE_SHARE_NOTE = 33;
 public static final int                     EXECUTE_SET_SPEECH = 34;
 public static final int                     EXECUTE_NEW_NOTES_FLDS = 35;
 public static final int                     EXECUTE_EDIT_NOTES_FLDS = 36;
 public static final int                     EXECUTE_DEL_NOTES_FLDS = 37;
 public static final int                     EXECUTE_BACKUP_NOTES_FLDS_LOCAL = 38;
 public static final int                     EXECUTE_BACKUP_NOTES_FLDS_REMOTE = 39;
 public static final int                     EXECUTE_RESTORE_NOTES_FLDS_LOCAL = 40;
 public static final int                     EXECUTE_RESTORE_NOTES_FLDS_REMOTE = 41;
 public static final int                     EXECUTE_RESTORE_NOTES_FLDS_SHARED_FILE = 42;
 public static final int                     EXECUTE_BACKUP_NOTES_LOCAL = 43;
 public static final int                     EXECUTE_UPLOAD_NOTES = 44;
 public static final int                     EXECUTE_RESTORE_NOTES_LOCAL = 45;
 public static final int                     EXECUTE_DOWNLOAD_NOTES = 46;
 public static final int                     EXECUTE_RESTORE_NOTE_SHARED_FILE = 47;
 public static final int                     EXECUTE_DEL_TEXT = 48;
 public static final int                     EXECUTE_INSTALL_GOOGLE_PLAY_SERVICES = 49;
 public static final int                     EXECUTE_REQ_APP_PERMISSIONS = 50;
 public static final int                     EXECUTE_SELECT_VIDEOS_TO_DEL_AFTER_SAVE = 51;
 public static final int                     EXECUTE_SELECT_VIDEOS_TO_DEL_AFTER_FINISH = 52;
 public static final int                     EXECUTE_COMMENT_DOCUMENT = 53;
 public static final int                     EXECUTE_SHARE_DOCUMENT = 54;
 public static final int                     EXECUTE_SHUFFLE_ON = 55;
 public static final int                     EXECUTE_SHUFFLE_OFF = 56;
 public static final int                     EXECUTE_REPEAT_OFF = 57;
 public static final int                     EXECUTE_REPEAT_ONE = 58;
 public static final int                     EXECUTE_REPEAT_ALL = 59;

 public static final int                     NOTE_MAX_LABEL_CHARS = 15;
 public static final int                     NOTE_SUBJ_CHARS = 64;
 public static final int                     NOTE_BODY_CHARS = 1024;

 public static final int                     CAT_UNDEF = -1;
 public static final int                     CAT_NONE_LABEL = R.string.common_no_category;

 public static final int                     FLD_UNDEF = -1;
 public static final int                     FLD_CHARS = 64;
 public static final int                     FLD_CONTENT_CHARS = 64;

 public static final int                     PRIO_UNDEF = -1;
 public static final int                     PRIO_NORMAL = 0;
 public static final int                     PRIO_IMPORTANT = 1;
 public static final int                     PRIO_URGENT = 2;
 public static final int                     PRIOS_LABEL = R.string.common_priorities;

 public static final int                     TYPE_PICTURES = 0;
 public static final int                     TYPE_TRACKS = 1;
 public static final int                     TYPE_PHOTOS = 2;
 public static final int                     TYPE_VIDEOS = 3;
 public static final int                     TYPE_DOCUMENTS = 4;

 public static final int                     INTENT_REQ_CODE_HANDLE_FLDS_VALUES = 1;

 public static final int                     ALTITUDE_UNDEF = -1;
 public static final float                   DIST_UNDEF = -1.0f;

 public static final int                     NOTE_PRIO_LABEL = R.string.common_priority;
 public static final int                     NOTE_CAT_LABEL = R.string.common_category;

 public static final int[]                   NOTES_PRIOS = {R.string.note_constants_normal,
                                                            R.string.note_constants_important,
                                                            R.string.note_constants_urgent};

 public static final RELEASE_TYPE            RELEASE_CURR = RELEASE_TYPE.BUSINESS;

 public static final String                  NOTES_FOLDER_NAME = "AllPurposeNotes" + RELEASE_CURR.label;
 public static final String                  NOTES_FOLDER_PATH_BEFORE_ANDROID_Q = Environment.getExternalStorageDirectory() + File.separator + NOTES_FOLDER_NAME;
 public static final String                  NOTES_FOLDER_PATH = NoteApplication.mExternalStoragePath + File.separator + NOTES_FOLDER_NAME;
 public static final String                  NOTES_TEMP_FOLDER_PATH = NOTES_FOLDER_PATH + File.separator + "temp";
 public static final String                  NOTES_BACKUP_LOCAL_FOLDER_PATH_DATA = NOTES_FOLDER_PATH + File.separator + "backup/data";
 public static final String                  NOTES_BACKUP_REMOTE_FOLDER_PATH_DATA = NOTES_FOLDER_NAME + File.separator + "backup/data";
 public static final String                  NOTES_BACKUP_LOCAL_FOLDER_PATH_FLDS = NOTES_FOLDER_PATH + File.separator + "backup/fields";
 public static final String                  NOTES_BACKUP_REMOTE_FOLDER_PATH_FLDS = NOTES_FOLDER_NAME + File.separator + "backup/fields";

 public static final String                  NOTES_CATS_FILE_NAME = "NOTES_CATS";
 public static final String                  NOTES_FLDS_FILE_NAME = "NOTES_FLDS";

 public static final String                  COMPRESSED_FILE_EXT = ".apn";

 public static final String[]                COMPRESSED_FILES_SET = {COMPRESSED_FILE_EXT};

 public static final int                     NOTE_FILTER_PRIO_FLD_INDEX = 0;
 public static final int                     NOTE_FILTER_BEGIN_DATE_FLD_INDEX = 1;
 public static final int                     NOTE_FILTER_END_DATE_FLD_INDEX = 2;
 public static final int                     NOTE_FILTER_CAT_FLD_INDEX = 3;
 public static final int                     NOTE_FILTER_SUBJ_FLD_INDEX = 4;
 public static final int                     NOTE_FILTER_BODY_FLD_INDEX = 5;
}