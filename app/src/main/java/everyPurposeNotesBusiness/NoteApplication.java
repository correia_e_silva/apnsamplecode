package everyPurposeNotesBusiness;


import android.os.StrictMode;
import androidx.multidex.MultiDexApplication;


import common.CommonConstants;
import common.commonUtilities.CommonUtilities;


public class NoteApplication extends MultiDexApplication implements CommonConstants
{
    public static String mExternalStoragePath;


    @Override
    public void onCreate()
    {

        super.onCreate();

        //Needed to avoid 'android.os.FileUriExposedException' (Nougat onwards)
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().build());

        if (CommonUtilities.getPreference(this, THEME_TAG).length() == 0)
        {
            CommonUtilities.setPreference(this, THEME_TAG, LIGHT_LABEL);
        }

        mExternalStoragePath = getExternalFilesDir(null).getAbsolutePath();
    }
}