package everyPurposeNotesBusiness.noteScreens;


import android.os.*;
import androidx.appcompat.app.AppCompatActivity;


import common.CommonConstants;
import common.commonUtilities.CommonUtilities;
import everyPurposeNotes.NoteConstants;


public class NoteList extends AppCompatActivity implements CommonConstants,
                                                           NoteConstants
{


 @Override
 public void onCreate(Bundle savedInstanceState)
 {

  setTheme(CommonUtilities.getPreference(this, THEME_TAG).compareTo(LIGHT_LABEL) == 0 ? R.style.CustomThemeLight : R.style.CustomThemeDark);

  super.onCreate(savedInstanceState);

  CommonUtilities.adjustFontScale(this, getResources().getConfiguration());
 }
}