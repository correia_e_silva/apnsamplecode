package everyPurposeNotesBusiness.noteScreens;


import android.graphics.Typeface;
import android.location.Geocoder;
import android.location.Location;
import android.os.*;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Gravity;
import android.widget.*;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.observers.DisposableObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;


import common.CommonConstants;
import common.commonUtilities.CommonUtilities;
import everyPurposeNotes.NoteConstants;


public class NoteData extends AppCompatActivity implements CommonConstants,
                                                           NoteConstants
{
 private static final int MAX_TEXT_VIEWS = 3;


 private Location         currentLocation;
 private TextView[]       textView = new TextView[MAX_TEXT_VIEWS];
 private Geocoder         geocoder;
 private Disposable       getLocationAddress;


 @Override
 public void onCreate(Bundle savedInstanceState)
 {

  setTheme(CommonUtilities.getPreference(this, THEME_TAG).compareTo(LIGHT_LABEL) == 0 ? R.style.CustomThemeLight : R.style.CustomThemeDark);

  super.onCreate(savedInstanceState);

  CommonUtilities.adjustFontScale(this, getResources().getConfiguration());

  currentLocation = null;

  for (int i = 0;
       i < textView.length;
       i ++)
  {
   textView[i] = new TextView(this);
   textView[i].setTypeface(Typeface.MONOSPACE);
   textView[i].setTextSize(FONT_SIZE_TINY);
   textView[i].setGravity(Gravity.LEFT);
  }

  geocoder = new Geocoder(this);

  getLocationAddress = null;
 }

 @Override
 public void onDestroy()
 {

  super.onDestroy();

  if (getLocationAddress != null)
  {
   getLocationAddress.dispose();
  }
 }


 private Observable<String> getLocationAddressObservable()
 {

  return Observable.create(new ObservableOnSubscribe<String>()
  {


   @Override
   public void subscribe(@NonNull ObservableEmitter<String> emitter) throws Throwable
   {
    String address;

    try
    {
     address = geocoder.getFromLocation(currentLocation.getLatitude(), currentLocation.getLongitude(), 1).get(0).getAddressLine(0);

     emitter.onNext(address);
     emitter.onComplete();
    }

    catch (Exception exception)
    {
     emitter.onError(exception);
    }
   }
  });
 }


 private Disposable getLocationAddressDisposable()
 {

  return getLocationAddressObservable().subscribeOn(Schedulers.io())
                                       .observeOn(AndroidSchedulers.mainThread())
                                       .subscribeWith(new DisposableObserver<String>()
                                                      {


                                                       @Override
                                                       public void onNext(@NonNull String s)
                                                       {

                                                        textView[2].setText(s);
                                                       }


                                                       @Override
                                                       public void onError(@NonNull Throwable e)
                                                       {
                                                       }


                                                       @Override
                                                       public void onComplete()
                                                       {
                                                       }
                                                      });
 }


 public void handleOnLocationChanged(Location location)
 {

  currentLocation = new Location(location);

  if (getLocationAddress != null)
  {
   getLocationAddress.dispose();
  }

  getLocationAddress = getLocationAddressDisposable();
 }
}