package everyPurposeNotesBusiness.noteScreens;


import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import common.commonAdapters.CommonBaseAdapterGridView;
import common.commonUtilities.CommonUtilities;
import common.launcher.CommonLauncher;
import common.nativeBridge.NativeBridge;
import everyPurposeNotes.NoteConstants;


public abstract class NoteLauncher extends CommonLauncher implements NoteConstants
{


    public NoteLauncher()
    {

        NativeBridge.getInstance().timeBomb();
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {

        setTheme(CommonUtilities.getPreference(this, THEME_TAG).compareTo(LIGHT_LABEL) == 0 ? R.style.CustomThemeLight : R.style.CustomThemeDark);

        super.onCreate(savedInstanceState);
    }


    @Override
    protected String[] getAppPermissions()
    {

        return APP_PERMISSIONS;
    }


    @Override
    protected int getAppPermissionsRequestActionCode()
    {

        return EXECUTE_REQ_APP_PERMISSIONS;
    }


    @Override
    protected View getContentView()
    {
        List<Drawable> icons;
        int totalColumns, totalRows, displayWidth, displayHeight;
        ArrayList<Drawable> rowsData;
        CommonBaseAdapterGridView commonBaseAdapter;
        GridView gridView;

        icons = Arrays.<Drawable>asList(getResources().getDrawable(R.drawable.selector_write_tab),
                                        getResources().getDrawable(R.drawable.selector_post_it_tab),
                                        getResources().getDrawable(R.drawable.selector_audio_tab),
                                        getResources().getDrawable(R.drawable.selector_photo_tab),
                                        getResources().getDrawable(R.drawable.selector_video_tab),
                                        getResources().getDrawable(R.drawable.selector_document_tab));

        displayWidth = CommonUtilities.getDisplaySize(this).x;
        displayHeight = CommonUtilities.getDisplaySize(this).y;

        rowsData = new ArrayList<Drawable>();

        commonBaseAdapter = new CommonBaseAdapterGridView(this, rowsData, UNDEF_PICTURE_BACKGROUND);

        totalColumns = 4;
        totalRows = icons.size();

        for (int i = 0;
             i < totalColumns;
             i ++)
        {
            for (Drawable icon : icons)
            {
                commonBaseAdapter.add(icon);
            }
        }

        commonBaseAdapter.setItemOpacity(0.25f);
        commonBaseAdapter.setItemWidth(displayWidth / totalColumns);
        commonBaseAdapter.setItemHeight(displayHeight / totalRows);

        gridView = new GridView(this);
        gridView.setAdapter(commonBaseAdapter);
        gridView.setNumColumns(4);
        gridView.setBackgroundColor(Color.BLACK);

        return gridView;
    }


    @Override
    protected String getDialogTitle()
    {

        return getString(R.string.app_name);
    }


    @Override
    protected int getDialogIcon()
    {

        return R.drawable.ic_warning_white_36dp;
    }
}