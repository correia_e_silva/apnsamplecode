package common.cryptography;


import android.util.Base64;
import android.util.Log;
import java.io.*;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Map;
import javax.crypto.Cipher;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import org.spongycastle.asn1.x509.SubjectPublicKeyInfo;
import org.spongycastle.jce.provider.BouncyCastleProvider;
import org.spongycastle.openssl.PEMKeyPair;
import org.spongycastle.openssl.PEMParser;
import org.spongycastle.openssl.jcajce.JcaPEMKeyConverter;


public class CommonPublicKeyCryptographyHelper
{

    static
    {
        Security.addProvider(new BouncyCastleProvider());
    }


    public static KeyPair generateKeys(String keyPairGeneratorAlgorithm,
                                       int    keyPairGeneratorSize)
    {
        KeyPair result = null;

        try
        {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(keyPairGeneratorAlgorithm);
            keyPairGenerator.initialize(keyPairGeneratorSize, new SecureRandom());

            result = keyPairGenerator.generateKeyPair();
        }

        catch (Exception exception)
        {
            Log.e(CommonPublicKeyCryptographyHelper.class.getSimpleName(), "Unexpected error " + exception);
            exception.printStackTrace();
        }

        return result;
    }


    public static KeyPair generateKeys(String ellipticCurveParameterStandardName,
                                       String keyPairGeneratorAlgorithm)
    {
        KeyPair result = null;

        try
        {
            ECGenParameterSpec ecGenParameterSpec = new ECGenParameterSpec(ellipticCurveParameterStandardName);
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(keyPairGeneratorAlgorithm);
            keyPairGenerator.initialize(ecGenParameterSpec, new SecureRandom());

            result = keyPairGenerator.generateKeyPair();
        }

        catch (Exception exception)
        {
            Log.e(CommonPublicKeyCryptographyHelper.class.getSimpleName(), "Unexpected error " + exception);
            exception.printStackTrace();
        }

        return result;
    }


    public static PublicKey readPublicKey(String publicKeyData,
                                          String keyFactoryAlgorithm) throws Exception
    {
        String publicKey = publicKeyData.replaceAll("-----BEGIN PUBLIC KEY-----", "")
                                        .replaceAll(System.getProperty("line.separator"), "")
                                        .replaceAll("-----END PUBLIC KEY-----", "");

        byte[] publicKeyBytes = Base64.decode(publicKey, Base64.DEFAULT);
        X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(publicKeyBytes);

        KeyFactory keyFactory = KeyFactory.getInstance(keyFactoryAlgorithm);

        return keyFactory.generatePublic(x509EncodedKeySpec);
    }


    public static PublicKey readPublicKey(String publicKeyData) throws Exception
    {
        BufferedReader bufferedReader = new BufferedReader(new StringReader(publicKeyData));

        PEMParser pemParser = new PEMParser(bufferedReader);
        SubjectPublicKeyInfo subjectPublicKeyInfo = (SubjectPublicKeyInfo) pemParser.readObject();

        JcaPEMKeyConverter jcaPEMKeyConverter = new JcaPEMKeyConverter();

        return jcaPEMKeyConverter.getPublicKey(subjectPublicKeyInfo);
    }


    public static String encryptData(PublicKey publicKey,
                                     String    cipherTransformation,
                                     byte[]    unencryptedBytes)
    {
        String result = null;

        try
        {
            Cipher cipher = Cipher.getInstance(cipherTransformation);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);

            byte[] encryptedBytes = cipher.doFinal(unencryptedBytes);

            result = Base64.encodeToString(encryptedBytes, Base64.DEFAULT);
        }

        catch (Exception exception)
        {
            Log.e(CommonPublicKeyCryptographyHelper.class.getSimpleName(), "Unexpected error " + exception);
            exception.printStackTrace();
        }

        return result;
    }


    public static String encryptData(String publicKeyData,
                                     String keyFactoryAlgorithm,
                                     String cipherTransformation,
                                     byte[] unencryptedBytes)
    {
        String result = null;

        try
        {
            PublicKey publicKey = (keyFactoryAlgorithm == null ? readPublicKey(publicKeyData) : readPublicKey(publicKeyData, keyFactoryAlgorithm));
            result = encryptData(publicKey, cipherTransformation, unencryptedBytes);
        }

        catch (Exception exception)
        {
            Log.e(CommonPublicKeyCryptographyHelper.class.getSimpleName(), "Unexpected error " + exception);
            exception.printStackTrace();
        }

        return result;
    }


    public static PrivateKey readPrivateKey(String privateKeyData,
                                            String keyFactoryAlgorithm) throws Exception
    {
        String privateKey = privateKeyData.replaceAll("-----BEGIN RSA PRIVATE KEY-----", "")
                                          .replaceAll(System.getProperty("line.separator"), "")
                                          .replaceAll("-----END RSA PRIVATE KEY-----", "");

        byte[] privateKeyBytes = Base64.decode(privateKey, Base64.DEFAULT);
        PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);

        KeyFactory keyFactory = KeyFactory.getInstance(keyFactoryAlgorithm);

        return keyFactory.generatePrivate(pkcs8EncodedKeySpec);
    }


    public static PrivateKey readPrivateKey(String privateKeyData) throws Exception
    {
        BufferedReader bufferedReader = new BufferedReader(new StringReader(privateKeyData));

        PEMParser pemParser = new PEMParser(bufferedReader);
        PEMKeyPair pemKeyPair = (PEMKeyPair) pemParser.readObject();

        JcaPEMKeyConverter jcaPEMKeyConverter = new JcaPEMKeyConverter();

        return jcaPEMKeyConverter.getKeyPair(pemKeyPair).getPrivate();
    }


    public static String decryptData(PrivateKey privateKey,
                                     String     cipherTransformation,
                                     byte[]     encryptedBytes)
    {
        String result = null;

        try
        {
            Cipher cipher = Cipher.getInstance(cipherTransformation);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);

            byte[] unencryptedBytes = cipher.doFinal(encryptedBytes);

            result = new String(unencryptedBytes);
        }

        catch (Exception exception)
        {
            Log.e(CommonPublicKeyCryptographyHelper.class.getSimpleName(), "Unexpected error " + exception);
            exception.printStackTrace();
        }

        return result;
    }


    public static String decryptData(String privateKeyData,
                                     String keyFactoryAlgorithm,
                                     String cipherTransformation,
                                     byte[] encryptedBytes)
    {
        String result = null;

        try
        {
            PrivateKey privateKey = (keyFactoryAlgorithm == null ? readPrivateKey(privateKeyData) : readPrivateKey(privateKeyData, keyFactoryAlgorithm));
            result = decryptData(privateKey, cipherTransformation, encryptedBytes);
        }

        catch (Exception exception)
        {
            Log.e(CommonPublicKeyCryptographyHelper.class.getSimpleName(), "Unexpected error " + exception);
            exception.printStackTrace();
        }

        return result;
    }


    public static String signData(PrivateKey privateKey,
                                  String     signatureAlgorithm,
                                  byte[]     unencryptedBytes)
    {
        String result = null;

        try
        {
            Signature signature = Signature.getInstance(signatureAlgorithm);
            signature.initSign(privateKey, new SecureRandom());
            signature.update(unencryptedBytes);

            byte[] signedBytes = signature.sign();

            result = Base64.encodeToString(signedBytes, Base64.DEFAULT);
        }

        catch (Exception exception)
        {
            Log.e(CommonPublicKeyCryptographyHelper.class.getSimpleName(), "Unexpected error " + exception);
            exception.printStackTrace();
        }

        return result;
    }


    public static String signData(String privateKeyData,
                                  String keyFactoryAlgorithm,
                                  String signatureAlgorithm,
                                  byte[] unencryptedBytes)
    {
        String result = null;

        try
        {
            PrivateKey privateKey = (keyFactoryAlgorithm == null ? readPrivateKey(privateKeyData) : readPrivateKey(privateKeyData, keyFactoryAlgorithm));
            result = signData(privateKey, signatureAlgorithm, unencryptedBytes);
        }

        catch (Exception exception)
        {
            Log.e(CommonPublicKeyCryptographyHelper.class.getSimpleName(), "Unexpected error " + exception);
            exception.printStackTrace();
        }

        return result;
    }


    public static Boolean verifyData(PublicKey publicKey,
                                     String    signatureAlgorithm,
                                     byte[]    unencryptedBytes,
                                     byte[]    signatureBytes)
    {
        Boolean result = null;

        try
        {
            Signature signature = Signature.getInstance(signatureAlgorithm);
            signature.initVerify(publicKey);
            signature.update(unencryptedBytes);

            result = signature.verify(signatureBytes);
        }

        catch (Exception exception)
        {
            Log.e(CommonPublicKeyCryptographyHelper.class.getSimpleName(), "Unexpected error " + exception);
            exception.printStackTrace();
        }

        return result;
    }


    public static Boolean verifyData(String publicKeyData,
                                     String keyFactoryAlgorithm,
                                     String signatureAlgorithm,
                                     byte[] unencryptedBytes,
                                     byte[] signatureBytes)
    {
        Boolean result = null;

        try
        {
            PublicKey publicKey = (keyFactoryAlgorithm == null ? readPublicKey(publicKeyData) : readPublicKey(publicKeyData, keyFactoryAlgorithm));
            result = verifyData(publicKey, signatureAlgorithm, unencryptedBytes, signatureBytes);
        }

        catch (Exception exception)
        {
            Log.e(CommonPublicKeyCryptographyHelper.class.getSimpleName(), "Unexpected error " + exception);
            exception.printStackTrace();
        }

        return result;
    }


    public static String signJwtData(PrivateKey          privateKey,
                                     Map<String, Object> jwtHeader,
                                     Map<String, Object> jwtPayload)
    {
        JwtBuilder jwtBuilder = Jwts.builder();

        return jwtBuilder.setHeader(jwtHeader).setClaims(jwtPayload).signWith(privateKey).compact();
    }


    public static String signJwtData(String              privateKeyData,
                                     String              keyFactoryAlgorithm,
                                     Map<String, Object> jwtHeader,
                                     Map<String, Object> jwtPayload)
    {
        String result = null;

        try
        {
            PrivateKey privateKey = (keyFactoryAlgorithm == null ? readPrivateKey(privateKeyData) : readPrivateKey(privateKeyData, keyFactoryAlgorithm));
            result = signJwtData(privateKey, jwtHeader, jwtPayload);
        }

        catch (Exception exception)
        {
            Log.e(CommonPublicKeyCryptographyHelper.class.getSimpleName(), "Unexpected error " + exception);
            exception.printStackTrace();
        }

        return result;
    }
}