package common.cryptography;


import android.content.Context;
import android.util.Base64;
import android.util.Log;
import org.json.JSONObject;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;


import common.commonUtilities.CommonUtilities;


public class CommonAESCryptographyHelper
{
    private static final int    SALT_LENGTH = 256;

    private static final int    PB_KEY_SPEC_ITERATIONS = 1000;
    private static final int    PB_KEY_SPEC_LENGTH = 256;

    private static final String SECRET_KEY_FACTORY_ALGORITHM = "PBKDF2WithHmacSHA1";

    private static final String SECRET_KEY_SPEC_ALGORITHM = "AES";

    private static final int    IV_LENGTH = 16;

    private static final String CIPHER_TRANSFORMATION = "AES/CBC/PKCS7Padding";

    private static final String DATA_CONTAINER_SALT_LABEL = "salt";
    private static final String DATA_CONTAINER_IV_LABEL = "iv";
    private static final String DATA_CONTAINER_ENCRYPTED_BYTES_LABEL = "encryptedBytes";


    public static JSONObject encryptData(String password,
                                         byte[] unencryptedBytes)
    {
        JSONObject dataContainer = null;

        try
        {
            byte[] salt = new byte[SALT_LENGTH];
            SecureRandom secureRandom = new SecureRandom();
            secureRandom.nextBytes(salt);

            //Derive the key from the password
            PBEKeySpec pbKeySpec = new PBEKeySpec(password.toCharArray(), salt, PB_KEY_SPEC_ITERATIONS, PB_KEY_SPEC_LENGTH);
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(SECRET_KEY_FACTORY_ALGORITHM);
            byte[] secretKey = secretKeyFactory.generateSecret(pbKeySpec).getEncoded();
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey, SECRET_KEY_SPEC_ALGORITHM);

            //Create initialization vector for AES
            byte[] iv = new byte[IV_LENGTH];
            secureRandom = new SecureRandom();
            secureRandom.nextBytes(iv);
            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

            Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);

            //Encrypt
            byte[] encryptedBytes = cipher.doFinal(unencryptedBytes);

            dataContainer = new JSONObject();
            dataContainer.put(DATA_CONTAINER_SALT_LABEL, Base64.encodeToString(salt, Base64.NO_WRAP));
            dataContainer.put(DATA_CONTAINER_IV_LABEL, Base64.encodeToString(iv, Base64.NO_WRAP));
            dataContainer.put(DATA_CONTAINER_ENCRYPTED_BYTES_LABEL, Base64.encodeToString(encryptedBytes, Base64.NO_WRAP));
        }

        catch (Exception exception)
        {
            Log.e(CommonAESCryptographyHelper.class.getSimpleName(), "Unexpected error " + exception);
            exception.printStackTrace();
        }

        return dataContainer;
    }


    public static boolean encryptData(Context context,
                                      String  id,
                                      String  password,
                                      byte[]  unencryptedBytes)
    {
        JSONObject dataContainer = encryptData(password, unencryptedBytes);

        if (dataContainer == null)
        {
            return false;
        }

        CommonUtilities.setPreference(context, id, dataContainer.toString());

        return true;
    }


    public static byte[] decryptData(String     password,
                                     JSONObject dataContainer)
    {
        byte[] unencryptedBytes = null;

        try
        {
            byte[] salt = Base64.decode((String) dataContainer.get(DATA_CONTAINER_SALT_LABEL), Base64.NO_WRAP);
            byte[] iv = Base64.decode((String) dataContainer.get(DATA_CONTAINER_IV_LABEL), Base64.NO_WRAP);
            byte[] encryptedBytes = Base64.decode((String) dataContainer.get(DATA_CONTAINER_ENCRYPTED_BYTES_LABEL), Base64.NO_WRAP);

            //Regenerate key from password
            PBEKeySpec pbKeySpec = new PBEKeySpec(password.toCharArray(), salt, PB_KEY_SPEC_ITERATIONS, PB_KEY_SPEC_LENGTH);
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(SECRET_KEY_FACTORY_ALGORITHM);
            byte[] secretKey = secretKeyFactory.generateSecret(pbKeySpec).getEncoded();
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey, SECRET_KEY_SPEC_ALGORITHM);

            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

            //Decrypt
            Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);

            unencryptedBytes = cipher.doFinal(encryptedBytes);
        }

        catch (Exception exception)
        {
            Log.e(CommonAESCryptographyHelper.class.getSimpleName(), "Unexpected error " + exception);
            exception.printStackTrace();
        }

        return unencryptedBytes;
    }


    public static byte[] decryptData(Context context,
                                     String  id,
                                     String  password)
    {

        try
        {
            JSONObject dataContainer = new JSONObject(CommonUtilities.getPreference(context, id));
            return decryptData(password, dataContainer);
        }

        catch (Exception exception)
        {
            return null;
        }
    }
}