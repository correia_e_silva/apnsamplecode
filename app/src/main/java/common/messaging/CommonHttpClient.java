package common.messaging;


import android.util.Log;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;


import common.CommonConstants;


public class CommonHttpClient implements CommonConstants
{
    private static final int BUFFER_SIZE = 4096;


    private static void setupConnection(HttpURLConnection httpURLConnection,
                                        String            requestMethod) throws Exception
    {

        httpURLConnection.setConnectTimeout(SRVR_CONNECTION_TIMEOUT);
        httpURLConnection.setReadTimeout(SRVR_INACTIVITY_TIMEOUT);

        if (requestMethod != null)
        {
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setRequestMethod(requestMethod);
        }
    }


    private static String getServerResponse(InputStream inputStream) throws Exception
    {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String dataRead;

        while ((dataRead = bufferedReader.readLine()) != null)
        {
            stringBuilder.append(dataRead).append(HTTP_STRING_LF);
        }

        inputStream.close();
        bufferedReader.close();

        String result = stringBuilder.toString();

        Log.d(CommonHttpClient.class.getSimpleName(), "Server response (" + result + ")");

        return result;
    }


    public static void sendMessage(String              address,
                                   String              message,
                                   Map<String, String> headers) throws Exception
    {

        Log.d(CommonHttpClient.class.getSimpleName(), "Will attempt to send message to " + address);

        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(address).openConnection();

        setupConnection(httpURLConnection, "POST");

        if ((headers != null) &&
            !headers.isEmpty())
        {
            for (Map.Entry<String, String> header : headers.entrySet())
            {
                httpURLConnection.setRequestProperty(header.getKey(), header.getValue());
            }
        }

        httpURLConnection.connect();

        OutputStream outputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
        outputStream.write(message.getBytes());
        outputStream.flush();
        outputStream.close();

        int statusCode = httpURLConnection.getResponseCode();

        try
        {
            if (statusCode == HttpURLConnection.HTTP_OK)
            {
                Log.d(CommonHttpClient.class.getSimpleName(), "Message sent (" + address + ")");
            }
            else
            {
                String errorDescription = "Error " +
                                          statusCode +
                                          " sending message (" +
                                          address +
                                          ") " +
                                          getServerResponse(httpURLConnection.getErrorStream());
                Log.e(CommonHttpClient.class.getSimpleName(), errorDescription);
                throw new RuntimeException(errorDescription);
            }
        }

        finally
        {
            httpURLConnection.disconnect();
        }
    }


    private static void addHeaderField(DataOutputStream dataOutputStream,
                                       String           name,
                                       String           value) throws Exception
    {

        dataOutputStream.writeBytes(name + ": " + value + "\""+ HTTP_STRING_CRLF);
        dataOutputStream.flush();
    }


    private static void addFormField(DataOutputStream dataOutputStream,
                                     String           name,
                                     String           value) throws Exception
    {

        dataOutputStream.writeBytes(HTTP_STRING_TWO_HYPHENS + HTTP_STRING_BOUNDARY + HTTP_STRING_CRLF);
        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + name + "\""+ HTTP_STRING_CRLF);
        dataOutputStream.writeBytes("Content-Type: text/plain; charset=UTF-8" + HTTP_STRING_CRLF);
        dataOutputStream.writeBytes(HTTP_STRING_CRLF);
        dataOutputStream.writeBytes(value + HTTP_STRING_CRLF);
        dataOutputStream.flush();
    }


    private static int addFile(DataOutputStream dataOutputStream,
                               String           fieldName,
                               String           filePath,
                               String           fileName) throws Exception
    {

        dataOutputStream.writeBytes(HTTP_STRING_TWO_HYPHENS + HTTP_STRING_BOUNDARY + HTTP_STRING_CRLF);
        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + fieldName + "\";filename=\"" + fileName + "\"" + HTTP_STRING_CRLF);
        dataOutputStream.writeBytes(HTTP_STRING_CRLF);
        dataOutputStream.flush();

        FileInputStream fileInputStream = new FileInputStream(new File(filePath + File.separator + fileName));
        byte[] buffer = new byte[BUFFER_SIZE];
        int totalBytes = 0;
        int bytesRead;

        while ((bytesRead = fileInputStream.read(buffer)) != -1)
        {
            dataOutputStream.write(buffer, 0, bytesRead);
            totalBytes += bytesRead;
        }

        dataOutputStream.flush();
        fileInputStream.close();

        return totalBytes;
    }


    public static void uploadFile(String              address,
                                  String              inputFilesPath,
                                  String              outputFilesPath,
                                  String              fileName,
                                  Map<String, String> headers) throws Exception
    {

        Log.d(CommonHttpClient.class.getSimpleName(), "Will attempt to upload file '" + inputFilesPath + File.separator + fileName + "' to " + address);

        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(address + ((outputFilesPath == null) ?  "" : File.separator + outputFilesPath)).openConnection();
        httpURLConnection.setUseCaches(false);

        setupConnection(httpURLConnection, "POST");

        if ((headers != null) &&
            !headers.isEmpty())
        {
            for (Map.Entry<String, String> header : headers.entrySet())
            {
                httpURLConnection.setRequestProperty(header.getKey(), header.getValue());
            }
        }

        DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
        int totalBytes = addFile(dataOutputStream, fileName, inputFilesPath, fileName);

        dataOutputStream.writeBytes(HTTP_STRING_CRLF);
        dataOutputStream.writeBytes(HTTP_STRING_TWO_HYPHENS + HTTP_STRING_BOUNDARY + HTTP_STRING_TWO_HYPHENS + HTTP_STRING_CRLF);
        dataOutputStream.flush();
        dataOutputStream.close();

        int statusCode = httpURLConnection.getResponseCode();

        try
        {
            if (statusCode == HttpURLConnection.HTTP_OK)
            {
                Log.d(CommonHttpClient.class.getSimpleName(), "Uploaded file (" + inputFilesPath + File.separator + fileName + ") with " + totalBytes + " bytes");
            }
            else
            {
                String errorDescription = "Error " +
                                          statusCode +
                                          " uploading file (" +
                                          inputFilesPath +
                                          File.separator +
                                          fileName +
                                          ") " +
                                          getServerResponse(httpURLConnection.getErrorStream());
                Log.e(CommonHttpClient.class.getSimpleName(), errorDescription);
                throw new RuntimeException(errorDescription);
            }
        }

        finally
        {
            httpURLConnection.disconnect();
        }
    }


    public static String receiveMessage(String address) throws Exception
    {

        Log.d(CommonHttpClient.class.getSimpleName(), "Will attempt to receive message from " + address);

        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(address).openConnection();

        setupConnection(httpURLConnection, null);

        int statusCode = httpURLConnection.getResponseCode();

        try
        {
            String result;

            if (statusCode == HttpURLConnection.HTTP_OK)
            {
                result = getServerResponse(httpURLConnection.getInputStream());
                Log.d(CommonHttpClient.class.getSimpleName(), "Received message (" + address + ")");
                return result;
            }
            else
            {
                result = "Error " +
                         statusCode +
                         " receiving message (" +
                         address +
                         ") " +
                         getServerResponse(httpURLConnection.getErrorStream());
                Log.e(CommonHttpClient.class.getSimpleName(), result);
                throw new RuntimeException(result);
            }
        }

        finally
        {
            httpURLConnection.disconnect();
        }
    }


    public static void downloadFile(String              address,
                                    String              inputFilesPath,
                                    String              outputFilesPath,
                                    String              fileName,
                                    Map<String, String> headers,
                                    boolean             isRedirected) throws Exception
    {
        String fullAddress = (isRedirected ? address
                                           : address + (inputFilesPath == null ? "" : File.separator + inputFilesPath + File.separator + fileName));

        Log.d(CommonHttpClient.class.getSimpleName(), "Will attempt to download file '" + fullAddress + "'");

        File auxOutputFilePath = new File(outputFilesPath);

        if (!auxOutputFilePath.exists() &&
            !auxOutputFilePath.mkdirs())
        {
            String errorDescription = "Error accessing or creating directory (" + auxOutputFilePath.getAbsolutePath() + ")";
            Log.e(CommonHttpClient.class.getSimpleName(), errorDescription);
            throw new RuntimeException(errorDescription);
        }

        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(fullAddress).openConnection();
        httpURLConnection.setInstanceFollowRedirects(true);
        httpURLConnection.setFollowRedirects(true);

        setupConnection(httpURLConnection, null);

        if ((headers != null) &&
            !headers.isEmpty())
        {
            for (Map.Entry<String, String> header : headers.entrySet())
            {
                httpURLConnection.setRequestProperty(header.getKey(), header.getValue());
            }
        }

        int statusCode = httpURLConnection.getResponseCode();

        try
        {
            if (statusCode == HttpURLConnection.HTTP_OK)
            {
                InputStream inputStream = httpURLConnection.getInputStream();
                byte[] buffer = new byte[BUFFER_SIZE];
                FileOutputStream fileOutputStream = new FileOutputStream(outputFilesPath + File.separator + fileName);
                int totalBytes = 0;
                int bytesRead;

                while ((bytesRead = inputStream.read(buffer)) != -1)
                {
                    fileOutputStream.write(buffer, 0, bytesRead);
                    totalBytes += bytesRead;
                }

                inputStream.close();
                fileOutputStream.close();

                Log.d(CommonHttpClient.class.getSimpleName(), "Downloaded file (" + fullAddress + ") with " + totalBytes + " bytes");
            }
            else if ((statusCode == HttpURLConnection.HTTP_MOVED_TEMP) ||
                     (statusCode == HttpURLConnection.HTTP_MOVED_PERM) ||
                     (statusCode == HttpURLConnection.HTTP_SEE_OTHER))
            {
                String redirectedAddress = httpURLConnection.getHeaderField("Location");
                Log.w(CommonHttpClient.class.getSimpleName(), "Redirection " + statusCode + " while downloading file (" + fullAddress + ") " + redirectedAddress);
                downloadFile(redirectedAddress, inputFilesPath, outputFilesPath, fileName, headers, true);
            }
            else
            {
                String errorDescription = "Error " +
                                          statusCode +
                                          " downloading file (" +
                                          fullAddress +
                                          ") " +
                                          getServerResponse(httpURLConnection.getErrorStream());
                Log.e(CommonHttpClient.class.getSimpleName(), errorDescription);
                throw new RuntimeException(errorDescription);
            }
        }

        finally
        {
            httpURLConnection.disconnect();
        }
    }
}