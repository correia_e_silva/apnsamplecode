package common.messaging;


import android.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import org.json.JSONObject;


import common.CommonConstants;
import common.commonUtilities.CommonUtilities;


public abstract class CommonFirebaseMessagingService extends FirebaseMessagingService implements CommonConstants
{


    @Override
    public void onNewToken(String token)
    {

        super.onNewToken(token);

        Log.d(getClass().getSimpleName(), "Token: " + token);

        CommonUtilities.setPreference(this, FIREBASE_REFRESHED_TOKEN_TAG, token);
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {

        try
        {
            if (remoteMessage == null)
            {
                return;
            }

            Log.d(getClass().getSimpleName(), "Message source: " + remoteMessage.getFrom());

            String messageTitle = null;
            String messageBody = null;
            JSONObject messageFields = null;

            if (remoteMessage.getNotification() != null)
            {
                Log.d(getClass().getSimpleName(), "Message notification title: " + remoteMessage.getNotification().getTitle() + ", body: " + remoteMessage.getNotification().getBody());
                messageTitle = remoteMessage.getNotification().getTitle();
                messageBody = remoteMessage.getNotification().getBody();
            }

            if (remoteMessage.getData().size() > 0)
            {
                Log.d(getClass().getSimpleName(), "Message fields: " + remoteMessage.getData());
                messageFields = new JSONObject(remoteMessage.getData().toString());

            }

            handleMessageData(FIREBASE_PUSH_NOTIFICATION_DATA_INTENT_LABEL, messageTitle, messageBody, messageFields);
        }

        catch (Exception exception)
        {
            Log.e(getClass().getSimpleName(), "Unexpected error " + exception);
            handleMessageData(FIREBASE_PUSH_NOTIFICATION_ERROR_INTENT_LABEL, null, exception.toString(), null);
            exception.printStackTrace();
        }
    }


    @Override
    public void onDeletedMessages()
    {
    }


    protected abstract void handleMessageData(String     intentLabel,
                                              String     messageTitle,
                                              String     messageBody,
                                              JSONObject messageFields);
}