package common.messaging


import android.os.Handler
import android.os.Looper
import android.util.Log
import com.microsoft.signalr.HttpHubConnectionBuilder
import com.microsoft.signalr.HubConnection
import com.microsoft.signalr.HubConnectionBuilder
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import java.util.concurrent.Future


class CommonSignalR private constructor()
{
    enum class MessageType
    {
        SIGNALR_PUSH_NOTIFICATION_CONNECTION_LABEL,
        SIGNALR_PUSH_NOTIFICATION_MESSAGE_LABEL
    }


    companion object
    {
        private var mCommonSignalR: CommonSignalR? = null

        @get:Synchronized
        val instance:               CommonSignalR
            get()
            {

                if (mCommonSignalR == null)
                {
                    mCommonSignalR = CommonSignalR()
                }

                return mCommonSignalR!!
            }
    }


    private var mUrl:                    String? = null
    private var mHubConnection:          HubConnection? = null
    private var mCommonSignalRCallbacks: MutableSet<CommonSignalRCallback>?


    interface CommonSignalRCallback
    {


        fun onComplete(messageType: MessageType,
                       result:      Boolean,
                       data:        Any?)
    }


    init
    {

        mCommonSignalRCallbacks = HashSet<CommonSignalRCallback>()
    }


    fun addCallback(commonSignalRCallback: CommonSignalRCallback?)
    {

        if (commonSignalRCallback != null)
        {
            mCommonSignalRCallbacks!!.add(commonSignalRCallback);
        }
    }


    fun removeCallback(commonSignalRCallback: CommonSignalRCallback?)
    {

        if (commonSignalRCallback != null)
        {
            mCommonSignalRCallbacks!!.remove(commonSignalRCallback);
        }
    }


    private fun signalCompletion(messageType:              MessageType,
                                 result:                   Boolean,
                                 data:                     Any?,
                                 signalCallbackOnUiThread: Boolean)
    {

        for (commonSignalRCallback in mCommonSignalRCallbacks!!)
        {
            if (signalCallbackOnUiThread)
            {
                Handler(Looper.getMainLooper()).post()
                {

                    commonSignalRCallback.onComplete(messageType, result, data)
                }
            }
            else
            {
                commonSignalRCallback.onComplete(messageType, result, data)
            }
        }
    }


    fun initialize(url:   String,
                   token: String?): Future<Boolean>
    {

        return Executors.newSingleThreadExecutor().submit<Boolean>(Callable<Boolean>
        {

            Log.d(javaClass.simpleName, "Will attempt to connect to $mUrl")

            mUrl = url;

            val httpHubConnectionBuilder: HttpHubConnectionBuilder = HubConnectionBuilder.create(mUrl)

            token?.let()
            {
                httpHubConnectionBuilder.withHeader("Authorization", token)
            }

            mHubConnection = httpHubConnectionBuilder.build().apply()
            {
                start().doOnComplete()
                {
                    Log.d(javaClass.simpleName, "Connected to $mUrl")

                    signalCompletion(MessageType.SIGNALR_PUSH_NOTIFICATION_CONNECTION_LABEL, true, null, true)
                }.doOnError()
                {
                    Log.e(javaClass.simpleName, "Error connecting to $mUrl ($it)")

                    signalCompletion(MessageType.SIGNALR_PUSH_NOTIFICATION_CONNECTION_LABEL, false, it, true)
                }.blockingAwait()
                onClosed()
                {
                    Log.d(javaClass.simpleName, "Connection to $mUrl was closed ($it)")
                }
            }

            true
        })
    }


    fun receiveMessages()
    {

        Log.d(javaClass.simpleName, "Will attempt to receive messages from $mUrl")

        mHubConnection!!.on("Send",
                            {
                                Log.d(javaClass.simpleName, "Received message from $mUrl")

                                signalCompletion(MessageType.SIGNALR_PUSH_NOTIFICATION_MESSAGE_LABEL, true, it, true)
                            },
                            String.Companion::class.java)
    }


    fun sendMessage(message: String)
    {

        Log.d(javaClass.simpleName, "Will attempt to send message to $mUrl")

        try
        {
            mHubConnection!!.send("Send", message)
        }

        catch (exception: Exception)
        {
            val errorDescription: String = "Error sending message to $mUrl (${exception.message})"
            exception.printStackTrace()
            Log.e(javaClass.simpleName, errorDescription)
            throw RuntimeException(errorDescription)
        }

        Log.d(javaClass.simpleName, "Message sent to $mUrl")
    }


    fun release()
    {

        mHubConnection?.run()
        {
            close()
        }
    }


    @Throws(CloneNotSupportedException::class)
    fun clone(): Any?
    {

        throw CloneNotSupportedException("Clone is not allowed.")
    }
}