package common.messaging;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.util.HashSet;
import java.util.Set;


import common.CommonConstants;


public class CommonFirebaseMessagingBroadcastReceiver extends BroadcastReceiver implements CommonConstants
{
    public enum FIREBASE_PUSH_NOTIFICATION_TYPE
    {
        FIREBASE_PUSH_NOTIFICATION_JSON_DATA(FIREBASE_PUSH_NOTIFICATION_DATA_INTENT_LABEL),
        FIREBASE_PUSH_NOTIFICATION_ERROR_DATA(FIREBASE_PUSH_NOTIFICATION_ERROR_INTENT_LABEL);


        private String label;


        FIREBASE_PUSH_NOTIFICATION_TYPE(String label)
        {

            this.label = label;
        }


        public static FIREBASE_PUSH_NOTIFICATION_TYPE fromLabel(String label)
        {

            for (FIREBASE_PUSH_NOTIFICATION_TYPE firebasePushNotificationType : FIREBASE_PUSH_NOTIFICATION_TYPE.values())
            {
                if (firebasePushNotificationType.label.equals(label))
                {
                    return firebasePushNotificationType;
                }
            }

            return null;
        }
    }


    private static CommonFirebaseMessagingBroadcastReceiver mCommonFirebaseMessagingBroadcastReceiver = null;
    private Set<FirebaseMessagingCallback>                  mFirebaseMessagingCallbacks;


    public interface FirebaseMessagingCallback
    {


        public void onPushNotification(FIREBASE_PUSH_NOTIFICATION_TYPE messageType,
                                       String                          messageData);
    }


    private CommonFirebaseMessagingBroadcastReceiver()
    {

        mFirebaseMessagingCallbacks = new HashSet<FirebaseMessagingCallback>();
    }


    public static synchronized CommonFirebaseMessagingBroadcastReceiver getInstance()
    {

        if (mCommonFirebaseMessagingBroadcastReceiver == null)
        {
            mCommonFirebaseMessagingBroadcastReceiver = new CommonFirebaseMessagingBroadcastReceiver();
        }

        return mCommonFirebaseMessagingBroadcastReceiver;
    }


    public void addCallback(FirebaseMessagingCallback firebaseMessagingCallback)
    {

        if (firebaseMessagingCallback != null)
        {
            mFirebaseMessagingCallbacks.add(firebaseMessagingCallback);
        }
    }


    public void removeCallback(FirebaseMessagingCallback firebaseMessagingCallback)
    {

        if (firebaseMessagingCallback != null)
        {
            mFirebaseMessagingCallbacks.remove(firebaseMessagingCallback);
        }
    }


    private void signalPushNotification(FIREBASE_PUSH_NOTIFICATION_TYPE messageType,
                                        String                          messageData)
    {

        for (FirebaseMessagingCallback firebaseMessagingCallback : mFirebaseMessagingCallbacks)
        {
            firebaseMessagingCallback.onPushNotification(messageType, messageData);
        }
    }


    @Override
    public void onReceive(Context context,
                          Intent  intent)
    {

        if (intent.getAction().equals(FIREBASE_PUSH_NOTIFICATION_DATA_INTENT_LABEL) ||
            intent.getAction().equals(FIREBASE_PUSH_NOTIFICATION_ERROR_INTENT_LABEL))
        {
            signalPushNotification(FIREBASE_PUSH_NOTIFICATION_TYPE.fromLabel(intent.getAction()), intent.getStringExtra(INTENT_PARAMS_STRING_LABEL));
        }
    }


    @Override
    protected Object clone() throws CloneNotSupportedException
    {

        throw new CloneNotSupportedException("Clone is not allowed.");
    }
}