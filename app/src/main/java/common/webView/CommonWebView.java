package common.webView;


import android.annotation.TargetApi;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import common.CommonConstants;
import common.commonUtilities.CommonUtilities;


public abstract class CommonWebView extends AppCompatActivity implements CommonConstants
{
    private String  mUrl;
    private WebView mWebView;


    public class WebViewInterface
    {


        public WebViewInterface()
        {
        }


        @JavascriptInterface
        public void javascriptCallbackMethod(String data)
        {

            handleJavascriptCallbackMethod(data);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);

        CommonUtilities.adjustFontScale(this, getResources().getConfiguration());

        Bundle extras = getIntent().getExtras();
        mUrl = extras.getString(INTENT_PARAMS_STRING_LABEL);

        final SwipeRefreshLayout swipeRefreshLayout = new SwipeRefreshLayout(this);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {


            @Override
            public void onRefresh()
            {

                swipeRefreshLayout.setRefreshing(true);

                mWebView.reload();
            }
        });

        mWebView = new WebView(this);
        mWebView.setWebViewClient(new WebViewClient()
        {


            @Override
            public void onReceivedError(WebView view,
                                        int     errorCode,
                                        String  description,
                                        String  failingUrl)
            {

                Log.e(getClass().getSimpleName(), "Unexpected error " + description);

                super.onReceivedError(view, errorCode, description, failingUrl);
            }


            @Override
            @TargetApi(23)
            public void onReceivedError(WebView            view,
                                        WebResourceRequest request,
                                        WebResourceError   error)
            {

                Log.e(getClass().getSimpleName(), "Unexpected error " + error.getDescription());

                super.onReceivedError(view, request, error);
            }


            @Override
            public void onReceivedSslError(WebView         view,
                                           SslErrorHandler handler,
                                           SslError        error)
            {

                Log.e(getClass().getSimpleName(), "Unexpected error " + error.toString());

                super.onReceivedSslError(view, handler, error);
            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view,
                                                    String  url)
            {

                return false;
            }


            @Override
            @TargetApi(24)
            public boolean shouldOverrideUrlLoading(WebView            view,
                                                    WebResourceRequest request)
            {

                return false;
            }


            @Override
            public void onPageFinished(WebView view,
                                       String  url)
            {

                swipeRefreshLayout.setRefreshing(false);

                super.onPageFinished(view, url);
            }
        });
        mWebView.addJavascriptInterface(new WebViewInterface(), WebViewInterface.class.getSimpleName());

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setDomStorageEnabled(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);

        swipeRefreshLayout.addView(mWebView);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(swipeRefreshLayout);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().getAttributes().layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
        {
            getSupportActionBar().hide();
        }
    }


    @Override
    public void onStart()
    {

        super.onStart();

        mWebView.loadUrl(mUrl);
    }


    @Override
    public boolean onKeyDown(int      keyCode,
                             KeyEvent event)
    {

        if ((keyCode == KeyEvent.KEYCODE_BACK) &&
            mWebView.canGoBack())
        {
            mWebView.goBack();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }


    protected abstract void handleJavascriptCallbackMethod(String data);
}