package common.commonUtilities;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Environment;
import android.os.LocaleList;
import android.os.Parcelable;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.Toast;
import androidx.core.content.FileProvider;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import java.io.*;
import java.lang.Math;
import java.lang.Runtime;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;


import common.CommonConstants;
import common.messaging.CommonHttpClient;


public class CommonUtilities implements CommonConstants
{
 private static final String              TAG = CommonUtilities.class.getSimpleName();

 private static final Map<String, String> LOCALE_LANGUAGES = new HashMap<String, String>();

 static
 {
  LOCALE_LANGUAGES.put("", EN_LABEL);
  LOCALE_LANGUAGES.put(ENGLISH_LABEL, EN_LABEL);
  LOCALE_LANGUAGES.put(FRENCH_LABEL, FR_LABEL);
  LOCALE_LANGUAGES.put(PORTUGUESE_LABEL, PT_LABEL);
 }


 private static String                    lastYYYYMMDDHHMMSSDate = null;
 private static Process                   logSavingProcess = null;


 public enum PathMode
 {
  Unknown("unknown"),
  Driving("driving"),//Driving
  Bicycling("bicycling"),//Bicycling
  Transit("transit"),//Public transport
  Walking("walking");//Walking


  private final String description;


  PathMode(String description)
  {

   this.description = description;
  }


  public static PathMode fromValue(int value)
  {

   if ((value >= 0) &&
       (value < values().length))
   {
    return (values()[value]);
   }

   return PathMode.Unknown;
  }


  public static PathMode fromDescription(String description)
  {

   for (PathMode pathMode : PathMode.values())
   {
    if (pathMode.description.equals(description))
    {
     return pathMode;
    }
   }

   return PathMode.Unknown;
  }
 }


 public static boolean isPntCoordGeodesic(String coord,
                                          int    totalChars)
 {
  int startCharIndex, endCharIndex;
  Object minValue, maxValue;

  if (coord.length() == totalChars)
  {
   for (int i = (totalChars == TOTAL_GEOD_PNT_LAT_CHARS ? GEOD_PNT_LAT_DEG_INDEX : GEOD_PNT_LONG_DEG_INDEX);
        i <= (totalChars == TOTAL_GEOD_PNT_LAT_CHARS ? GEOD_PNT_LAT_DIR_INDEX : GEOD_PNT_LONG_DIR_INDEX);
        i ++)
   {
    startCharIndex = ((Integer) GEOD_PNT_PROPS[i][GEOD_PNT_PROPS_START_CHAR_INDEX]) - (totalChars == TOTAL_GEOD_PNT_LAT_CHARS ? 0 : TOTAL_GEOD_PNT_LAT_CHARS);
    endCharIndex = ((Integer) GEOD_PNT_PROPS[i][GEOD_PNT_PROPS_END_CHAR_INDEX]) - (totalChars == TOTAL_GEOD_PNT_LAT_CHARS ? 0 : TOTAL_GEOD_PNT_LAT_CHARS);

    minValue = GEOD_PNT_PROPS[i][GEOD_PNT_PROPS_MIN_VALUE_INDEX];
    maxValue = GEOD_PNT_PROPS[i][GEOD_PNT_PROPS_MAX_VALUE_INDEX];

    if ((i == GEOD_PNT_LAT_DIR_INDEX) ||
        (i == GEOD_PNT_LONG_DIR_INDEX))
    {
     if ((coord.substring(startCharIndex, endCharIndex).compareTo((String) minValue) != 0) &&
         (coord.substring(startCharIndex, endCharIndex).compareTo((String) maxValue) != 0))
     {
      return false;
     }
    }
    else
    {
     try
     {
      if ((Integer.parseInt(coord.substring(startCharIndex, endCharIndex)) < ((Integer) minValue)) ||
          (Integer.parseInt(coord.substring(startCharIndex, endCharIndex)) > ((Integer) maxValue)))
      {
       return false;
      }
     }
     catch (NumberFormatException exception)
     {
      return false;
     }
    }
   }
  }
  else
  {
   return false;
  }

  return true;
 }


 public static double cnvPntCoordToDeg(String coord)
 {
  double result;
  int totalCoordChars, coordStartCharIndex, coordEndCharIndex;
  Object coordMinValue, coordMaxValue;

  result = 0;

  totalCoordChars = coord.length();

  for (int i = (totalCoordChars == TOTAL_GEOD_PNT_LAT_CHARS ? GEOD_PNT_LAT_DEG_INDEX : GEOD_PNT_LONG_DEG_INDEX);
       i <= (totalCoordChars == TOTAL_GEOD_PNT_LAT_CHARS ? GEOD_PNT_LAT_DIR_INDEX : GEOD_PNT_LONG_DIR_INDEX);
       i ++)
  {
   coordStartCharIndex = ((Integer) GEOD_PNT_PROPS[i][GEOD_PNT_PROPS_START_CHAR_INDEX]) - (totalCoordChars == TOTAL_GEOD_PNT_LAT_CHARS ? 0 : TOTAL_GEOD_PNT_LAT_CHARS);
   coordEndCharIndex = ((Integer) GEOD_PNT_PROPS[i][GEOD_PNT_PROPS_END_CHAR_INDEX]) - (totalCoordChars == TOTAL_GEOD_PNT_LAT_CHARS ? 0 : TOTAL_GEOD_PNT_LAT_CHARS);

   coordMinValue = GEOD_PNT_PROPS[i][GEOD_PNT_PROPS_MIN_VALUE_INDEX];
   coordMaxValue = GEOD_PNT_PROPS[i][GEOD_PNT_PROPS_MAX_VALUE_INDEX];

   switch (i)
   {
    case GEOD_PNT_LAT_DEG_INDEX:
    case GEOD_PNT_LONG_DEG_INDEX:
                                  result = Integer.parseInt(coord.substring(coordStartCharIndex, coordEndCharIndex));
                                  break;
    case GEOD_PNT_LAT_MIN_INDEX:
    case GEOD_PNT_LONG_MIN_INDEX:
                                  result += (Integer.parseInt(coord.substring(coordStartCharIndex, coordEndCharIndex)) / (double) 60);
                                  break;
    case GEOD_PNT_LAT_SEC_INDEX:
    case GEOD_PNT_LONG_SEC_INDEX:
                                  result += (Float.parseFloat(coord.substring(coordStartCharIndex, coordEndCharIndex)) / (double) 3600);
                                  break;
    default:
             result = coord.substring(coordStartCharIndex, coordEndCharIndex).compareTo((String) coordMinValue) == 0 ? result : -result;
             break;
   }
  }

  return result;
 }


 public static String[] cnvDisplayPntCoordsToDbFormat(String coords)
 {
  String auxCoords;

  //Handle decimal places separator which depends of selected phone language
  auxCoords = coords.replaceAll(",", ".");

  return (auxCoords.replaceAll("\u00B0", "").replaceAll("\u2032", "").replaceAll("\n", VALUES_SEP).replaceAll(" ", "").split(VALUES_SEP));
 }


 public static String cnvDbPntCoordsToDisplayFormat(String lattd,
                                                    String longtd)
 {
  String result, coord;
  int coordStartCharIndex, coordEndCharIndex;

  result = " ";

  coord = lattd + longtd;

  for (int i = GEOD_PNT_LAT_DEG_INDEX;
       i <= GEOD_PNT_LONG_DIR_INDEX;
       i ++)
  {
   coordStartCharIndex = ((Integer) GEOD_PNT_PROPS[i][GEOD_PNT_PROPS_START_CHAR_INDEX]);
   coordEndCharIndex = ((Integer) GEOD_PNT_PROPS[i][GEOD_PNT_PROPS_END_CHAR_INDEX]);

   switch (i)
   {
    case GEOD_PNT_LAT_DEG_INDEX:
    case GEOD_PNT_LONG_DEG_INDEX:
                                  result += coord.substring(coordStartCharIndex, coordEndCharIndex) + "\u00B0 ";
                                  break;
    case GEOD_PNT_LAT_MIN_INDEX:
    case GEOD_PNT_LONG_MIN_INDEX:
                                  result += coord.substring(coordStartCharIndex, coordEndCharIndex) + "\u2032 ";
                                  break;
    case GEOD_PNT_LAT_SEC_INDEX:
    case GEOD_PNT_LONG_SEC_INDEX:
                                  result += coord.substring(coordStartCharIndex, coordEndCharIndex) + "\u2032\u2032 ";
                                  break;
    default:
             result += coord.substring(coordStartCharIndex, coordEndCharIndex) + (i == GEOD_PNT_LAT_DIR_INDEX ? "\n" : "");
             break;
   }
  }

  return result;
 }


 public static String cnvLocationPntCoordsToDisplayFormat(String lattd,
                                                          String longtd)
 {
  DecimalFormat degFormatter, minFormatter, secFormatter;
  String[] auxCoord;
  String result;

  degFormatter = new DecimalFormat("00");
  minFormatter = new DecimalFormat("00");
  secFormatter = new DecimalFormat("00.000");

  auxCoord = lattd.split(":");
  //Handle decimal places separator which depends of selected phone language
  for (int i = 0;
       i < auxCoord.length;
       i ++)
  {
   auxCoord[i] = auxCoord[i].replaceAll(",", ".");
  }

  result = " " +
           degFormatter.format(Float.valueOf(auxCoord[0].replaceAll("-", ""))) +
           "\u00B0 " +
           minFormatter.format(Float.valueOf(auxCoord[1])) +
           "\u2032 " +
           secFormatter.format(Float.valueOf(auxCoord[2])) +
           "\u2032\u2032 " +
           (auxCoord[0].charAt(0) == '-' ? "S" : "N");

  degFormatter = new DecimalFormat("000");

  auxCoord = longtd.split(":");
  //Handle decimal places separator which depends of selected phone language
  for (int i = 0;
       i < auxCoord.length;
       i ++)
  {
   auxCoord[i] = auxCoord[i].replaceAll(",", ".");
  }

  result += "\n" +
            degFormatter.format(Float.valueOf(auxCoord[0].replaceAll("-", ""))) +
            "\u00B0 " +
            minFormatter.format(Float.valueOf(auxCoord[1])) +
            "\u2032 " +
            secFormatter.format(Float.valueOf(auxCoord[2])) +
            "\u2032\u2032 " +
            (auxCoord[0].charAt(0) == '-' ? "W" : "E");

  return result;
 }


 public static String cnvDisplayDateToDbFormat(String date)
 {

  return (date.replaceAll(" ", "").replaceAll("/", "").replaceAll(":", ""));
 }


 public static String cnvDbDateToDisplayFormat(String date)
 {
  String result;
  int dateStartCharIndex, dateEndCharIndex;

  result = "";

  for (int i = DATE_YEAR_INDEX;
       i <= DATE_SEC_INDEX;
       i ++)
  {
   dateStartCharIndex = DATE_PROPS[i][DATE_PROPS_START_CHAR_INDEX];
   dateEndCharIndex = DATE_PROPS[i][DATE_PROPS_END_CHAR_INDEX];

   switch (i)
   {
    case DATE_YEAR_INDEX:
    case DATE_MONTH_INDEX:
                           result += date.substring(dateStartCharIndex, dateEndCharIndex) + "/";
                           break;
    case DATE_DAY_INDEX:
    case DATE_SEC_INDEX:
                         result += date.substring(dateStartCharIndex, dateEndCharIndex) + (i == DATE_DAY_INDEX ? " " : "");
                         break;
    default:
             result += date.substring(dateStartCharIndex, dateEndCharIndex) + ":";
             break;
   }
  }

  return result;
 }


 public static Calendar cnvDbDateToCalendarFormat(String date)
 {
  Calendar result;
  int dateStartCharIndex, dateEndCharIndex;

  result = Calendar.getInstance();

  for (int i = DATE_YEAR_INDEX;
       i <= DATE_SEC_INDEX;
       i ++)
  {
   dateStartCharIndex = DATE_PROPS[i][DATE_PROPS_START_CHAR_INDEX];
   dateEndCharIndex = DATE_PROPS[i][DATE_PROPS_END_CHAR_INDEX];

   switch (i)
   {
    case DATE_YEAR_INDEX:
                          result.set(Calendar.YEAR, Integer.parseInt(date.substring(dateStartCharIndex, dateEndCharIndex)));
                          break;
    case DATE_MONTH_INDEX:
                           result.set(Calendar.MONTH, Integer.parseInt(date.substring(dateStartCharIndex, dateEndCharIndex)) - 1);
                           break;
    case DATE_DAY_INDEX:
                         result.set(Calendar.DATE, Integer.parseInt(date.substring(dateStartCharIndex, dateEndCharIndex)));
                         break;
    case DATE_HOUR_INDEX:
                          result.set(Calendar.HOUR_OF_DAY, Integer.parseInt(date.substring(dateStartCharIndex, dateEndCharIndex)));
                          break;
    case DATE_MIN_INDEX:
                         result.set(Calendar.MINUTE, Integer.parseInt(date.substring(dateStartCharIndex, dateEndCharIndex)));
                         break;
    default:
             result.set(Calendar.SECOND, Integer.parseInt(date.substring(dateStartCharIndex, dateEndCharIndex)));
             break;
   }
  }

  result.set(Calendar.MILLISECOND, 0);

  return result;
 }


 public static String cnvCalendarDateToDbFormat(Calendar date)
 {

  return (date.get(Calendar.YEAR) +
          FULL_TIME_FLD_FORMATTER.format(date.get(Calendar.MONTH) + 1) +
          FULL_TIME_FLD_FORMATTER.format(date.get(Calendar.DATE)) +
          FULL_TIME_FLD_FORMATTER.format(date.get(Calendar.HOUR_OF_DAY)) +
          FULL_TIME_FLD_FORMATTER.format(date.get(Calendar.MINUTE)) +
          FULL_TIME_FLD_FORMATTER.format(date.get(Calendar.SECOND)));
 }


 public static String cnvCalendarDateToDisplayFormat(Calendar date)
 {

  return (cnvDbDateToDisplayFormat(cnvCalendarDateToDbFormat(date)));
 }


 public static Calendar cnvDisplayDateToCalendarFormat(String date)
 {

  return (cnvDbDateToCalendarFormat(cnvDisplayDateToDbFormat(date)));
 }


 public static String cnvDateToDisplayFormat(long date)
 {
  Calendar calendar;

  calendar = Calendar.getInstance();
  calendar.setTime(new Date(date));

  return CommonUtilities.cnvCalendarDateToDisplayFormat(calendar);
 }


 public static double[] vectAddition(double[] fstVect,
                                     double[] lstVect)
 {
  double result[] = new double[3];

  result[0] = fstVect[0] + lstVect[0];
  result[1] = fstVect[1] + lstVect[1];
  result[2] = fstVect[2] + lstVect[2];

  return result;
 }


 public static double[] vectSubtraction(double[] fstVect,
                                        double[] lstVect)
 {
  double result[] = new double[3];

  result[0] = fstVect[0] - lstVect[0];
  result[1] = fstVect[1] - lstVect[1];
  result[2] = fstVect[2] - lstVect[2];

  return result;
 }


 public static double vectDotProduct(double[] fstVect,
                                     double[] lstVect)
 {


  return (fstVect[0] * lstVect[0] + fstVect[1] * lstVect[1] + fstVect[2] * lstVect[2]);
 }


 public static double[] vectScalarProduct(double   scalar,
                                          double[] vect)
 {
  double result[] = new double[3];

  result[0] = scalar * vect[0];
  result[1] = scalar * vect[1];
  result[2] = scalar * vect[2];

  return result;
 }


 public static double vectLength(double[] vect)
 {

  return (Math.sqrt(Math.pow(vect[0], 2) + Math.pow(vect[1], 2) + Math.pow(vect[2], 2)));
 }


 public static double[] setInnerVect(double[] fstVect,
                                     double[] lstVect,
                                     double   totalDist,
                                     double   dist)
 {
  double alpha;
  double[] auxVect;

  alpha = dist / totalDist;

  auxVect = vectAddition(lstVect, fstVect);
  auxVect = vectScalarProduct(alpha, auxVect);

  return (vectAddition(fstVect, auxVect));
 }


 public static double distPntSegment(double[] pnt,
                                     double[] fstSegmentPnt,
                                     double[] sndSegmentPnt)
 {
  double[] v, w, Pb, auxVector;
  double c1, c2, b;

  v = vectSubtraction(sndSegmentPnt, fstSegmentPnt);
  w = vectSubtraction(pnt, fstSegmentPnt);

  c1 = vectDotProduct(w, v);
  if (c1 <= 0)
  {
   //Angle between a line passing through the point and the line segment first point is >= 90 degrees
   return -1;
  }

  c2 = vectDotProduct(v, v);
  if (c2 < c1)
  {
   //Angle between a line passing through the point and the line segment second point is > 90 degrees
   return -1;
  }

  b = c1 / c2;
  auxVector = vectScalarProduct(b, v);
  Pb = vectAddition(fstSegmentPnt, auxVector);
  auxVector = vectSubtraction(pnt, Pb);

  return (vectLength(auxVector));
 }


 public static double[] segmentNormals(double[] fstSegmentPnt,
                                       double[] sndSegmentPnt)
 {
  double[] segment, result;

  segment = vectSubtraction(sndSegmentPnt, fstSegmentPnt);

  result = new double[6];

  //First normal
  result[0] = -segment[1];
  result[1] = segment[0];
  result[2] = 0;

  //Second normal
  result[3] = segment[1];
  result[4] = -segment[0];
  result[5] = 0;

  return result;
 }


 public static double[] getSegmentNormals(double[] fstSegmentPnt,
                                          double[] sndSegmentPnt,
                                          double   segmentNormalLength)
 {
  double[] segmentNormals, fstSegmentNormal, sndSegmentNormal, result;
  double alpha;

  segmentNormals = segmentNormals(fstSegmentPnt, sndSegmentPnt);

  fstSegmentNormal = new double[3];
  fstSegmentNormal[0] = segmentNormals[0];
  fstSegmentNormal[1] = segmentNormals[1];
  fstSegmentNormal[2] = segmentNormals[2];

  sndSegmentNormal = new double[3];
  sndSegmentNormal[0] = segmentNormals[3];
  sndSegmentNormal[1] = segmentNormals[4];
  sndSegmentNormal[2] = segmentNormals[5];

  alpha = segmentNormalLength / vectLength(fstSegmentNormal);
  fstSegmentNormal = vectScalarProduct(alpha, fstSegmentNormal);
  fstSegmentNormal = vectAddition(fstSegmentPnt, fstSegmentNormal);

  alpha = segmentNormalLength / vectLength(sndSegmentNormal);
  sndSegmentNormal = vectScalarProduct(alpha, sndSegmentNormal);
  sndSegmentNormal = vectAddition(fstSegmentPnt, sndSegmentNormal);

  result = new double[6];

  //First normal
  result[0] = fstSegmentNormal[0];
  result[1] = fstSegmentNormal[1];
  result[2] = fstSegmentNormal[2];

  //Second normal
  result[3] = sndSegmentNormal[0];
  result[4] = sndSegmentNormal[1];
  result[5] = sndSegmentNormal[2];

  return result;
 }


 public static double[] normalizeCoords(double   maxSourceXYZ,
                                        double   maxTargetXYZ,
                                        double[] coords)
 {
  double result[] = new double[3];

  if ((coords[0] < -maxSourceXYZ) ||
      (coords[0] > maxSourceXYZ) ||
      (coords[1] < -maxSourceXYZ) ||
      (coords[1] > maxSourceXYZ) ||
      (coords[2] < -maxSourceXYZ) ||
      (coords[2] > maxSourceXYZ))
  {
   return null;
  }

  result[0] = (coords[0] < 0 ? coords[0] * -maxTargetXYZ / -maxSourceXYZ : coords[0] * maxTargetXYZ / maxSourceXYZ);
  result[1] = (coords[1] < 0 ? coords[1] * -maxTargetXYZ / -maxSourceXYZ : coords[1] * maxTargetXYZ / maxSourceXYZ);
  result[2] = (coords[2] < 0 ? coords[2] * -maxTargetXYZ / -maxSourceXYZ : coords[2] * maxTargetXYZ / maxSourceXYZ);

  return result;
 }


 public static boolean isPntInPolygon(double[]   pnt,
                                      double[][] polygon)
 {
  int i, j;
  boolean result;

  j = polygon.length - 1;

  result = false;

  for (i = 0;
       i < polygon.length;
       i ++)
  {
   if (((polygon[i][1] < pnt[1]) &&
        (polygon[j][1] >= pnt[1])) ||
       ((polygon[j][1] < pnt[1]) &&
        (polygon[i][1] >= pnt[1])))
   {
    if (polygon[i][0] + (pnt[1] - polygon[i][1]) / (polygon[j][1] - polygon[i][1]) * (polygon[j][0] - polygon[i][0]) < pnt[0])
    {
     result =! result;
    }
   }

   j = i;
  }

  return result;
 }


 public static Size getPreviewSize(Camera camera,
                                   int    targetWidth,
                                   int    targetHeight)
 {
  List<Size> supportedPreviewSizes;
  Size previewSize;
  double targetAspectRatio, minAspectRatioDiff, previewSizeAspectRatio, aspectRatioDiff;
  int maxPreviewSizeArea, previewSizeArea;

  supportedPreviewSizes = camera.getParameters().getSupportedPreviewSizes();

  previewSize = null;

  targetAspectRatio = (double) targetWidth / (double) targetHeight;

  minAspectRatioDiff = Double.MAX_VALUE;
  maxPreviewSizeArea = Integer.MIN_VALUE;

  //Try to get the largest preview size with an aspect ratio similar to target and with dimensions which fit as close as possible to target's dimensions
  for (int i = 0;
       i < supportedPreviewSizes.size();
       i ++)
  {
   previewSizeAspectRatio = (double) supportedPreviewSizes.get(i).width / (double) supportedPreviewSizes.get(i).height;
   previewSizeArea = supportedPreviewSizes.get(i).width * supportedPreviewSizes.get(i).height;

   aspectRatioDiff = Math.abs(previewSizeAspectRatio - targetAspectRatio);

   if ((aspectRatioDiff <= minAspectRatioDiff) &&
       (supportedPreviewSizes.get(i).width >= (targetWidth / 2)) &&
       (supportedPreviewSizes.get(i).height >= (targetHeight / 2)) &&
       (supportedPreviewSizes.get(i).width <= targetWidth) &&
       (supportedPreviewSizes.get(i).height <= targetHeight))
   {
    if (!((aspectRatioDiff == minAspectRatioDiff) &&
         (previewSizeArea <= maxPreviewSizeArea)))
    {
     minAspectRatioDiff = aspectRatioDiff;
     maxPreviewSizeArea = previewSizeArea;
     previewSize = supportedPreviewSizes.get(i);
    }
   }
  }

  if (previewSize == null)
  {
   maxPreviewSizeArea = Integer.MIN_VALUE;

   //Try to get the largest preview size with dimensions which fit as close as possible to target's dimensions
   for (int i = 0;
        i < supportedPreviewSizes.size();
        i ++)
   {
    previewSizeArea = supportedPreviewSizes.get(i).width * supportedPreviewSizes.get(i).height;

    if ((supportedPreviewSizes.get(i).width <= targetWidth) &&
        (supportedPreviewSizes.get(i).height <= targetHeight))
    {
     if (!(previewSizeArea <= maxPreviewSizeArea))
     {
      maxPreviewSizeArea = previewSizeArea;
      previewSize = supportedPreviewSizes.get(i);
     }
    }
   }
  }

  return previewSize;
 }


 public static void setFocusMode(Parameters parameters,
                                 String     focusMode)
 {

  if (parameters.getSupportedFocusModes().contains(focusMode))
  {
   parameters.setFocusMode(focusMode);
  }
  else
  {
   Log.e(TAG, "Unsupported focus mode " + focusMode);
  }
 }


 public static boolean supportsFlashModes(Parameters   parameters,
                                          List<String> flashModes)
 {
  List<String> supportedFlashModes;

  if ((supportedFlashModes = parameters.getSupportedFlashModes()) != null)
  {
   return supportedFlashModes.containsAll(flashModes);
  }

  return false;
 }


 private static Point getDisplaySizeLegacy(Context context)
 {
  Display display;
  Point point;

  point = new Point();

  display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
  display.getSize(point);

  return point;
 }


 @TargetApi(17)
 private static Point getDisplaySizeV17(Context context)
 {
  Display display;
  DisplayMetrics displayMetrics;
  Point point;

  displayMetrics = new DisplayMetrics();

  display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
  display.getRealMetrics(displayMetrics);

  point = new Point();
  point.x = displayMetrics.widthPixels;
  point.y = displayMetrics.heightPixels;

  return point;
 }


 public static Point getDisplaySize(Context context)
 {

  return (VERSION.SDK_INT < VERSION_CODES.JELLY_BEAN_MR1 ? getDisplaySizeLegacy(context) : getDisplaySizeV17(context));
 }


 public static int getOrientationMode(Context context)
 {
  Point point;

  point = getDisplaySize(context);

  return (point.x < point.y ? MODE_PORTRAIT : MODE_LANDSCAPE);
 }


 private static String getYYYYMMDDHHMMSSDate(Calendar calendar)
 {

  return calendar.get(Calendar.YEAR) +
         FULL_TIME_FLD_FORMATTER.format(calendar.get(Calendar.MONTH) + 1) +
         FULL_TIME_FLD_FORMATTER.format(calendar.get(Calendar.DATE)) +
         FULL_TIME_FLD_FORMATTER.format(calendar.get(Calendar.HOUR_OF_DAY)) +
         FULL_TIME_FLD_FORMATTER.format(calendar.get(Calendar.MINUTE)) +
         FULL_TIME_FLD_FORMATTER.format(calendar.get(Calendar.SECOND));
 }


 public static synchronized String getYYYYMMDDHHMMSSDate()
 {
  Calendar currentTime;
  String result;

  currentTime = Calendar.getInstance();

  result = getYYYYMMDDHHMMSSDate(currentTime);

  while ((lastYYYYMMDDHHMMSSDate != null) &&
         (lastYYYYMMDDHHMMSSDate.compareTo(result) >= 0))
  {
   currentTime.add(Calendar.SECOND, 1);
   result = getYYYYMMDDHHMMSSDate(currentTime);
  }

  lastYYYYMMDDHHMMSSDate = result;

  return result;
 }


 public static String getMapUrl(double pntLattd,
                                double pntLongtd,
                                String pntId)
 {

  return ("geo:0,0?" +
          "q=" + pntLattd + VALUES_SEP + pntLongtd +
          (pntId == null ? "" : "(" + pntId + ")"));
 }


 public static String getNavigationUrl(double pntLattd,
                                       double pntLongtd,
                                       String pntId)
 {

  return ("http://maps.google.com/maps?" +
          "daddr=" + pntLattd + VALUES_SEP + pntLongtd +
          (pntId == null ? "" : "(" + pntId + ")"));
 }


 private static String getPathUrl(double   fstPntLattd,
                                  double   fstPntLongtd,
                                  double   sndPntLattd,
                                  double   sndPntLongtd,
                                  String   language,
                                  PathMode mode)
 {

  return ("https://maps.googleapis.com/maps/api/directions/json?" +
          "origin=" + fstPntLattd + VALUES_SEP + fstPntLongtd +
          "&" +
          "destination=" + sndPntLattd + VALUES_SEP + sndPntLongtd +
          "&" +
          "sensor=false" +
          "&" +
          "language=" + language +
          "&" +
          "units=metric" +
          "&" +
          "mode=" + mode.description +
          "&" +
          "key=" + "");
 }


 public static boolean getPath(double   fstPntLattd,
                               double   fstPntLongtd,
                               double   sndPntLattd,
                               double   sndPntLongtd,
                               String   language,
                               String   filePath,
                               String   fileName,
                               PathMode mode)
 {

  try
  {
   CommonHttpClient.downloadFile(getPathUrl(fstPntLattd, fstPntLongtd, sndPntLattd, sndPntLongtd, language, mode), null, filePath, fileName, null, false);
  }

  catch (Exception exception)
  {
   Log.e(TAG, "Unexpected error " + exception);
   exception.printStackTrace();
   return false;
  }

  return true;
 }


 public static String getPreference(Context context,
                                    String  preferenceTag,
                                    Object  defaultValue)
 {

  return (context.getSharedPreferences(APP_PREFS_LABEL, Context.MODE_PRIVATE).getString(preferenceTag, String.valueOf(defaultValue)));
 }


 public static String getPreference(Context context,
                                    String  preferenceTag)
 {

  return getPreference(context, preferenceTag, "");
 }


 public static void setPreference(Context context,
                                  String  preferenceTag,
                                  String  preferenceValue)
 {
  Editor editor;

  editor = context.getSharedPreferences(APP_PREFS_LABEL, Context.MODE_PRIVATE).edit();
  editor.putString(preferenceTag, preferenceValue);
  editor.commit();
 }


 public static Set<String> getPreference(Context      context,
                                         String       preferenceTag,
                                         Set<String>  defaultValues)
 {

  return (context.getSharedPreferences(APP_PREFS_LABEL, Context.MODE_PRIVATE).getStringSet(preferenceTag, defaultValues));
 }


 public static void setPreference(Context     context,
                                  String      preferenceTag,
                                  Set<String> preferenceValues)
 {
  Editor editor;

  editor = context.getSharedPreferences(APP_PREFS_LABEL, Context.MODE_PRIVATE).edit();
  editor.putStringSet(preferenceTag, preferenceValues);
  editor.commit();
 }


 public static void unsetPreference(Context context,
                                    String  preferenceTag)
 {

  Editor editor;

  editor = context.getSharedPreferences(APP_PREFS_LABEL, Context.MODE_PRIVATE).edit();
  editor.remove(preferenceTag);
  editor.commit();
 }


 public static long getMaxNativeHeap()
 {

  return (VERSION.SDK_INT < VERSION_CODES.HONEYCOMB ? Runtime.getRuntime().maxMemory() : Long.MAX_VALUE);
 }


 public static float cnvPixels(Context context,
                               float   pixels)
 {
  DisplayMetrics displayMetrics;

  displayMetrics = new DisplayMetrics();
  ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);

  return ((displayMetrics.densityDpi * pixels) / DisplayMetrics.DENSITY_HIGH);
 }


 public static int cnvToDip(Context context,
                            int     pixels)
 {

  return ((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, pixels, context.getResources().getDisplayMetrics()));
 }


 public static float cnvToDip(Context context,
                              float   pixels)
 {

  return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, pixels, context.getResources().getDisplayMetrics());
 }


 public static String removeFileNameExtension(String fileName)
 {

  if ((fileName == null) ||
      (fileName.indexOf(".") <= 0))
  {
   return fileName;
  }

  return fileName.substring(0, fileName.lastIndexOf("."));
 }


 public static boolean isFile(String fileName)
 {

  return ((fileName != null) &&
          (removeFileNameExtension(fileName).length() < fileName.length()));
 }


 public static boolean writeToFile(String  data,
                                   boolean appendData,
                                   String  fileName)
 {
  File filePath;
  FileWriter fileWriter;
  PrintWriter printWriter;

  filePath = new File(fileName).getParentFile();

  if (!filePath.exists() &&
      !filePath.mkdirs())
  {
   Log.e(TAG, "Error accessing or creating directory (" + filePath.getAbsolutePath() + ")");
   return false;
  }

  try
  {
   fileWriter = new FileWriter(fileName, appendData);
   printWriter = new PrintWriter(fileWriter);
   printWriter.print(data);
   printWriter.close();
   fileWriter.close();
  }

  catch (Exception exception)
  {
   Log.e(TAG, "Unexpected error " + exception);
   exception.printStackTrace();
   return false;
  }

  return true;
 }


 public static String readFromFile(String fileName)
 {
  File filePath;
  StringBuilder stringBuilder;
  FileReader fileReader;
  BufferedReader bufferedReader;
  String dataRead;

  filePath = new File(fileName).getParentFile();

  if (!filePath.exists() &&
      !filePath.mkdirs())
  {
   Log.e(TAG, "Error accessing or creating directory (" + filePath.getAbsolutePath() + ")");
   return null;
  }

  stringBuilder = new StringBuilder();

  try
  {
   fileReader = new FileReader(fileName);
   bufferedReader = new BufferedReader(fileReader);
   while ((dataRead = bufferedReader.readLine()) != null)
   {
    stringBuilder.append(dataRead);
   }
   bufferedReader.close();
   fileReader.close();
  }

  catch (Exception exception)
  {
   Log.e(TAG, "Unexpected error " + exception);
   exception.printStackTrace();
   return null;
  }

  return stringBuilder.toString();
 }


 public static boolean writeBytesToFile(byte[]  bytes,
                                        boolean appendData,
                                        String  fileName)
 {
  File filePath;
  FileOutputStream fileOutputStream;

  filePath = new File(fileName).getParentFile();

  if (!filePath.exists() &&
      !filePath.mkdirs())
  {
   Log.e(TAG, "Error accessing or creating directory (" + filePath.getAbsolutePath() + ")");
   return false;
  }

  try
  {
   fileOutputStream = new FileOutputStream(fileName, appendData);
   fileOutputStream.write(bytes);
   fileOutputStream.close();
  }

  catch (Exception exception)
  {
   Log.e(TAG, "Unexpected error " + exception);
   exception.printStackTrace();
   return false;
  }

  return true;
 }


 public static boolean writeObjectToFile(Object object,
                                         String fileName)
 {
  File filePath;
  FileOutputStream fileOutputStream;
  ObjectOutputStream objectOutputStream;

  filePath = new File(fileName).getParentFile();

  if (!filePath.exists() &&
      !filePath.mkdirs())
  {
   Log.e(TAG, "Error accessing or creating directory (" + filePath.getAbsolutePath() + ")");
   return false;
  }

  try
  {
   fileOutputStream = new FileOutputStream(fileName);
   objectOutputStream = new ObjectOutputStream(fileOutputStream);
   objectOutputStream.writeObject(object);
   objectOutputStream.close();
   fileOutputStream.close();
  }

  catch (Exception exception)
  {
   Log.e(TAG, "Unexpected error " + exception);
   exception.printStackTrace();
   return false;
  }

  return true;
 }


 public static Object readObjectFromFile(String fileName)
 {
  File filePath;
  FileInputStream fileInputStream;
  ObjectInputStream objectInputStream;
  Object object;

  filePath = new File(fileName).getParentFile();

  if (!filePath.exists() &&
      !filePath.mkdirs())
  {
   Log.e(TAG, "Error accessing or creating directory (" + filePath.getAbsolutePath() + ")");
   return null;
  }

  try
  {
   fileInputStream = new FileInputStream(fileName);
   objectInputStream = new ObjectInputStream(fileInputStream);
   object = objectInputStream.readObject();
   objectInputStream.close();
   fileInputStream.close();
  }

  catch (Exception exception)
  {
   Log.e(TAG, "Unexpected error " + exception);
   exception.printStackTrace();
   return null;
  }

  return object;
 }


 public static byte[] cnvObjectToBytes(Object object)
 {
  ByteArrayOutputStream byteArrayOutputStream;
  ObjectOutputStream objectOutputStream;
  byte[] bytes;

  try
  {
   byteArrayOutputStream = new ByteArrayOutputStream();
   objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
   objectOutputStream.writeObject(object);
   bytes = byteArrayOutputStream.toByteArray();
   objectOutputStream.close();
   byteArrayOutputStream.close();
  }

  catch (Exception exception)
  {
   Log.e(TAG, "Unexpected error " + exception);
   exception.printStackTrace();
   return null;
  }

  return bytes;
 }


 public static Object cnvBytesToObject(byte[] bytes)
 {
  ByteArrayInputStream byteArrayInputStream;
  ObjectInputStream objectInputStream;
  Object object;

  try
  {
   byteArrayInputStream = new ByteArrayInputStream(bytes);
   objectInputStream = new ObjectInputStream(byteArrayInputStream);
   object = objectInputStream.readObject();
   objectInputStream.close();
   byteArrayInputStream.close();
  }

  catch (Exception exception)
  {
   Log.e(TAG, "Unexpected error " + exception);
   exception.printStackTrace();
   return null;
  }

  return object;
 }


 public static boolean isIntentSupported(Context context,
                                         Intent  intent)
 {
  List<ResolveInfo> list;

  list = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

  return (list.size() > 0);
 }


 public static boolean copyFile(String inputFilePath,
                                String inputFileName,
                                String outputFilePath,
                                String outputFileName)
 {
  File auxInputFilePath, auxOutputFilePath;
  FileInputStream fileInputStream;
  FileOutputStream fileOutputStream;
  int bytesRead;
  byte[] dataBuffer = new byte[1024];

  auxInputFilePath = new File(inputFilePath);

  if (!auxInputFilePath.exists() &&
      !auxInputFilePath.mkdirs())
  {
   Log.e(TAG, "Error accessing or creating directory (" + auxInputFilePath + ")");
   return false;
  }

  auxOutputFilePath = new File(outputFilePath);

  if (!auxOutputFilePath.exists() &&
      !auxOutputFilePath.mkdirs())
  {
   Log.e(TAG, "Error accessing or creating directory (" + auxOutputFilePath + ")");
   return false;
  }

  try
  {
   fileInputStream = new FileInputStream(inputFilePath + File.separator + inputFileName);
   fileOutputStream = new FileOutputStream(outputFilePath + File.separator + outputFileName);

   while ((bytesRead = fileInputStream.read(dataBuffer)) != -1)
   {
    fileOutputStream.write(dataBuffer, 0, bytesRead);
   }

   fileInputStream.close();
   fileOutputStream.close();
  }

  catch (Exception exception)
  {
   Log.e(TAG, "Error copying file (" + inputFileName + ") " + exception);
   exception.printStackTrace();
   return false;
  }

  return true;
 }


 public static boolean deleteFile(String filePath)
 {
  File auxFile;
  File[] auxFiles;

  auxFile = new File(filePath);

  if (auxFile.exists())
  {
   if (auxFile.isDirectory())
   {
    auxFiles = auxFile.listFiles();

    for (int i = 0;
         i < auxFiles.length;
         i ++)
    {
     if (auxFiles[i].isDirectory())
     {
      if (!deleteFile(auxFiles[i].getAbsolutePath()))
      {
       return false;
      }
     }
     else
     {
      if (!auxFiles[i].delete())
      {
       return false;
      }
     }
    }
   }

   if (!auxFile.delete())
   {
    return false;
   }
  }

  return true;
 }


 public static void importFile(String inputFileName,
                               String outputFileName) throws Exception
 {
  FileInputStream fileInputStream;
  FileOutputStream fileOutputStream;
  int bytesAvailable, bytesRead;
  byte[] dataBuffer;

  fileInputStream = new FileInputStream(inputFileName);
  fileOutputStream = new FileOutputStream(outputFileName);

  bytesAvailable = fileInputStream.available();
  dataBuffer = new byte[Math.min(bytesAvailable, MAX_FILE_BUFFER_SIZE)];

  while ((bytesRead = fileInputStream.read(dataBuffer)) != -1)
  {
   fileOutputStream.write(dataBuffer, 0, bytesRead);
  }

  fileInputStream.close();
  fileOutputStream.close();
 }


 public static void importFile(Context context,
                               Uri     uri,
                               String  outputFileName) throws Exception
 {
  InputStream inputStream;
  FileOutputStream fileOutputStream;
  int bytesAvailable, bytesRead;
  byte[] dataBuffer;

  inputStream = context.getContentResolver().openInputStream(uri);
  fileOutputStream = new FileOutputStream(outputFileName);

  bytesAvailable = inputStream.available();
  dataBuffer = new byte[Math.min(bytesAvailable, MAX_FILE_BUFFER_SIZE)];

  while ((bytesRead = inputStream.read(dataBuffer)) != -1)
  {
   fileOutputStream.write(dataBuffer, 0, bytesRead);
  }

  inputStream.close();
  fileOutputStream.close();
 }


 public static boolean isFileKnown(String   fileName,
                                   String[] fileNamesSet)
 {

  for (int i = 0;
       i < fileNamesSet.length;
       i ++)
  {
   if (fileName.toLowerCase().endsWith(fileNamesSet[i]))
   {
    return true;
   }
  }

  return false;
 }


 public static String rightPadString(int    length,
                                     char   paddingCharacter,
                                     String value)
 {

  if (value == null)
  {
   return VALUES_STRING_DELIM + VALUES_STRING_DELIM;
  }

  for (int i = value.length();
       i < length;
       i ++)
  {
   value = value.concat(String.valueOf(paddingCharacter));
  }

  return value;
 }


 public static void adjustFontScale(Activity      activity,
                                    Configuration configuration)
 {
  DisplayMetrics displayMetrics;
  WindowManager windowManager;

  configuration.fontScale = 1.0f;

  displayMetrics = activity.getResources().getDisplayMetrics();

  windowManager = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
  windowManager.getDefaultDisplay().getMetrics(displayMetrics);

  displayMetrics.scaledDensity = configuration.fontScale * displayMetrics.density;

  activity.getBaseContext().getResources().updateConfiguration(configuration, displayMetrics);
 }


 public static Long getUriId(Context context,
                             Uri     uri)
 {
  Cursor cursor;
  int idColumnIndex;
  String[] projection = new String[]{BaseColumns._ID};

  cursor = context.getContentResolver().query(uri, projection, null, null, null);
  idColumnIndex = cursor.getColumnIndexOrThrow(BaseColumns._ID);
  cursor.moveToFirst();

  return Long.parseLong(cursor.getString(idColumnIndex));
 }


 private static String getUriPath(Activity activity,
                                  String   columnName,
                                  Uri      uri)
 {
  Cursor cursor;
  String[] projection = new String[]{columnName};
  int columnIndex;

  if (VERSION.SDK_INT < VERSION_CODES.HONEYCOMB)
  {
   cursor = activity.managedQuery(uri, projection, null, null, null);
  }
  else
  {
   cursor = activity.getContentResolver().query(uri, projection, null, null, null);
  }

  columnIndex = cursor.getColumnIndexOrThrow(columnName);
  cursor.moveToFirst();

  return (cursor.getString(columnIndex));
 }


 private static String getUriPath(Context context,
                                  Uri     uri)
 {
  Cursor cursor;
  int nameColumnIndex;
  String nameColumnLabel;
  File file;

  cursor = context.getContentResolver().query(uri, null, null, null, null);
  nameColumnIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
  cursor.moveToFirst();
  nameColumnLabel = cursor.getString(nameColumnIndex);
  file = new File(context.getFilesDir(), nameColumnLabel);

  try
  {
   importFile(context, uri, file.getPath());
  }

  catch (Exception exception)
  {
   Log.e(TAG, "Unexpected error " + exception);
   exception.printStackTrace();
   return null;
  }

  return file.getPath();
 }


 public static String getUriPath(Activity activity,
                                 Uri      uri)
 {
  String result;

  try
  {
   //Handle files with the 'content:' prefix
   if (VERSION.SDK_INT < VERSION_CODES.Q)
   {
    if ((result = getUriPath(activity, MediaStore.MediaColumns.DATA, uri)) == null)
    {
     result = getUriPath((Context) activity, uri);
    }
   }
   else
   {
    result = getUriPath((Context) activity, uri);
   }

   return result;
  }

  catch (Exception exception)
  {
   //Handle files with the 'file:' prefix
   return uri.getPath();
  }
 }


 public static boolean isRemoteUrl(String url)
 {

  try
  {
   if (URLUtil.isHttpUrl(url) ||
       URLUtil.isHttpsUrl(url))
   {
    return true;
   }
  }

  catch (Exception exception)
  {
  }

  return false;
 }


 public static String getContentPath(String url)
 {
  String result;

  result = url;

  if (url.contains("http://") ||
      url.contains("https://"))
  {
   result = url.substring("YYYYMMDDHHMMSS_".length());
  }

  return result;
 }


 public static Uri getContentUri(Context context,
                                 String  fileName)
 {

  return (VERSION.SDK_INT < VERSION_CODES.R ? Uri.fromFile(new File(fileName)) : FileProvider.getUriForFile(context,
                                                                                                            context.getPackageName() + ".provider",
                                                                                                            new File(fileName)));
 }


 public static void shareMedia(Activity activity,
                               String   title,
                               String   label,
                               Object   data,
                               String   type)
 {
  Intent intent;

  intent = new Intent();
  intent.setAction(Intent.ACTION_SEND);
  intent.putExtra(Intent.EXTRA_SUBJECT, label);

  if (data instanceof String)
  {
   intent.putExtra(Intent.EXTRA_TEXT, (String) data);
  }
  else if (data instanceof Parcelable)
  {
   intent.putExtra(Intent.EXTRA_STREAM, (Parcelable) data);
  }

  intent.setType(type);
  intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

  activity.startActivityForResult(Intent.createChooser(intent, title), INTENT_REQ_CODE_SHARE_DATA);
 }


 private static boolean isAppInBackgroundLegacy(Context context)
 {
  ActivityManager activityManager;
  List<ActivityManager.RunningAppProcessInfo> runningAppProcessInfos;

  activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
  runningAppProcessInfos = activityManager.getRunningAppProcesses();

  for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcessInfos)
  {
   if (runningAppProcessInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND)
   {
    for (String packageName : runningAppProcessInfo.pkgList)
    {
     if (packageName.equals(context.getPackageName()))
     {
      return false;
     }
    }
   }
  }

  return true;
 }


 @TargetApi(29)
 private static boolean isAppInBackgroundV29(Context context)
 {
  ActivityManager activityManager;
  List<ActivityManager.RunningTaskInfo> runningTaskInfos;

  activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
  runningTaskInfos = activityManager.getRunningTasks(1);

  if (runningTaskInfos.get(0).topActivity.getPackageName().equals(context.getPackageName()))
  {
   return false;
  }

  return true;
 }


 public static boolean isAppInBackground(Context context)
 {

  return (VERSION.SDK_INT < VERSION_CODES.Q ? isAppInBackgroundLegacy(context) : isAppInBackgroundV29(context));
 }


 private static boolean isLogSavingInProgress()
 {

  try
  {
   logSavingProcess.exitValue();
   return false;
  }

  catch (Exception exception)
  {
   return true;
  }
 }


 public static void startSavingLogs(Context context,
                                    String  filePath,
                                    String  info)
 {
  String externalStorageState, applicationName, currentTime;
  File auxFilePath, file;

  if ((logSavingProcess == null) ||
      !isLogSavingInProgress())
  {
   try
   {
    externalStorageState = Environment.getExternalStorageState();

    if (externalStorageState.compareTo(Environment.MEDIA_MOUNTED) != 0)
    {
     Log.e(TAG, "Error accessing external storage (" + externalStorageState + ")");
     return;
    }

    auxFilePath = new File(filePath);

    if (!auxFilePath.exists() &&
        !auxFilePath.mkdirs())
    {
     Log.e(TAG, "Error accessing or creating directory (" + filePath + ")");
     return;
    }

    applicationName = context.getApplicationInfo().loadLabel(context.getPackageManager()).toString();
    currentTime = DATE_TIME_FORMATTER_FILE.format(new Date(System.currentTimeMillis()));
    file = new File(filePath + File.separator + applicationName + "_" + currentTime + TEXT_FILE_EXT);

    if (file.createNewFile())
    {
     logSavingProcess = Runtime.getRuntime().exec(new String[]{LOG_COMMAND,
                                                               LOG_PRINT_FORMAT_OPTION,
                                                               LOG_PRINT_FORMAT,
                                                               LOG_WRITE_TO_FILE_OPTION,
                                                               file.getPath(),
                                                               LOG_PRIORITY});

     if (info != null)
     {
      Toast.makeText(context, info, Toast.LENGTH_SHORT).show();
     }
    }
   }

   catch (Exception exception)
   {
    Log.e(TAG, "Unexpected error " + exception);
    exception.printStackTrace();
   }
  }
 }


 public static void stopSavingLogs(Context context,
                                   String  info)
 {

  if ((logSavingProcess != null) &&
      isLogSavingInProgress())
  {
   logSavingProcess.destroy();

   if (info != null)
   {
    Toast.makeText(context, info, Toast.LENGTH_SHORT).show();
   }
  }
 }


 public static float[] getRoundRectangleShape(Context context,
                                              int     radius)
 {
  Point point;

  point = getDisplaySize(context);

  return new float[]{point.x / radius,
                     point.x / radius,
                     point.x / radius,
                     point.x / radius,
                     point.x / radius,
                     point.x / radius,
                     point.x / radius,
                     point.x / radius};
 }


 @TargetApi(26)
 public static NotificationChannel getNotificationChannel(Context context,
                                                          int     idResource,
                                                          int     nameResource)
 {
  NotificationChannel channel;

  channel = new NotificationChannel(context.getString(idResource),
                                    context.getString(nameResource),
                                    NotificationManager.IMPORTANCE_DEFAULT);

  return channel;
 }


 public static String getLengthiestString(String fstString,
                                          String sndString)
 {

  if (fstString == null)
  {
   return sndString;
  }

  if (sndString == null)
  {
   return fstString;
  }

  return (fstString.length() > sndString.length() ? fstString : sndString);
 }


 public static int getRandomInt(int minValue,
                                int maxValue)
 {

  return (new Random()).nextInt((maxValue - minValue) + 1) + minValue;
 }


 public static String getLanguage(Context context)
 {

  return LOCALE_LANGUAGES.get(getPreference(context, LANGUAGE_TAG));
 }


 public static ContextWrapper updateLocale(Context context)
 {
  Locale locale;
  Resources resources;
  Configuration configuration;

  locale = new Locale(getLanguage(context));
  resources = context.getResources();
  configuration = resources.getConfiguration();

  if (VERSION.SDK_INT >= VERSION_CODES.N)
  {
   LocaleList localeList = new LocaleList(locale);
   LocaleList.setDefault(localeList);
   configuration.setLocales(localeList);
  }
  else
  {
   configuration.locale = locale;
  }

  if (VERSION.SDK_INT >= VERSION_CODES.N_MR1)
  {
   context = context.createConfigurationContext(configuration);
  }
  else
  {
   resources.updateConfiguration(configuration, resources.getDisplayMetrics());
  }

  return new ContextWrapper(context);
 }


 public static void retrieveJsonToken(JsonReader jsonReader,
                                      String     jsonTokenNameKey,
                                      String     jsonTokenValueKey) throws Exception
 {
  String jsonTokenName, jsonTokenValue;
  JsonToken jsonToken;

  jsonTokenName = null;
  jsonTokenValue = null;

  do
  {
   jsonToken = jsonReader.peek();

   if (jsonToken.equals(JsonToken.BEGIN_ARRAY))
   {
    jsonReader.beginArray();
   }
   else if (jsonToken.equals(JsonToken.END_ARRAY))
   {
    jsonReader.endArray();
   }
   else if (jsonToken.equals(JsonToken.BEGIN_OBJECT))
   {
    jsonReader.beginObject();
   }
   else if (jsonToken.equals(JsonToken.END_OBJECT))
   {
    jsonReader.endObject();
   }
   else if (jsonToken.equals(JsonToken.NAME))
   {
    jsonTokenName = jsonReader.nextName();
   }
   else if (jsonToken.equals(JsonToken.STRING) ||
           jsonToken.equals(JsonToken.NUMBER))
   {
    jsonTokenValue = jsonReader.nextString();
   }
   else if (jsonToken.equals(JsonToken.BOOLEAN))
   {
    jsonTokenValue = String.valueOf(jsonReader.nextBoolean());
   }
   else if (jsonToken.equals(JsonToken.NULL))
   {
    jsonTokenValue = "null";
    jsonReader.nextNull();
   }

   if (!jsonToken.equals(JsonToken.NAME))
   {
    if ((jsonTokenName != null) &&
        jsonTokenName.equals(jsonTokenNameKey))
    {
     if (jsonTokenValueKey == null)
     {
      return;
     }

     if ((jsonTokenValue != null) &&
         jsonTokenValue.toLowerCase().contains(jsonTokenValueKey.toLowerCase()))
     {
      return;
     }
    }

    jsonTokenName = null;
    jsonTokenValue = null;
   }

  } while (!JsonToken.END_DOCUMENT.equals(jsonToken));
 }
}