package common;


import android.graphics.Color;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


/**
 * Interface to be implemented by classes sharing common constants.
 */
public interface CommonConstants
{
 public static final String        LOG_FILES_PATH = "../log/";
 public static final String        ICON_FILES_PATH = "../icons/";

 public static final String        LOGOUT_APP_CONFIRMATION_MSG = "Are you sure you want to logout?";

 public static final int           UNDEF_ROW = -1;
 public static final int           UNDEF_COLUMN = -1;

 public static final int           SORT_PROPS_COLUMN_INDEX = 0;
 public static final int           SORT_PROPS_SECONDARY_KEY_COLUMN_INDEX = 1;
 public static final int           SORT_PROPS_SECONDARY_KEY_COLUMN_SORTING_ORDER_INDEX = 2;

 public static final int           FILE_BROWSER_ICON_COLUMN = 0;
 public static final int           FILE_BROWSER_FILE_NAME_COLUMN = 1;
 public static final int           FILE_BROWSER_FILE_INFO_COLUMN = 2;
 public static final int           FILE_BROWSER_FILE_PATH_COLUMN = 3;
 public static final int           FILE_BROWSER_FILE_MODIFICATION_DATE_COLUMN = 4;
 public static final int           FILE_BROWSER_SEL_COLUMN = 5;
 public static final int           FILE_BROWSER_LOAD_BITMAP_COLUMN = 6;

 public static final boolean       SORT_ASCENDING = true;
 public static final boolean       SORT_DESCENDING = false;

 public static final int           COLUMN_PRESSED = 0;
 public static final int           COLUMN_UNPRESSED = 1;

 public static final int           ROW_NOT_SELECTABLE = -1;
 public static final int           ROW_SELECTED = 0;
 public static final int           ROW_UNSELECTED = 1;

 public static final String        VALUES_FST_DELIM = "(";
 public static final String        VALUES_LST_DELIM = ")";
 public static final String        VALUES_SEP = ",";
 public static final String        VALUES_EQUALS = "=";
 public static final String        VALUES_STRING_DELIM = "\"";
 public static final String        VALUES_NULL = "null";
 public static final String        VALUES_SPACE = " ";

 public static final DateFormat    DATE_TIME_FORMATTER_FILE = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
 public static final DateFormat    DATE_TIME_FORMATTER_DISPLAY = new SimpleDateFormat("yyyy/MM/dd HH:mm");
 public static final DateFormat    DATE_TIME_FORMATTER_WITH_LOCALE = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM, Locale.getDefault());
 public static final DateFormat    DATE_FORMATTER_DISPLAY = new SimpleDateFormat("yyyy/MM/dd");

 public static final String        FULL_TIME_FLD_FORMAT = "00";
 public static final DecimalFormat FULL_TIME_FLD_FORMATTER = new DecimalFormat(FULL_TIME_FLD_FORMAT);

 public static final int           SRVR_CONNECTION_TIMEOUT = 10000;
 public static final int           SRVR_INACTIVITY_TIMEOUT = 1000;

 public static final String        FIREBASE_PUSH_NOTIFICATION_DATA_INTENT_LABEL = "FIREBASE_PUSH_NOTIFICATION_DATA_INTENT_LABEL";
 public static final String        FIREBASE_PUSH_NOTIFICATION_ERROR_INTENT_LABEL = "FIREBASE_PUSH_NOTIFICATION_ERROR_INTENT_LABEL";
 public static final String        SIP_AUDIO_CALL_INTENT_LABEL = "SIP_AUDIO_CALL";

 public static final int           SIP_AUDIO_CALL_TIMEOUT = 30;

 public static final String        JAVA_VERSION_LABEL = " [Java ".concat(System.getProperty("java.version")).concat("]");

 public static final String        INTENT_PARAMS_STRING_ARRAY_LABEL = "STRING ARRAY";
 public static final String        INTENT_PARAMS_STRING_LABEL = "STRING";
 public static final String        INTENT_PARAMS_INT_ARRAY_LABEL = "INT ARRAY";
 public static final String        INTENT_PARAMS_INT_LABEL = "INT";
 public static final String        INTENT_PARAMS_BOOLEAN_LABEL = "BOOLEAN";
 public static final String        INTENT_PARAMS_FST_OBJECT_LABEL = "FST_OBJECT";
 public static final String        INTENT_PARAMS_SND_OBJECT_LABEL = "SND_OBJECT";
 public static final String        INTENT_PARAMS_TRD_OBJECT_LABEL = "TRD_OBJECT";
 public static final String        INTENT_PARAMS_BYTE_ARRAY_LABEL = "BYTE ARRAY";

 public static final String        INTENT_PARAMS_NOTIFICATION_MENU_RESOURCES_LABEL = "NOTIFICATION MENU";

 public static final String        INTENT_ACTION_PREFIX = "INTENT_ACTION_";

 public static final String        POST_IT_PICTURE_INTENT_PARAMS_BUTTONS_RESOURCES_LABEL = "POST-IT BUTTONS";
 public static final String        POST_IT_PICTURE_INTENT_PARAMS_CANVAS_BACKGROUND_COLORS_SPINNER_RESOURCES_LABEL = "POST-IT BACKGRAOUND COLORS SPINNER";
 public static final String        POST_IT_PICTURE_INTENT_PARAMS_LINE_COLORS_SPINNER_RESOURCES_LABEL = "POST-IT LINE COLORS SPINNER";
 public static final String        POST_IT_PICTURE_INTENT_PARAMS_LINE_WEIGHTS_SPINNER_RESOURCES_LABEL = "POST-IT LINE WEIGHTS SPINNER";
 public static final String        POST_IT_PICTURE_INTENT_PARAMS_BACKGROUND_LABEL = "POST-IT BACKGROUND";

 public static final String        MSG_PARAMS_LABEL = "MSG PARAMS";
 public static final String        MSG_PARAMS_MAX_ELEMS_LABEL = "MSG PARAMS MAX ELEMS";
 public static final String        MSG_PARAMS_CURR_ELEM_LABEL = "MSG PARAMS CURR ELEM";
 public static final String        MSG_PARAMS_MAX_AUX_ELEMS_LABEL = "MSG PARAMS MAX AUX ELEMS";
 public static final String        MSG_PARAMS_CURR_AUX_ELEM_LABEL = "MSG PARAMS CURR AUX ELEM";

 public static final int           NOTIFICATION_MENU_ICONS_INFO_INDEX = 0;
 public static final int           NOTIFICATION_MENU_ICONS_WARNING_INDEX = 1;
 public static final int           NOTIFICATION_MENU_ICONS_ERROR_INDEX = 2;
 public static final int           NOTIFICATION_MENU_ICONS_QUESTION_INDEX = 3;

 public static final String        APP_PREFS_LABEL = "APP PREFS";

 public static final String        FTP_SRVR_ADDRESS_TAG = "FTP_SRVR_ADDR";
 public static final String        DIST_UNIT_TAG = "DIST_UNIT";
 public static final String        CONSTRAINT_TAG = "CONSTRAINT";
 public static final String        VIEW_TAG = "VIEW";
 public static final String        COVERAGE_TAG = "COVERAGE";
 public static final String        PHOTO_CAMERA_TAG = "PHOTO_CAMERA";
 public static final String        PHOTO_CAMERA_FACING_FRONT_SIZE_TAG = "PHOTO_CAMERA_FACING_FRONT_SIZE";
 public static final String        PHOTO_CAMERA_FACING_BACK_SIZE_TAG = "PHOTO_CAMERA_FACING_BACK_SIZE";
 public static final String        IMAGE_VIEWER_TAG = "IMAGE_VIEWER";
 public static final String        VIDEO_CAMERA_TAG = "VIDEO_CAMERA";
 public static final String        VIDEO_CAMERA_FACING_FRONT_QUALITY_TAG = "VIDEO_CAMERA_FACING_FRONT_QUALITY";
 public static final String        VIDEO_CAMERA_FACING_BACK_QUALITY_TAG = "VIDEO_CAMERA_FACING_BACK_QUALITY";
 public static final String        VIDEO_PLAYER_TAG = "VIDEO_PLAYER";
 public static final String        TUTORIAL_TAG = "TUTORIAL";
 public static final String        MAP_PATH_MODE_TAG = "MAP_PATH_MODE";
 public static final String        AR_PATH_MODE_TAG = "AR_PATH_MODE";
 public static final String        FILE_BROWSER_SORTING_COLUMN_TAG = "FILE_BROWSER_SORTING_COLUMN";
 public static final String        FILE_BROWSER_SORTING_ORDER_TAG = "FILE_BROWSER_SORTING_ORDER";
 public static final String        THEME_TAG = "THEME";
 public static final String        FIREBASE_REFRESHED_TOKEN_TAG = "FIREBASE_REFRESHED_TOKEN";
 public static final String        CLOUD_SERVER_ADDRESS_TAG = "CLOUD_SERVER_ADDRESS";
 public static final String        SAVE_LOGS_TAG = "SAVE_LOGS";
 public static final String        AUTHENTICATION_TAG = "AUTHENTICATION";
 public static final String        PIN_TAG = "PIN";
 public static final String        PIN_LAST_FAILED_INPUT_TIME_TAG = "PIN_LAST_FAILED_INPUT_TIME";
 public static final String        FIREBASE_MESSAGES_TAG = "FIREBASE_MESSAGES_TAG";
 public static final String        SHUFFLE_TAG = "SHUFFLE_TAG";
 public static final String        REPEAT_TAG = "REPEAT_TAG";
 public static final String        LANGUAGE_TAG = "LANGUAGE_TAG";

 public static final String        M_LABEL = "m";
 public static final String        KM_LABEL = "Km";
 public static final String        NM_LABEL = "Nm";
 public static final String        MI_LABEL = "Mi";
 public static final String        BYTES_LABEL = "bytes";
 public static final String        KB_LABEL = "KB";
 public static final String        MB_LABEL = "MB";

 public static final float         FACTOR_NM_TO_KM = 1.852f;
 public static final float         FACTOR_MI_TO_KM = 1.609344f;

 public static final String        NORMAL_LABEL = "Normal";
 public static final String        HYBRID_LABEL = "Hybrid";
 public static final String        REGULAR_LABEL = "Regular";
 public static final String        SATELLITE_LABEL = "Satellite";
 public static final String        TERRAIN_LABEL = "Terrain";

 public static final String        ANDROID_CAMERA_LABEL = "ANDROID_CAMERA";
 public static final String        CUSTOM_CAMERA_LABEL = "CUSTOM_CAMERA";

 public static final String        ANDROID_VIEWER_LABEL = "ANDROID_VIEWER";
 public static final String        CUSTOM_VIEWER_LABEL = "CUSTOM_VIEWER";

 public static final String        ANDROID_PLAYER_LABEL = "ANDROID_PLAYER";
 public static final String        CUSTOM_PLAYER_LABEL = "CUSTOM_PLAYER";

 public static final String        LQ_LABEL = "LQ";
 public static final String        HQ_LABEL = "HQ";
 public static final String        QCIF_LABEL = "QCIF";
 public static final String        CIF_LABEL = "CIF";
 public static final String        _480P_LABEL = "480P";
 public static final String        _720P_LABEL = "720P";
 public static final String        _1080P_LABEL = "1080P";
 public static final String        _2160P_LABEL = "2160P";

 public static final String        ALWAYS_POP_UP_LABEL = "ALWAYS_POP_UP";
 public static final String        NEVER_POP_UP_LABEL = "NEVER_POP_UP";

 public static final String        LIGHT_LABEL = "LIGHT";
 public static final String        DARK_LABEL = "DARK";

 public static final String        SHUFFLE_OFF_LABEL = "SHUFFLE_OFF";
 public static final String        SHUFFLE_ON_LABEL = "SHUFFLE_ON";

 public static final String        REPEAT_OFF_LABEL = "REPEAT_OFF";
 public static final String        REPEAT_ONE_LABEL = "REPEAT_ONE";
 public static final String        REPEAT_ALL_LABEL = "REPEAT_ALL";

 public static final String        NONE_LABEL = "NONE";
 public static final String        PIN_LABEL = "PIN";
 public static final String        FINGERPRINT_LABEL = "FINGERPRINT";

 public static final String        ENGLISH_LABEL = "ENGLISH";
 public static final String        FRENCH_LABEL = "FRENCH";
 public static final String        PORTUGUESE_LABEL = "PORTUGUESE";

 public static final String        EN_LABEL = "EN";
 public static final String        FR_LABEL = "FR";
 public static final String        PT_LABEL = "PT";

 public static final int           TOTAL_GEOD_PNT_LAT_CHARS = 11;
 public static final int           TOTAL_GEOD_PNT_LONG_CHARS = 12;
 public static final int           TOTAL_GEOD_PNT_CHARS = TOTAL_GEOD_PNT_LAT_CHARS + TOTAL_GEOD_PNT_LONG_CHARS;

 public static final int           GEOD_PNT_LAT_DEG_INDEX = 0;
 public static final int           GEOD_PNT_LAT_MIN_INDEX = 1;
 public static final int           GEOD_PNT_LAT_SEC_INDEX = 2;
 public static final int           GEOD_PNT_LAT_DIR_INDEX = 3;
 public static final int           GEOD_PNT_LONG_DEG_INDEX = 4;
 public static final int           GEOD_PNT_LONG_MIN_INDEX = 5;
 public static final int           GEOD_PNT_LONG_SEC_INDEX = 6;
 public static final int           GEOD_PNT_LONG_DIR_INDEX = 7;

 public static final Object[][]    GEOD_PNT_PROPS = {{new Integer(0),  new Integer(2),  new Integer(0), new Integer(89)},
                                                     {new Integer(2),  new Integer(4),  new Integer(0), new Integer(59)},
                                                     {new Integer(4),  new Integer(10), new Integer(0), new Integer(59)},
                                                     {new Integer(10), new Integer(11), "N",            "S"},
                                                     {new Integer(11), new Integer(14), new Integer(0), new Integer(179)},
                                                     {new Integer(14), new Integer(16), new Integer(0), new Integer(59)},
                                                     {new Integer(16), new Integer(22), new Integer(0), new Integer(59)},
                                                     {new Integer(22), new Integer(23), "E",            "W"}};

 public static final int           GEOD_PNT_PROPS_START_CHAR_INDEX = 0;
 public static final int           GEOD_PNT_PROPS_END_CHAR_INDEX = 1;
 public static final int           GEOD_PNT_PROPS_MIN_VALUE_INDEX = 2;
 public static final int           GEOD_PNT_PROPS_MAX_VALUE_INDEX = 3;

 public static final String        GEOD_PNT_MIN_LAT = "895959.999N";
 public static final String        GEOD_PNT_MAX_LAT = "895959.999S";
 public static final String        GEOD_PNT_MIN_LONG = "1795959.999E";
 public static final String        GEOD_PNT_MAX_LONG = "1795959.999W";

 public static final int           DATE_YEAR_INDEX = 0;
 public static final int           DATE_MONTH_INDEX = 1;
 public static final int           DATE_DAY_INDEX = 2;
 public static final int           DATE_HOUR_INDEX = 3;
 public static final int           DATE_MIN_INDEX = 4;
 public static final int           DATE_SEC_INDEX = 5;

 public static final Integer[][]   DATE_PROPS = {{new Integer(0),  new Integer(4) },
                                                 {new Integer(4),  new Integer(6) },
                                                 {new Integer(6),  new Integer(8) },
                                                 {new Integer(8),  new Integer(10)},
                                                 {new Integer(10), new Integer(12)},
                                                 {new Integer(12), new Integer(14)}};

 public static final int           DATE_PROPS_START_CHAR_INDEX = 0;
 public static final int           DATE_PROPS_END_CHAR_INDEX = 1;

 public static final float         FONT_SIZE_TINY = 11.0f;
 public static final float         FONT_SIZE_SMALL = 13.5f;
 public static final float         FONT_SIZE_MEDIUM = 16.0f;
 public static final float         FONT_SIZE_LARGE = 32.0f;
 public static final float         FONT_SIZE_HUGE = 52.0f;

 public static final int           WAITING_FOR_GPS_MESSAGE_BLINK_INTERVAL = 500;//milliseconds

 public static final int           PIN_MAX_INPUT_ATTEMPTS = 5;
 public static final int           PIN_FAILED_INPUTS_DELAY = 30;//Seconds

 public static final String        PICTURE_FILE_PREFIX = "PICTURE_";
 public static final String        AUDIO_FILE_PREFIX = "TRACK_";
 public static final String        PHOTO_FILE_PREFIX = "PHOTO_";
 public static final String        VIDEO_FILE_PREFIX = "VIDEO_";
 public static final String        DOCUMENT_FILE_PREFIX = "DOCUMENT_";
 public static final String        ALL_FILES_PREFIX = "*";

 public static final String        PATH_TEMP_FILE_NAME = "path";
 public static final String        PHOTO_TEMP_FILE_NAME = "photo";
 public static final String        VIDEO_TEMP_FILE_NAME = "video";

 public static final String        PATH_FILE_EXT = ".kml";
 public static final String        PICTURE_FILE_EXT = ".jpg";
 public static final String        AUDIO_FILE_EXT = ".3gpp";
 public static final String        PHOTO_FILE_EXT = ".jpg";
 public static final String        VIDEO_FILE_EXT = ".mp4";
 public static final String        BACKUP_FILE_EXT = ".sav";
 public static final String        TEMP_FILE_EXT = ".tmp";
 public static final String        TEXT_FILE_EXT = ".txt";
 public static final String        RICH_TEXT_FORMAT_EXT = ".rtf";
 public static final String        EXCEL_FILE_EXT = ".xls";
 public static final String        PORTABLE_DOCUMENT_FORMAT_EXT = ".pdf";
 public static final String        POWERPOINT_FILE_EXT = ".ppt";
 public static final String        WORD_FILE_EXT = ".doc";
 public static final String        ALL_FILES_EXT = ".*";

 public static final String[]      PICTURE_FILES_SET = {".bmp",
                                                        ".gif",
                                                        PICTURE_FILE_EXT,
                                                        ".png"};

 public static final String[]      AUDIO_FILES_SET = {AUDIO_FILE_EXT,
                                                      ".mp3",
                                                      ".flac"};

 public static final String[]      PHOTO_FILES_SET = {".jpeg",
                                                      PHOTO_FILE_EXT};

 public static final String[]      VIDEO_FILES_SET = {".3gp",
                                                      ".m4v",
                                                      VIDEO_FILE_EXT};

 public static final List<String>  DOCUMENT_CODE_FILES_SET = Arrays.<String>asList(".c",
                                                                                   ".cpp",
                                                                                   ".h",
                                                                                   ".hpp",
                                                                                   ".java");

 public static final List<String>  DOCUMENT_WORD_FILES_SET = Arrays.<String>asList(WORD_FILE_EXT,
                                                                                   ".docm",
                                                                                   ".docx");

 public static final List<String>  DOCUMENT_PORTABLE_DOCUMENT_FORMAT_FILES_SET = Arrays.<String>asList(PORTABLE_DOCUMENT_FORMAT_EXT);

 public static final List<String>  DOCUMENT_POWERPOINT_FILES_SET = Arrays.<String>asList(POWERPOINT_FILE_EXT,
                                                                                         ".pptm",
                                                                                         ".pptx");

 public static final List<String>  DOCUMENT_TEXT_FILES_SET = Arrays.<String>asList(RICH_TEXT_FORMAT_EXT,
                                                                                   TEXT_FILE_EXT);

 public static final List<String>  DOCUMENT_EXCEL_FILES_EXT = Arrays.<String>asList(EXCEL_FILE_EXT,
                                                                                    ".xlsm",
                                                                                    ".xlsx");

 public static final String[]      BACKUP_FILES_SET = {BACKUP_FILE_EXT};

 public static final String        LOG_COMMAND = "logcat";
 public static final String        LOG_PRINT_FORMAT_OPTION = "-v";
 public static final String        LOG_PRINT_FORMAT = "threadtime";
 public static final String        LOG_WRITE_TO_FILE_OPTION = "-f";
 public static final String        LOG_PRIORITY = "*:V";

 public static final int           UNDEF_PICTURE_BACKGROUND = -3;
 public static final int           THUMBNAIL_IMAGE_BACKGROUND = -2;
 public static final int           THUMBNAIL_VIDEO_BACKGROUND = -1;

 public static final int           INTENT_REQ_CODE_CHECK_TTS = 1;
 public static final int           INTENT_REQ_CODE_DRAW_PICTURES = 2;
 public static final int           INTENT_REQ_CODE_RECORD_TRACK = 3;
 public static final int           INTENT_REQ_CODE_CAPTURE_PHOTO = 4;
 public static final int           INTENT_REQ_CODE_CAPTURE_PHOTOS = 5;
 public static final int           INTENT_REQ_CODE_CAPTURE_VIDEO = 6;
 public static final int           INTENT_REQ_CODE_CAPTURE_VIDEOS = 7;
 public static final int           INTENT_REQ_CODE_IMPORT_PICTURES = 8;
 public static final int           INTENT_REQ_CODE_IMPORT_TRACKS = 9;
 public static final int           INTENT_REQ_CODE_IMPORT_PHOTOS = 10;
 public static final int           INTENT_REQ_CODE_IMPORT_VIDEOS = 11;
 public static final int           INTENT_REQ_CODE_RECOGNIZE_SPEECH = 12;
 public static final int           INTENT_REQ_CODE_RESTORE_LOCAL_DATA = 13;
 public static final int           INTENT_REQ_CODE_RESTORE_REMOTE_DATA = 14;
 public static final int           INTENT_REQ_CODE_BACKUP_REMOTE_DATA = 15;
 public static final int           INTENT_REQ_CODE_INVITE = 16;
 public static final int           INTENT_REQ_CODE_SHARE_DATA = 17;
 public static final int           INTENT_REQ_CODE_DEL_VIDEOS_AFTER_SAVE = 18;
 public static final int           INTENT_REQ_CODE_DEL_VIDEOS_AFTER_FINISH = 19;
 public static final int           INTENT_REQ_CODE_IMPORT_DOCUMENTS = 20;
 public static final int           INTENT_REQ_CODE_SET_PIN = 21;

 public static final int           OPEN_GL_OBJECT_TYPE_NOTE = 0;
 public static final int           OPEN_GL_OBJECT_TYPE_PATH = 1;

 public static final int           ORIENTATION_UNKNOWN = -1;
 public static final int           ORIENTATION_PORTRAIT = 0;
 public static final int           ORIENTATION_LANDSCAPE = 1;
 public static final int           ORIENTATION_INVERSE_PORTRAIT = 2;
 public static final int           ORIENTATION_INVERSE_LANDSCAPE = 3;

 public static final int           MODE_PORTRAIT = 0;
 public static final int           MODE_LANDSCAPE = 1;

 public static final String        PHOTO_CAPTURE_ON_START_LABEL = "ON_START";
 public static final String        PHOTO_CAPTURE_ON_SHUTTER_LABEL = "ON_SHUTTER";

 public static final String        FIREBASE_MESSAGES_TAG_LABEL = "FIREBASE_MESSAGES_TAG";

 public static final String        MAP_MAX_LABEL_CHARS = "0123456789";//10
 public static final String        MAP_MAX_LABEL_TRUNCATION_SUFFIX = "...";

 public static final long          MAX_FILE_SIZE = 4000000000L;//4Gb

 public static final long          HEAP_MARGIN = 6 * 1024 * 1024;//6Mb

 public static final int           MAX_FILE_BUFFER_SIZE = 1024 * 1024;//1Mb

 public static final int           EXECUTE_PLAY_AUDIO = -2;
 public static final int           EXECUTE_RECORD_AUDIO = -3;
 public static final int           EXECUTE_PLAY_VIDEO = -4;
 public static final int           EXECUTE_CAPTURE_VIDEO = -5;
 public static final int           EXECUTE_PAUSE_AUDIO = -6;
 public static final int           EXECUTE_PREV_AUDIO = -7;
 public static final int           EXECUTE_NEXT_AUDIO = -8;
 public static final int           EXECUTE_NOTIFY_MEDIA_CONTROLLER = -9;
 public static final int           EXECUTE_DISMISS_MEDIA_CONTROLLER = -10;
 public static final int           EXECUTE_REFRESH_DATA = -11;

 public static final int           TTS_INIT_ENGINE_ERROR = -2;
 public static final int           TTS_INIT_LANG_ERROR = -1;
 public static final int           TTS_INIT_SUCCESS = 0;

 public static final String        PRINT_MANAGER_JOB_NAME = "PrintManagerJob";
 public static final String        PRINT_MANAGER_DOCUMENT_NAME_PREFIX = "Data_";

 public static final int           REQ_ALL_APP_PERMISSIONS = 0;

 public static final int           ROW_BACKGROUND_COLOR_THEME_LIGHT = Color.argb(255, 185, 185, 185);
 public static final int           ROW_BACKGROUND_COLOR_ALTERNATE_THEME_LIGHT = Color.argb(255, 175, 175, 175);
 public static final int           ROW_BACKGROUND_COLOR_THEME_DARK = Color.argb(225, 30, 30, 30);
 public static final int           ROW_BACKGROUND_COLOR_ALTERNATE_THEME_DARK = Color.argb(225, 20, 20, 20);

 public static final int           COLOR_PANTONE_660C = Color.rgb(70, 120, 200);//https://codebeautify.org/rgb-to-pantone-converter

 public static final int           MAX_COMMENTS_CHARS = 64;
 public static final int           MAX_URL_CHARS = 256;

 public static final int           TOTAL_PIN_CHARS = 6;

 public static final String        HTTP_STRING_CR = "\r";
 public static final String        HTTP_STRING_LF = "\n";
 public static final String        HTTP_STRING_CRLF = HTTP_STRING_CR + HTTP_STRING_LF;
 public static final String        HTTP_STRING_TWO_HYPHENS = "--";
 public static final String        HTTP_STRING_BOUNDARY = "*****";
}