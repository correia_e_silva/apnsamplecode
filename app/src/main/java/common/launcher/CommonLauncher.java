package common.launcher;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Executors;


import common.CommonConstants;
import common.commonUtilities.CommonUtilities;


public abstract class CommonLauncher extends AppCompatActivity implements CommonConstants
{
    private Bundle   extras;
    private String   fileName;
    private Class    targetClass;
    private String[] appPermissions;


    private boolean checkPermissions()
    {

        for (int i = 0;
             i < appPermissions.length;
             i ++)
        {
            if (ContextCompat.checkSelfPermission(this, appPermissions[i]) != PackageManager.PERMISSION_GRANTED)
            {
                return false;
            }
        }

        return true;
    }


    private void launchTarget()
    {
        Intent intent;

        intent = new Intent(this, targetClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        if (extras != null)
        {
            intent.putExtras(extras);
        }

        if (fileName != null)
        {
            intent.putExtra(INTENT_PARAMS_STRING_LABEL, fileName);
        }

        startActivity(intent);
        finish();
    }


    private void authenticate()
    {
        BiometricPrompt biometricPrompt;
        BiometricPrompt.PromptInfo promptInfo;

        if (CommonUtilities.getPreference(this, AUTHENTICATION_TAG).compareTo(NONE_LABEL) == 0)
        {
            launchTarget();
        }
        else if (CommonUtilities.getPreference(this, AUTHENTICATION_TAG).compareTo(PIN_LABEL) == 0)
        {
            if (Calendar.getInstance().getTimeInMillis() < Long.parseLong(CommonUtilities.getPreference(this, PIN_LAST_FAILED_INPUT_TIME_TAG, 0)))
            {
                finish();
            }
        }
        else if (CommonUtilities.getPreference(this, AUTHENTICATION_TAG).compareTo(FINGERPRINT_LABEL) == 0)
        {
            switch (BiometricManager.from(this).canAuthenticate())
            {
                case BiometricManager.BIOMETRIC_SUCCESS:
                    break;
                case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                    Log.e(getClass().getSimpleName(), "There is no biometric hardware");
                    finish();
                    return;
                case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                    Log.e(getClass().getSimpleName(), "The hardware is unavailable. Try again later");
                    finish();
                    return;
                case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                    Log.w(getClass().getSimpleName(), "The user does not have any biometrics enrolled");
                    launchTarget();
                    return;
            }

            biometricPrompt = new BiometricPrompt(this, Executors.newSingleThreadExecutor(), new BiometricPrompt.AuthenticationCallback()
            {


                @Override
                public void onAuthenticationError(final int             errorCode,
                                                  @NonNull CharSequence errString)
                {

                    super.onAuthenticationError(errorCode, errString);

                    if (errorCode != BiometricPrompt.ERROR_NEGATIVE_BUTTON)
                    {
                        Log.e(getClass().getSimpleName(), "Unexpected error " + errString);
                    }

                    finish();
                }


                @Override
                public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result)
                {

                    super.onAuthenticationSucceeded(result);

                    Log.d(getClass().getSimpleName(), "Fingerprint recognised");
                    launchTarget();
                }


                @Override
                public void onAuthenticationFailed()
                {

                    super.onAuthenticationFailed();

                    Log.e(getClass().getSimpleName(), "Fingerprint not recognised");
                }
            });

            promptInfo = new BiometricPrompt.PromptInfo.Builder().setDeviceCredentialAllowed(true).build();

            biometricPrompt.authenticate(promptInfo);
        }
        else
        {
            launchTarget();
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        Intent intent;
        Uri uri;
        List<String> appPermissionsList;

        super.onCreate(savedInstanceState);

        CommonUtilities.adjustFontScale(this, getResources().getConfiguration());

        intent = getIntent();

        extras = intent.getExtras();

        uri = intent.getData();

        if (uri == null)
        {
            fileName = null;
        }
        else
        {
            fileName = CommonUtilities.getUriPath(this, uri);
        }

        targetClass = getTargetClass();

        appPermissionsList = new ArrayList<String>();
        appPermissionsList.addAll(Arrays.<String>asList(getAppPermissions()));

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU)
        {
            appPermissionsList.remove(Manifest.permission.POST_NOTIFICATIONS);
            appPermissionsList.remove(Manifest.permission.READ_MEDIA_IMAGES);
            appPermissionsList.remove(Manifest.permission.READ_MEDIA_AUDIO);
            appPermissionsList.remove(Manifest.permission.READ_MEDIA_VIDEO);
        }
        else
        {
            appPermissionsList.remove(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (BiometricManager.from(this).canAuthenticate() == BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE)
        {
            Log.d(getClass().getSimpleName(), "There is no biometric hardware");
            appPermissionsList.remove(Manifest.permission.USE_BIOMETRIC);
        }

        appPermissions = new String[appPermissionsList.size()];
        appPermissionsList.toArray(appPermissions);

        if (checkPermissions())
        {
            authenticate();
        }
        else
        {
            ActivityCompat.requestPermissions(this, appPermissions, REQ_ALL_APP_PERMISSIONS);
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().getAttributes().layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
    }


    @Override
    public void attachBaseContext(Context context)
    {

        super.attachBaseContext(CommonUtilities.updateLocale(context));
    }


    protected abstract String[] getAppPermissions();
    protected abstract int getAppPermissionsRequestActionCode();
    protected abstract Class getTargetClass();
    protected abstract View getContentView();
    protected abstract String getDialogTitle();
    protected abstract int getDialogIcon();
}