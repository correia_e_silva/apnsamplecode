package common.nativeBridge;


public class NativeBridge
{
    static
    {
        System.loadLibrary("native_bridge_jni");
    }


    private static NativeBridge mInstance = new NativeBridge();


    private NativeBridge()
    {

        nativeInitialize();
    }


    public static NativeBridge getInstance()
    {

        return mInstance;
    }


    public void timeBomb()
    {

        nativeTimeBomb("everyPurposeNotesBusiness/noteScreens/BuildConfig");
    }


    private native int nativeInitialize();


    private native void nativeTimeBomb(String buildConfigFieldClass);
}