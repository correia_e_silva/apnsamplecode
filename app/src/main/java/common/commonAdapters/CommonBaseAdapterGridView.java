package common.commonAdapters;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import java.util.List;


import common.CommonConstants;


public class CommonBaseAdapterGridView extends BaseAdapter implements CommonConstants
{
    private Context context;
    private List    rowsData;
    private int     itemBackground, itemWidth, itemHeight;
    private float   itemOpacity;


    public CommonBaseAdapterGridView(Context context,
                                     List    rowsData,
                                     int     itemBackground)
    {

        this.context = context;
        this.rowsData = rowsData;
        this.itemBackground = itemBackground;

        itemOpacity = 1.0f;
        itemWidth = GridView.LayoutParams.MATCH_PARENT;
        itemHeight = GridView.LayoutParams.MATCH_PARENT;
 }


 @Override
 public int getCount()
 {

     return rowsData.size();
 }


 @Override
 public Object getItem(int position)
 {

     return rowsData.get(position);
 }


 public long getItemId(int position)
 {

     return position;
 }


 @Override
 public View getView(int       position,
                     View      convertView,
                     ViewGroup parent)
 {
     View view;

     if (convertView == null)

     {
         view = new ImageView(context);
         view.setLayoutParams(new GridView.LayoutParams(itemWidth, itemHeight));
         view.setAlpha(itemOpacity);
         ((ImageView) view).setAdjustViewBounds(true);
         ((ImageView) view).setScaleType(ImageView.ScaleType.CENTER_INSIDE);
         if (itemBackground != UNDEF_PICTURE_BACKGROUND)
         {
             view.setBackgroundResource(itemBackground);
         }
     }
     else
     {
         view = convertView;
     }

     ((ImageView) view).setImageDrawable((Drawable) getItem(position));

     return view;
 }


 public void add(Object object)
 {

     rowsData.add(object);
 }


 public void remove(Object object)
 {

     rowsData.remove(object);
 }


 public void clear()
 {

     rowsData.clear();
 }


 public void setItemOpacity(float opacity)
 {

     itemOpacity = opacity;
 }


 public void setItemWidth(int width)
 {

     itemWidth = width;
 }


 public void setItemHeight(int height)
 {

     itemHeight = height;
 }
}