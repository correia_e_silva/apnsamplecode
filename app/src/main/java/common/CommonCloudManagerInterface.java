package common;


import android.content.Context;
import android.os.Handler;
import java.util.Map;
import java.util.concurrent.Future;


import internal.cloudManager.CustomCloudManager;
import internal.cloudManager.DropboxCloudManager;


public interface CommonCloudManagerInterface
{
    public enum CloudManagerType
    {
        Default,
        Custom
    }


    public static class CommonCloudManagerInterfaceProvider
    {


        public static CommonCloudManagerInterface getInstance(CloudManagerType cloudManagerType,
                                                              Context          context)
        {

            switch (cloudManagerType)
            {
                case Default:
                    return DropboxCloudManager.getInstance(context);
                case Custom:
                    return CustomCloudManager.getInstance(context);
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }


    public interface SessionCallback
    {


        public void onLoggedIn();


        public void onLoggedOut();
    }


    public interface MessageCallback
    {


        public void onMessageReceived(String message);


        public void onMessageError(Exception exception);
    }


    public interface DeleteCallback
    {


        public void onDeleteError(Exception exception);
    }


    public interface DownloadCallback
    {


        public void onDownloadStart(int totalItems);


        public void onItemDownloaded(int    index,
                                     String itemName,
                                     int    totalItems);


        public void onDownloadError(Exception exception);


        public void onDownloadStop();
    }


    public interface UploadCallback
    {


        public void onUploadStart(int totalItems);


        public void onItemUploaded(int    index,
                                   String itemName,
                                   int    totalItems);


        public void onUploadError(Exception exception);


        public void onUploadStop();
    }


    public void setCallbacks(SessionCallback  sessionCallback,
                             MessageCallback  messageCallback,
                             DeleteCallback   deleteCallback,
                             DownloadCallback downloadCallback,
                             UploadCallback   uploadCallback);


    public void initialize();


    public void release();


    public void logIn();


    public void logOut();


    public Future<String> receiveMessage();


    public Future<Boolean> sendMessage(String message);


    public Future<Boolean> delete(String fileName);


    public void download(String              inputFilesPath,
                         String[]            inputFilesExtensions,
                         String              outputFilesPath,
                         Handler             handler,
                         boolean             spawnThread,
                         Map<String, String> httpHeaders) throws Exception;


    public void upload(String              inputFilesPath,
                       String[]            inputFilesExtensions,
                       String              outputFilesPath,
                       Handler             handler,
                       boolean             spawnThread,
                       Map<String, String> httpHeaders) throws Exception;
}