package common.fragments


import android.animation.ObjectAnimator
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.Typeface
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.*
import android.view.animation.AlphaAnimation
import android.view.GestureDetector.SimpleOnGestureListener
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ZoomButtonsController.OnZoomListener
import androidx.annotation.UiThread
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import java.util.Calendar
import java.util.HashSet


import kotlin.math.abs
import kotlin.math.roundToInt
import kotlin.math.sqrt
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlin.collections.ArrayList


import common.CommonConstants
import common.CommonInterface
import common.bitmapManager.CommonBitmapManager
import common.commonAdapters.CommonBaseAdapterGallery
import common.commonNotificationMenu.CommonNotificationMenu
import common.commonUtilities.CommonUtilities
import common.progressDialog.CommonProgressDialog
import everyPurposeNotesBusiness.noteScreens.R


/**
 * CommonAlbum display pictures in an album. It uses a set of extended data which includes a set of icons to be used in notifications,
 * a set of files which defines the universe of pictures to be displayed and the index of the current picture.
 * The user can pinch/zoom the pictures. Double-tapping a picture will fit the picture to the display
 */
open class CommonAlbumFragment : Fragment()
{
    private enum class TouchMode
    {
        MODE_NONE,
        MODE_DRAG,
        MODE_ZOOM
    }


    companion object
    {
        private const val LOAD_MSG: Int = R.string.common_loading_snd

        private const val HIDE_CONTROLS_TIMEOUT: Long = 3000

        private const val ZOOM_IN_STEP = 1.5f
        private const val ZOOM_OUT_STEP = 0.5f

        private const val EXECUTE_UNDEF = -1
    }


    private var   MIN_ZOOM_SPAN = 10.0f / 1.5f //dip (10 pixels in 240 dpi)


    private val   parentJob = Job()
    private val   uiCoroutineScope = CoroutineScope(Dispatchers.Main + parentJob)
    private var   uiCoroutineScopeCancellableJob: Job? = null

    private var   gestureDetector: GestureDetector? = null

    protected var notificationMenuIcons: IntArray? = null
    private var   elems: ArrayList<*>? = null
    private var   elemsLabels: ArrayList<String?>? = null
    private var   currElemIndex = 0
    private var   startX = 0f
    private var   startY = 0f
    private var   midX = 0f
    private var   midY = 0f
    private var   prevSpan = 0f
    private var   lastSpan = 0f
    private var   currMatrixScaleFactor = 0f
    private var   viewMinX = 0f
    private var   viewMaxX = 0f
    private var   viewMinY = 0f
    private var   viewMaxY = 0f
    private var   viewWidth = 0f
    private var   viewHeight = 0f
    private var   touchMode: TouchMode? = null
    private var   prevMatrix: Matrix? = null
    private var   currMatrix: Matrix? = null
    private val   currMatrixValues = FloatArray(9)
    private var   selectedView: ImageView? = null
    private var   waitForGalleryOnMeasure = false
    private var   isScrollAllowed = false
    private var   fstStart = false
    private var   handler: Handler? = null
    private var   hideControlsRunnable: Runnable? = null
    private var   progressDialog: CommonProgressDialog? = null
    private var   rowsData: ArrayList<*>? = null
    private var   commonBaseAdapter: CommonBaseAdapterGallery? = null
    private var   gallery: Gallery? = null
    protected var textView: TextView? = null
    private var   progressBar: ProgressBar? = null
    protected var linearLayout: LinearLayout? = null
    private var   showAnimation: Any? = null
    private var   hideAnimation: Any? = null
    private var   zoomButtonsController: ZoomButtonsController? = null
    private var   viewCache: Array<Bitmap?>? = null
    private var   viewCacheList: List<Bitmap?>? = null
    private var   viewBitmaps: MutableSet<Bitmap?>? = null
    private var   inProgressBitmap: Bitmap? = null
    private var   viewBitmapsTimeStamp: Long = 0
    private var   relativeLayout: RelativeLayout? = null


    @UiThread
    private suspend fun displayViewCacheBitmaps() = withContext(Dispatchers.Main)
    {

        progressDialog = CommonProgressDialog.show(context, null, getString(LOAD_MSG), true, false, 1)

        val indexesFlow = flow()
        {
            for ((i, _) in elems!!.withIndex())
            {
                emit(i)
            }
        }

        val bitmapsFlow = flow()
        {
            for (i in elems!!)
            {
                emit(inProgressBitmap)
            }
        }

        indexesFlow.zip(bitmapsFlow)
        {
            index, bitmap -> Pair(index, bitmap)
        }.map()
        {
            check(it.second != null)
            {
                getString(R.string.common_error_getting_file_data) + elems!![it.first] + ")"
            }

            it
        }.catch()
        { exception ->
            progressDialog!!.dismiss()
            exception.message?.let()
            {
                logData(it)
            }
        }.collect()
        {
            viewCache!![it.first] = it.second
            commonBaseAdapter!!.add(viewCache!![it.first])

            if (!viewCacheList!!.contains(null))
            {
                commonBaseAdapter!!.notifyDataSetChanged()
                gallery!!.setSelection(currElemIndex)
                progressDialog!!.dismiss()
            }
        }
    }


    @UiThread
    private suspend fun displayViewBitmaps(indices:   List<Int>,
                                           timestamp: Long) = withContext(Dispatchers.Main)
    {
        val indexesFlow = flow()
        {
            for (i in indices)
            {
                emit(i)
            }
        }

        val bitmapsFlow = flow()
        {
            for (i in indices)
            {
                val selectedItemPosition = gallery!!.selectedItemPosition

                val displayWidth = CommonUtilities.getDisplaySize(context).x
                val displayHeight = CommonUtilities.getDisplaySize(context).y

                val elemBitmap = if (elems!![i] is String)
                {
                    CommonBitmapManager.getBitmap(elems!![i] as String,
                                                  if (selectedItemPosition == i) displayWidth else displayWidth / 6,
                                                  if (selectedItemPosition == i) displayHeight else displayHeight / 6)
                }
                else
                {
                    CommonBitmapManager.getBitmap(context,
                                                  elems!![i] as Int,
                                                  if (selectedItemPosition == i) displayWidth / 6 * 5 else displayWidth / 6,
                                                  if (selectedItemPosition == i) displayHeight / 6 * 5 else displayHeight / 6)
                }

                emit(elemBitmap)
            }
        }

        indexesFlow.zip(bitmapsFlow)
        {
            index, bitmap -> Pair(index, bitmap)
        }.map()
        {
            check(it.second != null)
            {
                getString(R.string.common_error_getting_file_data) + elems!![it.first] + ")"
            }

            it
        }.flowOn(Dispatchers.Default).catch()
        { exception ->
            progressDialog!!.dismiss()
            exception.message?.let()
            {
                logData(it)
            }
        }.collect()
        {
            if (timestamp != viewBitmapsTimeStamp)
            {
                cancel()
            }

            val storedBitmap = commonBaseAdapter!!.getItem(it.first) as Bitmap

            commonBaseAdapter!!.set(it.first, it.second)

            if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB)
            {
                if (!storedBitmap.sameAs(inProgressBitmap))
                {
                    //Remove reference to low or high resolution picture, so that it can be garbage collected
                    viewBitmaps!!.remove(storedBitmap)
                }
            }

            viewBitmaps!!.add(it.second)

            selectedView = gallery!!.selectedView as ImageView?

            if (gallery!!.getPositionForView(selectedView) == it.first)
            {
                //Refresh selected view
                selectedView!!.run()
                {
                    setImageBitmap(commonBaseAdapter!!.getItem(it.first) as Bitmap)
                    invalidate()
                }
                progressBar!!.visibility = View.INVISIBLE
                waitForGalleryOnMeasure = true
            }
        }
    }


    /**
     * Calculates the boundaries of a picture
     */
    private val pictureData: Unit
        get()
        {

            currMatrix!!.getValues(currMatrixValues)

            currMatrixScaleFactor = currMatrixValues[Matrix.MSCALE_X]

            viewMinX = currMatrixValues[Matrix.MTRANS_X]
            viewMaxX = currMatrixValues[Matrix.MTRANS_X] + selectedView!!.drawable.bounds.right * currMatrixScaleFactor
            viewMinY = currMatrixValues[Matrix.MTRANS_Y]
            viewMaxY = currMatrixValues[Matrix.MTRANS_Y] + selectedView!!.drawable.bounds.bottom * currMatrixScaleFactor

            viewWidth = viewMaxX - viewMinX
            viewHeight = viewMaxY - viewMinY
        }


    /**
     * Fits a picture to the display
     * @param width picture width
     * @param height picture height
     */
    private fun fitPictureToView(width:  Float,
                                 height: Float)
    {

        if (touchMode == TouchMode.MODE_ZOOM)
        {
            selectedView!!.run()
            {
                scaleType = ImageView.ScaleType.MATRIX
                imageMatrix = currMatrix
                scrollTo(0, 0)
            }
        }
        else
        {
            selectedView!!.scaleType = ImageView.ScaleType.FIT_CENTER
        }

        currMatrix!!.reset()

        pictureData

        val scaleFactor = if (viewWidth * height > width * viewHeight) width / viewWidth else height / viewHeight

        var translationCorrectionX = 0f
        var translationCorrectionY = 0f

        currMatrix!!.postScale(scaleFactor, scaleFactor, 0f, 0f)

        if ((viewWidth * scaleFactor).roundToInt().toFloat() == width)
        {
            //Picture fits the view in X, so it may be necessary to center it in Y
            translationCorrectionY = (height - viewHeight * scaleFactor) / 2
        }

        if ((viewHeight * scaleFactor).roundToInt().toFloat() == height)
        {
            //Picture fits the view in Y, so it may be necessary to center it in X
            translationCorrectionX = (width - viewWidth * scaleFactor) / 2
        }

        currMatrix!!.postTranslate(translationCorrectionX, translationCorrectionY)
    }


    /**
     * Hides controls
     */
    protected open fun hideControls()
    {

        if (VERSION.SDK_INT < VERSION_CODES.HONEYCOMB)
        {
            textView!!.startAnimation(hideAnimation as AlphaAnimation?)
        }
        else
        {
            (hideAnimation as ObjectAnimator?)!!.start()
        }

        textView!!.visibility = View.GONE
    }


    /**
     * Shows controls
     */
    protected open fun showControls()
    {

        if (VERSION.SDK_INT < VERSION_CODES.HONEYCOMB)
        {
            textView!!.startAnimation(showAnimation as AlphaAnimation?)
        }
        else
        {
            (showAnimation as ObjectAnimator?)!!.start()
        }

        textView!!.visibility = View.VISIBLE
    }


    /**
     * Obtains the span related to a pinch
     * @param event event related to a pinch
     * @return obtained span
     */
    private fun getSpan(event: MotionEvent): Float
    {
        val x = event.getX(0) - event.getX(1)
        val y = event.getY(0) - event.getY(1)

        return sqrt((x * x + y * y).toDouble()).toFloat()
    }


    /**
     * Corrects the translation related to a drag
     * @param event event related to a drag
     */
    private fun correctTranslation(event: MotionEvent)
    {

        selectedView!!.scrollTo(0, 0)

        //Get stored matrix since translation reference must be the last ACTION_DOWN
        currMatrix!!.run()
        {
            set(prevMatrix)
            postTranslate(event.x - startX, event.y - startY)
        }

        pictureData

        var translationCorrectionX = 0f
        var translationCorrectionY = 0f

        isScrollAllowed = false

        if ((viewMinX < 0) &&
            (event.x < startX))
        {
            //Check if there are no more pixels to drag beyond view right boundary
            if (viewMaxX <= gallery!!.width)
            {
                //Translation exceeds view left boundary, so compensate translation value in X, taking into account picture width
                isScrollAllowed = true
                translationCorrectionX = abs(viewMinX).coerceAtMost(gallery!!.width - viewMaxX)
            }
        }
        else if ((viewMaxX > gallery!!.width) &&
                 (event.x > startX))
        {
            //Check if there are no more pixels to drag beyond view left boundary
            if (viewMinX >= 0)
            {
                //Translation exceeds view right boundary, so compensate translation value in X, taking into account picture width
                isScrollAllowed = true
                translationCorrectionX = -(viewMaxX - gallery!!.width).coerceAtMost(viewMinX)
            }
        }

        if ((viewMinY < 0) &&
            (event.y < startY))
        {
            //Check if there are no more pixels to drag beyond view bottom boundary
            if (viewMaxY <= gallery!!.height)
            {
                //Translation exceeds view top boundary, so compensate translation value in Y, taking into account picture width
                translationCorrectionY = abs(viewMinY).coerceAtMost(gallery!!.height - viewMaxY)
            }
        }
        else if ((viewMaxY > gallery!!.height) &&
                 (event.y > startY))
        {
            //Check if there are no more pixels to drag beyond view top boundary
            if (viewMinY >= 0)
            {
                //Translation exceeds view bottom boundary, so compensate translation value in Y, taking into account picture width
                translationCorrectionY = -(viewMaxY - gallery!!.height).coerceAtMost(viewMinY)
            }
        }

        //The following situations only apply to pictures which fit the view
        if ((viewMaxX - viewMinX).roundToInt() == gallery!!.width)
        {
            //Picture fits the view in X, so it may be necessary to center it in Y
            translationCorrectionY = (viewMinY + gallery!!.height - viewMaxY) / 2 - viewMinY
        }

        if ((viewMaxY - viewMinY).roundToInt() == gallery!!.height)
        {
            //Picture fits the view in Y, so it may be necessary to center it in X
            translationCorrectionX = (viewMinX + gallery!!.width - viewMaxX) / 2 - viewMinX
        }

        currMatrix!!.postTranslate(translationCorrectionX, translationCorrectionY)
    }


    /**
     * Obtains the minimum allowed scale factor related to a zoom
     * @return obtained minimum scale factor
     */
    private fun getMinScaleFactor(): Float =
        ((gallery!!.width / 4).toFloat() / selectedView!!.drawable.intrinsicWidth.toFloat()).coerceAtLeast((gallery!!.height / 4).toFloat() / selectedView!!.drawable.intrinsicHeight.toFloat())


    /**
     * Obtains the maximum allowed scale factor related to a zoom
     * @return obtained maximum scale factor
     */
    private fun getMaxScaleFactor(): Float =
        ((gallery!!.width * 4).toFloat() / selectedView!!.drawable.intrinsicWidth.toFloat()).coerceAtMost((gallery!!.height * 4).toFloat() / selectedView!!.drawable.intrinsicHeight.toFloat())


    /**
     * Corrects the scale related to a zoom
     * @param scaleFactor scale factor related to a zoom
     * @param isPinchZoom flag indicating if the zoom is related to a pinch
     */
    private fun correctScale(scaleFactor: Float,
                             isPinchZoom: Boolean)
    {
        var scaleFactor = scaleFactor

        selectedView!!.run()
        {
            scaleType = ImageView.ScaleType.MATRIX
            imageMatrix = currMatrix
        }

        pictureData

        val minScaleFactor = getMinScaleFactor()
        val maxScaleFactor = getMaxScaleFactor()

        if (scaleFactor * currMatrixScaleFactor < minScaleFactor)
        {
            //Enlarge scale factor so that current scale factor multiplied by it equals minimum scale factor
            scaleFactor = minScaleFactor / currMatrixScaleFactor
        }
        else if (scaleFactor * currMatrixScaleFactor > maxScaleFactor)
        {
            //Reduce scale factor so that current scale factor multiplied by it equals maximum scale factor
            scaleFactor = maxScaleFactor / currMatrixScaleFactor
        }

        if (isPinchZoom)
        {
            //Get stored matrix since scale reference must be the last ACTION_POINTER_DOWN
            currMatrix!!.set(prevMatrix)
        }
        currMatrix!!.postScale(scaleFactor,
                               scaleFactor,
                               if (isPinchZoom) midX else viewMinX + viewWidth / 2,
                               if (isPinchZoom) midY else viewMinY + viewHeight / 2)
    }


    protected open fun handleTouchEvent(event: MotionEvent)
    {
        var currSpan: Float

        gestureDetector!!.onTouchEvent(event)

        selectedView = gallery!!.selectedView as ImageView?

        when (event.action and MotionEvent.ACTION_MASK)
        {
            MotionEvent.ACTION_DOWN ->
            {
                //At first finger touch, controls are shown and touched position is stored
                handler!!.removeCallbacks(hideControlsRunnable!!)
                if (textView!!.visibility != View.VISIBLE)
                {
                    showControls()
                }
                handler!!.postDelayed(hideControlsRunnable!!, HIDE_CONTROLS_TIMEOUT)
                zoomButtonsController!!.setVisible(true)
                startX = event.x
                startY = event.y
                prevMatrix!!.set(currMatrix)
                touchMode = TouchMode.MODE_DRAG
            }
            MotionEvent.ACTION_POINTER_DOWN ->
            {
                //At second finger touch, controls are shown and position between fingers is stored
                handler!!.removeCallbacks(hideControlsRunnable!!)
                if (textView!!.visibility != View.VISIBLE)
                {
                    showControls()
                }
                handler!!.postDelayed(hideControlsRunnable!!, HIDE_CONTROLS_TIMEOUT)
                zoomButtonsController!!.setVisible(true)
                //Check if distance between fingers is enough to enter zoom mode
                if (getSpan(event).also()
                {
                    prevSpan = it
                } > MIN_ZOOM_SPAN)
                {
                    midX = (event.getX(0) + event.getX(1)) / 2
                    midY = (event.getY(0) + event.getY(1)) / 2
                    prevMatrix!!.set(currMatrix)
                    touchMode = TouchMode.MODE_ZOOM
                }
            }
            MotionEvent.ACTION_MOVE ->
            {
                //At drag or pinch, controls are shown and translation or zoom is handled
                handler!!.removeCallbacks(hideControlsRunnable!!)
                if (textView!!.visibility != View.VISIBLE)
                {
                    showControls()
                }
                handler!!.postDelayed(hideControlsRunnable!!, HIDE_CONTROLS_TIMEOUT)
                zoomButtonsController!!.setVisible(true)
                if (touchMode == TouchMode.MODE_DRAG)
                {
                    correctTranslation(event)
                }
                else if (touchMode == TouchMode.MODE_ZOOM)
                {
                    //Check if distance between fingers is enough to handle zoom
                    if (getSpan(event).also()
                    {
                        currSpan = it
                    } > MIN_ZOOM_SPAN)
                    {
                        //Check if variation related to distance between fingers is enough to handle zoom
                        if (abs(currSpan - lastSpan) > MIN_ZOOM_SPAN)
                        {
                            correctScale(currSpan / prevSpan, true)
                            isScrollAllowed = false
                        }
                        lastSpan = currSpan
                    }
                }
            }
            MotionEvent.ACTION_POINTER_UP ->
            {
                //At second finger release, zoom is handled
                if (touchMode == TouchMode.MODE_ZOOM)
                {
                    pictureData
                    if ((viewWidth < gallery!!.width) &&
                        (viewHeight < gallery!!.height))
                    {
                        //Fit picture since it's smaller than the view
                        fitPictureToView(gallery!!.width.toFloat(), gallery!!.height.toFloat())
                    }
                }
                touchMode = TouchMode.MODE_NONE
                waitForGalleryOnMeasure = false
            }
            else -> touchMode = TouchMode.MODE_NONE
        }

        selectedView!!.run()
        {
            imageMatrix = currMatrix
            invalidate()
        }
    }


    protected open fun setupViews()
    {

        CommonUtilities.adjustFontScale(requireActivity(), resources.configuration)

        //Convert dip to pixels
        MIN_ZOOM_SPAN = CommonUtilities.cnvToDip(context, MIN_ZOOM_SPAN)

        val extras = requireActivity().intent.extras!!
        notificationMenuIcons = extras.getIntArray(CommonConstants.INTENT_PARAMS_NOTIFICATION_MENU_RESOURCES_LABEL)
        elems = extras.get(CommonConstants.INTENT_PARAMS_FST_OBJECT_LABEL) as ArrayList<*>?
        elemsLabels = extras.getStringArrayList(CommonConstants.INTENT_PARAMS_SND_OBJECT_LABEL)
        currElemIndex = extras.getInt(CommonConstants.INTENT_PARAMS_INT_LABEL)

        gestureDetector = GestureDetector(context, object : SimpleOnGestureListener()
        {


            override fun onDoubleTap(e: MotionEvent): Boolean
            {

                //Fit picture since double-tap has occurred
                fitPictureToView(gallery!!.width.toFloat(), gallery!!.height.toFloat())

                waitForGalleryOnMeasure = true

                return true
            }
        })

        startX = 0.0f
        startY = 0.0f

        midX = 0.0f
        midY = 0.0f

        prevSpan = 1.0f
        lastSpan = 0.0f

        prevMatrix = Matrix()
        currMatrix = Matrix()

        touchMode = TouchMode.MODE_NONE

        for (i in currMatrixValues.indices)
        {
            currMatrixValues[i] = 0.0f
        }

        selectedView = null

        waitForGalleryOnMeasure = false

        currMatrixScaleFactor = 1.0f

        viewMinX = 0.0f
        viewMaxX = 0.0f
        viewMinY = 0.0f
        viewMaxY = 0.0f

        viewWidth = 0.0f
        viewHeight = 0.0f

        isScrollAllowed = true

        fstStart = false

        handler = Handler(Looper.getMainLooper())

        hideControlsRunnable = Runnable()
        {

            hideControls()
        }

        rowsData = ArrayList<Any?>()
        commonBaseAdapter = CommonBaseAdapterGallery(requireActivity(), rowsData, CommonConstants.UNDEF_PICTURE_BACKGROUND, true)

        gallery = object : Gallery(context)
        {


            override fun onMeasure(widthMeasureSpec:  Int,
                                   heightMeasureSpec: Int)
            {

                super.onMeasure(widthMeasureSpec, heightMeasureSpec)

                this@CommonAlbumFragment.selectedView = gallery!!.selectedView as ImageView?
                this@CommonAlbumFragment.selectedView?.run()
                {

                    val width = getDefaultSize(drawable.intrinsicWidth, widthMeasureSpec)
                    val height = getDefaultSize(drawable.intrinsicHeight, heightMeasureSpec)

                    gallery!!.setSpacing(if (width > height) height / 2 else width / 3)

                    //Fit picture since orientation has changed
                    fitPictureToView(width.toFloat(), height.toFloat())

                    imageMatrix = currMatrix
                    invalidate()
                }
            }


            override fun onTouchEvent(event: MotionEvent): Boolean
            {

                handleTouchEvent(event)

                return super.onTouchEvent(event)
            }


            override fun onFling(e1:        MotionEvent?,
                                 e2:        MotionEvent,
                                 velocityX: Float,
                                 velocityY: Float): Boolean
            {

                //Allow flinging at one third of the default speed, only if not in zoom mode
                return if (isScrollAllowed) super.onFling(e1, e2, velocityX / 2.0f, velocityY / 2.0f) else false
            }


            override fun onScroll(e1:        MotionEvent?,
                                  e2:        MotionEvent,
                                  distanceX: Float,
                                  distanceY: Float): Boolean
            {

                //Allow scrolling only if not in zoom mode
                return if (isScrollAllowed) super.onScroll(e1, e2, distanceX, distanceY) else false
            }


            override fun onDetachedFromWindow()
            {

                super.onDetachedFromWindow()

                zoomButtonsController!!.setVisible(false)
            }
        }.apply()
        {
            adapter = commonBaseAdapter
            setUnselectedAlpha(255f)
            onItemSelectedListener = object : OnItemSelectedListener
            {


                override fun onItemSelected(parent:   AdapterView<*>?,
                                            v:        View,
                                            position: Int,
                                            id:       Long)
                {

                    textView!!.text = (position + 1).toString() +
                                      "/" +
                                      commonBaseAdapter!!.count +
                                      if (elemsLabels == null || elemsLabels!![position] == null || elemsLabels!![position]!!.length == 0) "" else " - " + elemsLabels!![position]

                    this@CommonAlbumFragment.selectedView = gallery!!.selectedView as ImageView?

                    waitForGalleryOnMeasure = false

                    //Fill selected item with high resolution picture and neighbour items with low resolution pictures.
                    uiCoroutineScopeCancellableJob?.cancel()

                    progressBar!!.visibility = View.VISIBLE

                    viewBitmapsTimeStamp = Calendar.getInstance().timeInMillis

                    uiCoroutineScopeCancellableJob = uiCoroutineScope.launch()
                    {
                        displayViewBitmaps(listOf(position), viewBitmapsTimeStamp)

                        val indices = elems!!.indices.toMutableList()
                        indices.removeAt(position)

                        displayViewBitmaps(indices, viewBitmapsTimeStamp)
                    }
                }


                override fun onNothingSelected(parent: AdapterView<*>?)
                {
                }
            }
        }

        textView = TextView(context).apply()
        {
            setShadowLayer(CommonUtilities.cnvToDip(context, 2).toFloat(),
                           CommonUtilities.cnvToDip(context, 1).toFloat(),
                           CommonUtilities.cnvToDip(context, 1).toFloat(),
                           Color.BLACK)
            setTextColor(Color.WHITE)
            setTypeface(Typeface.MONOSPACE, Typeface.BOLD)
            textSize = CommonConstants.FONT_SIZE_MEDIUM
            setPadding(0,
                       CommonUtilities.cnvToDip(context, 3),
                       0,
                       0)
            gravity = Gravity.CENTER
            visibility = View.GONE
        }

        progressBar = ProgressBar(context)

        if (VERSION.SDK_INT < VERSION_CODES.HONEYCOMB)
        {
            showAnimation = AlphaAnimation(0F, 1F).apply()
            {
                duration = 0
                fillAfter = true
            }

            hideAnimation = AlphaAnimation(1F, 0F).apply()
            {
                duration = 300
                fillAfter = true
            }
        }
        else
        {
            showAnimation = ObjectAnimator.ofInt(textView, "alpha", 0, 1)
            (showAnimation as ObjectAnimator?)!!.duration = 0

            hideAnimation = ObjectAnimator.ofInt(textView, "alpha", 1, 0)
            (hideAnimation as ObjectAnimator?)!!.duration = 300
        }

        zoomButtonsController = ZoomButtonsController(gallery)
        zoomButtonsController!!.setOnZoomListener(object : OnZoomListener
        {


            override fun onVisibilityChanged(visible: Boolean)
            {
            }


            override fun onZoom(zoomIn: Boolean)
            {

                touchMode = TouchMode.MODE_ZOOM

                correctScale(if (zoomIn) ZOOM_IN_STEP else ZOOM_OUT_STEP, false)

                pictureData

                if ((viewWidth < gallery!!.width) &&
                    (viewHeight < gallery!!.height))
                {
                    //Fit picture since it's smaller than the view
                    fitPictureToView(gallery!!.width.toFloat(), gallery!!.height.toFloat())
                }

                selectedView!!.run()
                {
                    imageMatrix = currMatrix
                    invalidate()
                }

                //Workaround to allow zooming-in the first time a view is selected
                if (waitForGalleryOnMeasure)
                {
                    waitForGalleryOnMeasure = false
                    if (zoomIn)
                    {
                        handler!!.postDelayed(
                        {

                            onZoom(true)
                        }, 50)
                    }
                }
            }
        })

        viewCache = arrayOfNulls(elems!!.size)
        viewCacheList = viewCache!!.asList()

        viewBitmaps = HashSet<Bitmap?>()

        inProgressBitmap = CommonBitmapManager.getBitmap(context, R.drawable.dummy_alt, 1, 1)

        gallery!!.run()
        {
            setSelection(currElemIndex)
            //'Album' location
            layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
        }

        //'Current/total pictures' aggregation
        textView!!.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)

        linearLayout = LinearLayout(context).apply()
        {
            orientation = LinearLayout.HORIZONTAL
            gravity = Gravity.CENTER
            addView(textView)
        }

        //'Current/total pictures' location
        var layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        with (layoutParams)
        {
            addRule(RelativeLayout.ALIGN_PARENT_TOP)
            addRule(RelativeLayout.CENTER_HORIZONTAL)
        }
        linearLayout!!.layoutParams = layoutParams

        //'Progress indicator' location
        layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT)
        progressBar!!.layoutParams = layoutParams

        //'Album' + 'Current/total pictures' + 'Progress indicator' aggregation
        relativeLayout = RelativeLayout(context).apply()
        {
            setBackgroundColor(Color.BLACK)
            addView(gallery)
            addView(linearLayout)
            addView(progressBar)
        }

        requireActivity().window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB)
        {
            (requireActivity() as AppCompatActivity).supportActionBar!!.hide()
        }
    }


    override fun onCreateView(inflater:           LayoutInflater,
                              container:          ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {

        setupViews()

        return relativeLayout
    }


    /**
     * Logs an error message and shows an error dialog
     * @param message related to error
     */
    private fun logData(message: String)
    {

        Log.e(javaClass.simpleName, message)

        CommonNotificationMenu.showMessageDialog(requireActivity() as CommonInterface, message, CommonConstants.ERROR_TITLE, notificationMenuIcons!![CommonConstants.NOTIFICATION_MENU_ICONS_ERROR_INDEX], EXECUTE_UNDEF)
    }


    override fun onStart()
    {

        super.onStart()

        //Try to release memory to managed heap bitmaps (Honeycomb onwards)
        System.gc()

        if (!fstStart)
        {
            //Fill the album with 'in-progress' pictures
            uiCoroutineScope.launch()
            {
                displayViewCacheBitmaps()
            }

            fstStart = true
        }
    }


    override fun onDestroy()
    {

        super.onDestroy()

        uiCoroutineScope.cancel()

        //Recycle 'in-progress' picture
        if (inProgressBitmap != null)
        {
            inProgressBitmap!!.recycle()
        }

        //Recycle low and/or high resolution pictures
        val iterator = viewBitmaps!!.iterator()
        while (iterator.hasNext())
        {
            iterator.next()!!.recycle()
        }
    }
}