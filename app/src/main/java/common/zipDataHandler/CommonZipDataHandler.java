package common.zipDataHandler;


import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;


import common.CommonConstants;


/**
 * Common HMI window ZIP data handler class. It is used to compress files.
 */
public class CommonZipDataHandler implements CommonConstants
{
    private static final int MAX_ZIP_BUFFER_BYTES = 4096;


    private static byte[]    mZipBuffer = new byte[MAX_ZIP_BUFFER_BYTES];


    public interface ZipFileCallback
    {


        public void onFileZipped(String fileName,
                                 int    fileIndex,
                                 int    totalItems);


        public void onComplete(Boolean result,
                               Object  data);
    }


    public interface UnzipFileCallback
    {


        public void onFileUnzipped(String fileName,
                                   int    fileIndex,
                                   int    totalItems);


        public void onComplete(Boolean result,
                               Object  data);
    }


    private static void signalFileZipped(final ZipFileCallback zipFileCallback,
                                         final String          fileName,
                                         final int             fileIndex,
                                         final int             totalFiles,
                                         final boolean         signalCallbackOnUiThread)
    {

        if (zipFileCallback != null)
        {
            if (signalCallbackOnUiThread)
            {
                new Handler(Looper.getMainLooper()).post(new Runnable()
                {


                    @Override
                    public void run()
                    {

                        zipFileCallback.onFileZipped(fileName, fileIndex, totalFiles);
                    }
                });
            }
            else
            {
                zipFileCallback.onFileZipped(fileName, fileIndex, totalFiles);
            }
        }
    }


    private static void signalCompletion(final ZipFileCallback zipFileCallback,
                                         final Boolean         result,
                                         final Object          data,
                                         final boolean         signalCallbackOnUiThread)
    {

        if (zipFileCallback != null)
        {
            if (signalCallbackOnUiThread)
            {
                new Handler(Looper.getMainLooper()).post(new Runnable()
                {


                    @Override
                    public void run()
                    {

                        zipFileCallback.onComplete(result, data);
                    }
                });
            }
            else
            {
                zipFileCallback.onComplete(result, data);
            }
        }
    }


    private static ZipOutputStream handleExistingFile(String outputFileName,
                                                      int    compressionLevel) throws Exception
    {
        File outputFileDescriptor = new File(outputFileName);
        File tempOutputFileDescriptor = new File(outputFileDescriptor.getAbsolutePath() + TEMP_FILE_EXT);

        Log.d(CommonZipDataHandler.class.getSimpleName(), "Will rename existing file " + outputFileDescriptor.getAbsolutePath());

        if (!outputFileDescriptor.renameTo(tempOutputFileDescriptor))
        {
            Log.e(CommonZipDataHandler.class.getSimpleName(), "Error renaming file " + outputFileDescriptor.getAbsolutePath());
            throw new RuntimeException("Error renaming file " + outputFileDescriptor.getAbsolutePath());
        }

        ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(tempOutputFileDescriptor));

        ZipOutputStream result = new ZipOutputStream(new FileOutputStream(outputFileName));
        result.setLevel(compressionLevel);

        for (ZipEntry zipEntry = zipInputStream.getNextEntry();
             zipEntry != null;
             zipEntry = zipInputStream.getNextEntry())
        {
            result.putNextEntry(zipEntry);

            int bytesRead;

            while ((bytesRead = zipInputStream.read(mZipBuffer)) != -1)
            {
                result.write(mZipBuffer, 0, bytesRead);
            }

            result.closeEntry();
        }

        zipInputStream.close();

        tempOutputFileDescriptor.delete();

        return result;
    }


    /**
     * Common HMI window ZIP data handler method which compresses a file.
     * @param inputFileName input file name
     * @param outputFileName output file name
     * @param appendFile flag indicating if input file shall be appended if output file exists
     * @param compressionLevel compression level
     * @param zipFilePath optional ZIP file path
     * @param zipFileCallback optional callback to be invoked during the process
     * @param signalCallbackOnUiThread flag indicating if callback signaling shall be performed in the UI thread
     * @param unwantedFileNames file names to exclude
     * @return Future to use for obtaining the result of the file compression
     */
    public static Future<Boolean> zipFile(final String          inputFileName,
                                          final String          outputFileName,
                                          final boolean         appendFile,
                                          final int             compressionLevel,
                                          final String          zipFilePath,
                                          final ZipFileCallback zipFileCallback,
                                          final boolean         signalCallbackOnUiThread,
                                          final List<String>    unwantedFileNames)
    {

        return Executors.newSingleThreadExecutor().submit(new Callable<Boolean>()
        {


            @Override
            public Boolean call()
            {

                try
                {
                    ZipOutputStream zipOutputStream = null;

                    if (appendFile)
                    {
                        if (new File(outputFileName).exists())
                        {
                            zipOutputStream = handleExistingFile(outputFileName, compressionLevel);
                        }
                    }

                    if (zipOutputStream == null)
                    {
                        zipOutputStream = new ZipOutputStream(new FileOutputStream(outputFileName));
                        zipOutputStream.setLevel(compressionLevel);
                    }

                    File inputFileDescriptor;
                    File[] inputFiles;

                    if ((inputFileDescriptor = new File(inputFileName)).isDirectory())
                    {
                        inputFiles = inputFileDescriptor.listFiles();
                    }
                    else
                    {
                        inputFiles = new File[1];
                        inputFiles[0] = inputFileDescriptor.getAbsoluteFile();
                    }

                    for (int i = 0;
                         i < inputFiles.length;
                         i ++)
                    {
                        if (unwantedFileNames.contains(inputFiles[i].getAbsolutePath()))
                        {
                            Log.d(CommonZipDataHandler.class.getSimpleName(), "Will skip file " + inputFiles[i].getAbsolutePath());
                        }
                        else
                        {
                            Log.d(CommonZipDataHandler.class.getSimpleName(), "Will compress file " + inputFiles[i].getAbsolutePath());

                            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(inputFiles[i].getAbsolutePath()));

                            zipOutputStream.putNextEntry(new ZipEntry((zipFilePath == null ? "" : zipFilePath + File.separator) + inputFiles[i].getName()));

                            int bytesRead;

                            while ((bytesRead = bufferedInputStream.read(mZipBuffer)) != -1)
                            {
                                zipOutputStream.write(mZipBuffer, 0, bytesRead);
                            }

                            bufferedInputStream.close();

                            Log.d(CommonZipDataHandler.class.getSimpleName(), "Has compressed file " + inputFiles[i].getAbsolutePath());

                            signalFileZipped(zipFileCallback, inputFiles[i].getAbsolutePath(), i + 1, inputFiles.length, signalCallbackOnUiThread);
                        }
                    }

                    zipOutputStream.close();
                }

                catch (Exception exception)
                {
                    Log.e(CommonZipDataHandler.class.getSimpleName(), "Unexpected error " + exception);
                    exception.printStackTrace();
                    signalCompletion(zipFileCallback, false, exception, signalCallbackOnUiThread);
                    return false;
                }

                signalCompletion(zipFileCallback, true, null, signalCallbackOnUiThread);

                return true;
            }
        });
    }


    /**
     * Common HMI window ZIP data handler method which compresses a file.
     * @param inputFileName input file name
     * @param outputFileName output file name
     * @param appendFile flag indicating if input file shall be appended if output file exists
     * @param compressionLevel compression level
     * @param zipFilePath optional ZIP file path
     * @param zipFileCallback optional callback to be invoked during the process
     * @param signalCallbackOnUiThread flag indicating if callback signaling shall be performed in the UI thread
     * @return Future to use for obtaining the result of the file compression
     */
    public static Future<Boolean> zipFile(final String          inputFileName,
                                          final String          outputFileName,
                                          final boolean         appendFile,
                                          final int             compressionLevel,
                                          final String          zipFilePath,
                                          final ZipFileCallback zipFileCallback,
                                          final boolean         signalCallbackOnUiThread)
    {
        return zipFile(inputFileName, outputFileName, appendFile, compressionLevel, zipFilePath, zipFileCallback, signalCallbackOnUiThread, Collections.<String>emptyList());
    }


    private static void signalFileUnzipped(final UnzipFileCallback unzipFileCallback,
                                           final String            fileName,
                                           final int               fileIndex,
                                           final int               totalFiles,
                                           final boolean           signalCallbackOnUiThread)
    {

        if (unzipFileCallback != null)
        {
            if (signalCallbackOnUiThread)
            {
                new Handler(Looper.getMainLooper()).post(new Runnable()
                {


                    @Override
                    public void run()
                    {

                        unzipFileCallback.onFileUnzipped(fileName, fileIndex, totalFiles);
                    }
                });
            }
            else
            {
                unzipFileCallback.onFileUnzipped(fileName, fileIndex, totalFiles);
            }
        }
    }


    private static void signalCompletion(final UnzipFileCallback unzipFileCallback,
                                         final Boolean           result,
                                         final Object            data,
                                         final boolean           signalCallbackOnUiThread)
    {

        if (unzipFileCallback != null)
        {
            if (signalCallbackOnUiThread)
            {
                new Handler(Looper.getMainLooper()).post(new Runnable()
                {


                    @Override
                    public void run()
                    {

                        unzipFileCallback.onComplete(result, data);
                    }
                });
            }
            else
            {
                unzipFileCallback.onComplete(result, data);
            }
        }
    }


    /**
     * Common HMI window ZIP data handler method which decompresses a file.
     * @param inputFileName input file name
     * @param outputFilePath output file path
     * @param unzipFileCallback optional callback to be invoked during the process
     * @param signalCallbackOnUiThread flag indicating if callback signaling shall be performed in the UI thread
     * @return Future to use for obtaining the result of the file decompression
     */
    public static Future<Boolean> unzipFile(final String            inputFileName,
                                            final String            outputFilePath,
                                            final UnzipFileCallback unzipFileCallback,
                                            final boolean           signalCallbackOnUiThread)
    {

        return Executors.newSingleThreadExecutor().submit(new Callable<Boolean>()
        {


            @Override
            public Boolean call()
            {

                try
                {
                    ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(inputFileName));
                    int totalEntries = new ZipFile(inputFileName).size();
                    int currentEntry = 1;

                    for (ZipEntry zipEntry = zipInputStream.getNextEntry();
                         zipEntry != null;
                         zipEntry = zipInputStream.getNextEntry())
                    {
                        Log.d(CommonZipDataHandler.class.getSimpleName(), "Will uncompress file " + zipEntry.getName());

                        File filePath = new File(outputFilePath + File.separator + zipEntry.getName()).getParentFile();

                        if (!filePath.exists() &&
                            !filePath.mkdirs())
                        {
                            Log.e(CommonZipDataHandler.class.getSimpleName(), "Error accessing or creating directory (" + filePath.getAbsolutePath() + ")");
                            throw new RuntimeException("Error accessing or creating directory (" + filePath.getAbsolutePath() + ")");
                        }

                        FileOutputStream fileOutputStream = new FileOutputStream(outputFilePath + File.separator + zipEntry.getName());

                        int bytesRead;

                        while ((bytesRead = zipInputStream.read(mZipBuffer)) != -1)
                        {
                            fileOutputStream.write(mZipBuffer, 0, bytesRead);
                        }

                        fileOutputStream.close();

                        Log.d(CommonZipDataHandler.class.getSimpleName(), "Has uncompressed file " + zipEntry.getName());

                        signalFileUnzipped(unzipFileCallback, zipEntry.getName(), currentEntry ++, totalEntries, signalCallbackOnUiThread);
                    }

                    zipInputStream.close();
                }

                catch (Exception exception)
                {
                    Log.e(CommonZipDataHandler.class.getSimpleName(), "Unexpected error " + exception);
                    exception.printStackTrace();
                    signalCompletion(unzipFileCallback, false, exception, signalCallbackOnUiThread);
                    return false;
                }

                signalCompletion(unzipFileCallback, true, null, signalCallbackOnUiThread);

                return true;
            }
        });
    }
}