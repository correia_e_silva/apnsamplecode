package internal.cloudManager;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import androidx.core.util.Pair;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;


import common.CommonCloudManagerInterface;
import common.commonUtilities.CommonUtilities;
import common.messaging.CommonFirebaseMessagingBroadcastReceiver;
import common.messaging.CommonHttpClient;
import common.webView.CommonWebView;
import everyPurposeNotes.messaging.NoteMessageDataHandler;
import everyPurposeNotesBusiness.noteScreens.R;


public class CustomCloudManager extends BaseCustomCloudManager implements CommonFirebaseMessagingBroadcastReceiver.FirebaseMessagingCallback
{
    public static class CustomCloudManagerWebView extends CommonWebView
    {


        @Override
        protected void onCreate(Bundle savedInstanceState)
        {

            setTheme(CommonUtilities.getPreference(this, THEME_TAG).compareTo(LIGHT_LABEL) == 0 ? R.style.CustomThemeLight : R.style.CustomThemeDark);

            super.onCreate(savedInstanceState);
        }

        
        @Override
        protected void handleJavascriptCallbackMethod(String data)
        {

            CustomCloudManager.handleJavascriptCallbackMethod(data);
        }
    }


    private static final String       CUSTOM_ACCESS_TOKEN_TAG = "CUSTOM_ACCESS_TOKEN";


    private static CustomCloudManager mCustomCloudManager = null;
    private Map<String, String>       mHttpHeaders;
    private NoteMessageDataHandler    mNoteMessageDataHandler;


    private CustomCloudManager(Context context)
    {

        super(context);

        mHttpHeaders = new HashMap<String, String>();
        mNoteMessageDataHandler = NoteMessageDataHandler.getInstance(mContext);
    }


    public static synchronized CommonCloudManagerInterface getInstance(Context context)
    {

        if (mCustomCloudManager == null)
        {
            mCustomCloudManager = new CustomCloudManager(context);
        }

        return mCustomCloudManager;
    }


    private static synchronized void handleJavascriptCallbackMethod(String data)
    {

        mCustomCloudManager.setAccessToken(data);
        mCustomCloudManager.initialize();
    }


    @Override
    public void initialize()
    {

        CommonFirebaseMessagingBroadcastReceiver.getInstance().addCallback(this);

        setLoggedIn(getAccessToken() != null);
    }


    @Override
    public void release()
    {

        CommonFirebaseMessagingBroadcastReceiver.getInstance().removeCallback(this);
    }


    private void sendAuthenticationMessage(NoteMessageDataHandler.DATA_ACTION dataAction)
    {
        String requestMessage;

        requestMessage = mNoteMessageDataHandler.buildAuthenticationRequestMessage(Calendar.getInstance().getTimeInMillis(), dataAction);
        sendMessage(requestMessage);
    }


    @Override
    public void logIn()
    {

        sendAuthenticationMessage(NoteMessageDataHandler.DATA_ACTION.LOGIN);
    }


    @Override
    public void logOut()
    {

        sendAuthenticationMessage(NoteMessageDataHandler.DATA_ACTION.LOGOUT);
    }


    @Override
    protected String getAccessTokenTag()
    {

        return CUSTOM_ACCESS_TOKEN_TAG;
    }


    @Override
    protected int getDownloadThreadPoolSize()
    {

        return Runtime.getRuntime().availableProcessors() + 1;
    }


    @Override
    protected int getUploadThreadPoolSize()
    {

        return Runtime.getRuntime().availableProcessors() + 1;
    }


    @Override
    protected List<String> getFileNamesList(String   inputFilesPath,
                                            String[] inputFilesExtensions) throws Exception
    {

        return Arrays.<String>asList(inputFilesExtensions);
    }


    @Override
    protected void downloadFile(String              inputFilesPath,
                                String              outputFilesPath,
                                String              fileName,
                                int                 totalItems,
                                AtomicInteger       totalTransferredItems,
                                Map<String, String> httpHeaders) throws Exception
    {

        CommonHttpClient.downloadFile(CommonUtilities.getPreference(mContext, CLOUD_SERVER_ADDRESS_TAG), inputFilesPath, outputFilesPath, fileName, httpHeaders, false);

        mDownloadCallback.onItemDownloaded(totalTransferredItems.getAndIncrement(), fileName, totalItems);
    }


    @Override
    protected void uploadFile(String              inputFilesPath,
                              String              outputFilesPath,
                              String              fileName,
                              int                 totalItems,
                              AtomicInteger       totalTransferredItems,
                              Map<String, String> httpHeaders) throws Exception
    {

        mHttpHeaders.clear();
        mHttpHeaders.putAll(HTTP_HEADERS_UPLOAD_FILE);

        if ((httpHeaders != null) &&
            !httpHeaders.isEmpty())
        {
            mHttpHeaders.putAll(httpHeaders);
        }

        CommonHttpClient.uploadFile(CommonUtilities.getPreference(mContext, CLOUD_SERVER_ADDRESS_TAG), inputFilesPath, outputFilesPath, fileName, mHttpHeaders);

        mUploadCallback.onItemUploaded(totalTransferredItems.getAndIncrement(), fileName, totalItems);
    }


    @Override
    public void onPushNotification(CommonFirebaseMessagingBroadcastReceiver.FIREBASE_PUSH_NOTIFICATION_TYPE messageType,
                                   String                                                                   messageData)
    {
        Pair<Map.Entry<String, NoteMessageDataHandler.NoteMessageData>, NoteMessageDataHandler.DATA_RESULT> result;
        Intent intent;

        try
        {
            if (messageType != CommonFirebaseMessagingBroadcastReceiver.FIREBASE_PUSH_NOTIFICATION_TYPE.FIREBASE_PUSH_NOTIFICATION_JSON_DATA)
            {
                return;
            }

            result = mNoteMessageDataHandler.processAuthenticationMessage(messageData);

            if (result != null)
            {
                switch (result.first.getValue().mDataAction)
                {
                    case LOGIN:
                        if (result.second == NoteMessageDataHandler.DATA_RESULT.APN_SUCCESS)
                        {
                            initialize();
                        }
                        else if (result.second == NoteMessageDataHandler.DATA_RESULT.APN_FAILURE_NOT_LOGGED_IN)
                        {
                            intent = new Intent(mContext, CustomCloudManagerWebView.class);
                            intent.putExtra(INTENT_PARAMS_STRING_LABEL, result.first.getValue().mInfo);
                            mContext.startActivity(intent);
                        }
                        break;
                    case LOGOUT:
                        if (result.second == NoteMessageDataHandler.DATA_RESULT.APN_SUCCESS)
                        {
                            clearAccessToken();
                            setLoggedIn(false);
                        }
                        break;
                }
            }
        }

        catch (Exception exception)
        {
            Log.e(getClass().getSimpleName(), "Unexpected error " + exception);
            exception.printStackTrace();
        }
    }


    @Override
    protected Object clone() throws CloneNotSupportedException
    {

        throw new CloneNotSupportedException("Clone is not allowed.");
    }
}