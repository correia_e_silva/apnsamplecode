package internal.cloudManager;


import android.content.Context;
import android.util.Log;
import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxOAuth1AccessToken;
import com.dropbox.core.DbxOAuth1Upgrader;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.android.Auth;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.DeleteErrorException;
import com.dropbox.core.v2.files.ListFolderErrorException;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.files.WriteMode;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;


import common.CommonCloudManagerInterface;
import common.CommonConstants;
import common.commonUtilities.CommonUtilities;


public class DropboxCloudManager extends AbstractCloudManager implements CommonConstants
{
    private static final String        DROPBOX_ACCESS_KEY_TAG = "DROPBOX_ACCESS_KEY";
    private static final String        DROPBOX_ACCESS_SECRET_TAG = "DROPBOX_ACCESS_SECRET";
    private static final String        DROPBOX_ACCESS_TOKEN_TAG = "DROPBOX_ACCESS_SECRET";

    private static final String        DROPBOX_APP_KEY = "";
    private static final String        DROPBOX_APP_SECRET = "";


    private static DropboxCloudManager mDropboxCloudManager = null;
    private DeleteCallback             mDeleteCallback;
    private DbxRequestConfig           mDbxRequestConfig;
    private DbxClientV2                mDbxClient;


    private String[] getKeys()
    {
        String key, secret;
        String[] result;

        key = CommonUtilities.getPreference(mContext, DROPBOX_ACCESS_KEY_TAG);
        secret = CommonUtilities.getPreference(mContext, DROPBOX_ACCESS_SECRET_TAG);

        result = null;

        if ((key.length() != 0) &&
            (secret.length() != 0))
        {
            result = new String[2];
            result[0] = key;
            result[1] = secret;
        }

        return result;
    }


    private void clearKeys()
    {

        CommonUtilities.unsetPreference(mContext, DROPBOX_ACCESS_KEY_TAG);
        CommonUtilities.unsetPreference(mContext, DROPBOX_ACCESS_SECRET_TAG);
    }


    private String handleAccessToken()
    {
        String oAuth2AccessToken;
        String[] oAuth1Keys;
        DbxAppInfo dbxAppInfo;
        DbxOAuth1Upgrader dbxOAuth1Upgrader;
        DbxOAuth1AccessToken dbxOAuth1AccessToken;

        try
        {
            if ((oAuth2AccessToken = getAccessToken()) == null)
            {
                if ((oAuth1Keys = getKeys()) == null)
                {
                    if ((oAuth2AccessToken = Auth.getOAuth2Token()) != null)
                    {
                        setAccessToken(oAuth2AccessToken);
                    }
                }
                else
                {
                    dbxAppInfo = new DbxAppInfo(DROPBOX_APP_KEY, DROPBOX_APP_SECRET);
                    dbxOAuth1Upgrader = new DbxOAuth1Upgrader(mDbxRequestConfig, dbxAppInfo);
                    dbxOAuth1AccessToken = new DbxOAuth1AccessToken(oAuth1Keys[0], oAuth1Keys[1]);

                    if ((oAuth2AccessToken = dbxOAuth1Upgrader.createOAuth2AccessToken(dbxOAuth1AccessToken)) != null)
                    {
                        setAccessToken(oAuth2AccessToken);
                        dbxOAuth1Upgrader.disableOAuth1AccessToken(dbxOAuth1AccessToken);
                        clearKeys();
                    }
                }
            }
        }

        catch (Exception exception)
        {
            Log.e(getClass().getSimpleName(), "Unexpected error " + exception);
            exception.printStackTrace();

            oAuth2AccessToken = null;
        }

        return oAuth2AccessToken;
    }


    private DropboxCloudManager(Context context)
    {

        super(context);

        mDbxRequestConfig = new DbxRequestConfig(getClass().getSimpleName());
        mDbxClient = null;
    }


    public static synchronized CommonCloudManagerInterface getInstance(Context context)
    {

        if (mDropboxCloudManager == null)
        {
            mDropboxCloudManager = new DropboxCloudManager(context);
        }

        return mDropboxCloudManager;
    }


    @Override
    public void setCallbacks(SessionCallback  sessionCallback,
                             MessageCallback  messageCallback,
                             DeleteCallback   deleteCallback,
                             DownloadCallback downloadCallback,
                             UploadCallback   uploadCallback)
    {

        super.setCallbacks(sessionCallback, downloadCallback, uploadCallback);

        mDeleteCallback = deleteCallback;
    }


    @Override
    public void initialize()
    {
        String accessToken;

        if ((accessToken = handleAccessToken()) != null)
        {
            mDbxClient = new DbxClientV2(mDbxRequestConfig, accessToken);
        }

        setLoggedIn(mDbxClient != null);
    }


    @Override
    public void release()
    {

        throw new UnsupportedOperationException();
    }


    @Override
    public void logIn()
    {

        Auth.startOAuth2Authentication(mContext, DROPBOX_APP_KEY);

        initialize();
    }


    @Override
    public void logOut()
    {

        mDbxClient = null;

        clearAccessToken();

        setLoggedIn(false);
    }


    @Override
    public Future<String> receiveMessage()
    {
    
        throw new UnsupportedOperationException();  
    }


    @Override
    public Future<Boolean> sendMessage(String message)
    {

        throw new UnsupportedOperationException();
    }


    private void signalDeleteError(Exception exception)
    {

        if (mDeleteCallback != null)
        {
            mDeleteCallback.onDeleteError(exception);
        }
    }


    @Override
    public Future<Boolean> delete(final String fileName)
    {

        return Executors.newSingleThreadExecutor().submit(new Callable<Boolean>()
        {


            @Override
            public Boolean call()
            {

                try
                {
                    mDbxClient.files().deleteV2(File.separator + fileName);
                }

                catch (Exception exception)
                {
                    if (!((exception instanceof DeleteErrorException) &&
                          ((DeleteErrorException) exception).errorValue.isPathLookup() &&
                          ((DeleteErrorException) exception).errorValue.getPathLookupValue().isNotFound()))
                    {
                        Log.e(getClass().getSimpleName(), "Unexpected error " + exception);
                        exception.printStackTrace();
                        signalDeleteError(exception);
                        return false;
                    }
                }

                return true;
            }
        });
    }


    @Override
    protected String getAccessTokenTag()
    {

        return DROPBOX_ACCESS_TOKEN_TAG;
    }


    @Override
    protected int getDownloadThreadPoolSize()
    {

        return Runtime.getRuntime().availableProcessors() + 1;
    }


    @Override
    protected int getUploadThreadPoolSize()
    {

        return 1;//Workaround for RateLimitException issue...
    }


    @Override
    protected List<String> getFileNamesList(String   inputFilesPath,
                                            String[] inputFilesExtensions) throws Exception
    {
        List<String> result;
        ListFolderResult entry;
        List<Metadata> metadatas;

        result = new ArrayList<String>();

        if ((inputFilesExtensions != null) &&
            (inputFilesExtensions.length == 1) &&
            CommonUtilities.isFile(inputFilesExtensions[0]))
        {
            result.add(inputFilesExtensions[0]);
            return result;
        }

        metadatas = new ArrayList<Metadata>();

        try
        {
            entry = mDbxClient.files().listFolder(File.separator + inputFilesPath);

            for (Metadata content : entry.getEntries())
            {
                if ((inputFilesExtensions == null) ||
                    CommonUtilities.isFileKnown(content.getPathLower(), inputFilesExtensions))
                {
                    metadatas.add(content);
                }
            }
        }

        catch (ListFolderErrorException exception)
        {
            if (exception.errorValue.isPath() &&
                exception.errorValue.getPathValue().isNotFound())
            {
                Log.d(getClass().getSimpleName(), "Will create remote folder " + File.separator + inputFilesPath);
                mDbxClient.files().createFolderV2(File.separator + inputFilesPath);
            }
        }

        for (Metadata metadata : metadatas)
        {
            result.add(metadata.getName());
        }

        return result;
    }


    @Override
    protected void downloadFile(String              inputFilesPath,
                                String              outputFilesPath,
                                String              fileName,
                                int                 totalItems,
                                AtomicInteger       totalTransferredItems,
                                Map<String, String> httpHeaders) throws Exception
    {
        File file;
        FileOutputStream fileOutputStream;

        file = new File(outputFilesPath + File.separator + fileName);
        fileOutputStream = new FileOutputStream(file);
        mDbxClient.files().download(File.separator + inputFilesPath + File.separator + fileName, null).download(fileOutputStream);

        mDownloadCallback.onItemDownloaded(totalTransferredItems.getAndIncrement(), fileName, totalItems);
    }


    @Override
    protected void uploadFile(String              inputFilesPath,
                              String              outputFilesPath,
                              String              fileName,
                              int                 totalItems,
                              AtomicInteger       totalTransferredItems,
                              Map<String, String> httpHeaders) throws Exception
    {
        File file;
        FileInputStream fileInputStream;

        file = new File(inputFilesPath + File.separator + fileName);
        fileInputStream = new FileInputStream(file);
        mDbxClient.files().uploadBuilder(File.separator + outputFilesPath + File.separator + fileName)
                          .withMode(WriteMode.OVERWRITE)
                          .uploadAndFinish(fileInputStream);

        mUploadCallback.onItemUploaded(totalTransferredItems.getAndIncrement(), fileName, totalItems);
    }


    @Override
    protected Object clone() throws CloneNotSupportedException
    {

        throw new CloneNotSupportedException("Clone is not allowed.");
    }
}