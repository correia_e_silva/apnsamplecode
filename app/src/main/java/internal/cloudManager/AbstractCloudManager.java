package internal.cloudManager;


import android.content.Context;
import android.os.Handler;
import android.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;


import common.CommonCloudManagerInterface;
import common.commonUtilities.CommonUtilities;


public abstract class AbstractCloudManager implements CommonCloudManagerInterface
{
    protected Context          mContext;
    protected SessionCallback  mSessionCallback;
    protected DownloadCallback mDownloadCallback;
    protected UploadCallback   mUploadCallback;


    private abstract class TransferFileRunnable implements Runnable
    {
        private String                     mInputFilesPath, mOutputFilesPath, mFileName;
        private int                        mTotalItems;
        private AtomicInteger              mTotalTransferredItems;
        private AtomicReference<Exception> mAtomicReference;
        private ExecutorService            mExecutorService;
        private Map<String, String>        mHttpHeaders;


        public TransferFileRunnable(String                     inputFilesPath,
                                    String                     outputFilesPath,
                                    String                     fileName,
                                    int                        totalItems,
                                    AtomicInteger              totalTransferredItems,
                                    AtomicReference<Exception> atomicReference,
                                    ExecutorService            executorService,
                                    Map<String, String>        httpHeaders)
        {

            mInputFilesPath = inputFilesPath;
            mOutputFilesPath = outputFilesPath;
            mFileName = fileName;
            mTotalItems = totalItems;
            mTotalTransferredItems = totalTransferredItems;
            mAtomicReference = atomicReference;
            mExecutorService = executorService;
            mHttpHeaders = httpHeaders;
        }


        @Override
        public void run()
        {

            try
            {
                transferFile(mInputFilesPath,
                             mOutputFilesPath,
                             mFileName,
                             mTotalItems,
                             mTotalTransferredItems,
                             mHttpHeaders);
            }

            catch (Exception exception)
            {
                Log.e(getClass().getSimpleName(), "Unexpected error " + exception);
                exception.printStackTrace();

                mAtomicReference.set(exception);

                mExecutorService.shutdownNow();
            }
        }


        protected abstract void transferFile(String              inputFilesPath,
                                             String              outputFilesPath,
                                             String              fileName,
                                             int                 totalItems,
                                             AtomicInteger       totalTransferredItems,
                                             Map<String, String> httpHeaders) throws Exception;
    }


    private class DownloadFileRunnable extends TransferFileRunnable
    {


        private DownloadFileRunnable(String                     inputFilesPath,
                                     String                     outputFilesPath,
                                     String                     fileName,
                                     int                        totalItems,
                                     AtomicInteger              totalTransferredItems,
                                     AtomicReference<Exception> atomicReference,
                                     ExecutorService            executorService,
                                     Map<String, String>        httpHeaders)
        {

            super(inputFilesPath,
                  outputFilesPath,
                  fileName,
                  totalItems,
                  totalTransferredItems,
                  atomicReference,
                  executorService,
                  httpHeaders);
        }


        @Override
        protected void transferFile(String              inputFilesPath,
                                    String              outputFilesPath,
                                    String              fileName,
                                    int                 totalItems,
                                    AtomicInteger       totalTransferredItems,
                                    Map<String, String> httpHeaders) throws Exception
        {

            downloadFile(inputFilesPath, outputFilesPath, fileName, totalItems, totalTransferredItems, httpHeaders);
        }
    }


    private class UploadFileRunnable extends TransferFileRunnable
    {


        private UploadFileRunnable(String                     inputFilesPath,
                                   String                     outputFilesPath,
                                   String                     fileName,
                                   int                        totalItems,
                                   AtomicInteger              totalTransferredItems,
                                   AtomicReference<Exception> atomicReference,
                                   ExecutorService            executorService,
                                   Map<String, String>        httpHeaders)
        {

            super(inputFilesPath,
                  outputFilesPath,
                  fileName,
                  totalItems,
                  totalTransferredItems,
                  atomicReference,
                  executorService,
                  httpHeaders);
        }


        @Override
        protected void transferFile(String              inputFilesPath,
                                    String              outputFilesPath,
                                    String              fileName,
                                    int                 totalItems,
                                    AtomicInteger       totalTransferredItems,
                                    Map<String, String> httpHeaders) throws Exception
        {

            uploadFile(inputFilesPath, outputFilesPath, fileName, totalItems, totalTransferredItems, httpHeaders);
        }
    }


    protected AbstractCloudManager(Context context)
    {

        mContext = context;
    }


    protected void setCallbacks(SessionCallback  sessionCallback,
                                DownloadCallback downloadCallback,
                                UploadCallback   uploadCallback)
    {

        mSessionCallback = sessionCallback;
        mDownloadCallback = downloadCallback;
        mUploadCallback = uploadCallback;
    }


    protected String getAccessToken()
    {
        String result;

        if ((result = CommonUtilities.getPreference(mContext, getAccessTokenTag())).length() == 0)
        {
            return null;
        }

        return result;
    }


    protected void setAccessToken(String accessToken)
    {

        CommonUtilities.setPreference(mContext, getAccessTokenTag(), accessToken);
    }


    protected void clearAccessToken()
    {

        CommonUtilities.unsetPreference(mContext, getAccessTokenTag());
    }


    protected void setLoggedIn(boolean loggedIn)
    {

        if (mSessionCallback == null)
        {
            return;
        }

        if (loggedIn)
        {
            mSessionCallback.onLoggedIn();
        }
        else
        {
            mSessionCallback.onLoggedOut();
        }
    }


    private void downloadFiles(String              inputFilesPath,
                               String[]            inputFilesExtensions,
                               String              outputFilesPath,
                               Handler             handler,
                               boolean             spawnedThread,
                               Map<String, String> httpHeaders) throws Exception
    {
        final AtomicReference<Exception> atomicReference;
        File file;
        List<String> fileNames;
        final AtomicInteger totalDownloadedItems;
        final ExecutorService executorService;

        atomicReference = new AtomicReference<Exception>();

        try
        {
            file = new File(outputFilesPath);

            if (!file.exists() &&
                !file.mkdirs())
            {
                throw new RuntimeException("Error accessing or creating directory (" + outputFilesPath + ")");
            }

            fileNames = getFileNamesList(inputFilesPath, inputFilesExtensions);

            mDownloadCallback.onDownloadStart(fileNames.size());

            if (fileNames.size() == 0)
            {
                mDownloadCallback.onDownloadStop();
                return;
            }

            totalDownloadedItems = new AtomicInteger(0);

            executorService = Executors.newFixedThreadPool(Math.min(fileNames.size(), getDownloadThreadPoolSize()));
            for (int i = 0;
                 i < fileNames.size();
                 i ++)
            {
                executorService.execute(new DownloadFileRunnable(inputFilesPath,
                                                                 outputFilesPath,
                                                                 fileNames.get(i),
                                                                 fileNames.size(),
                                                                 totalDownloadedItems,
                                                                 atomicReference,
                                                                 executorService,
                                                                 httpHeaders));
            }

            handler.post(new Runnable()
            {


                @Override
                public void run()
                {

                    executorService.shutdown();
                }
            });

            while (!executorService.isTerminated())
            {
            }

            if (atomicReference.get() == null)
            {
                mDownloadCallback.onDownloadStop();
            }
            else
            {
                if (spawnedThread)
                {
                    mDownloadCallback.onDownloadError(atomicReference.get());
                }
                else
                {
                    throw atomicReference.get();
                }
            }
        }

        catch (Exception exception)
        {
            Log.e(getClass().getSimpleName(), "Unexpected error " + exception);
            exception.printStackTrace();

            if (spawnedThread)
            {
                mDownloadCallback.onDownloadError(exception);
            }
            else
            {
                throw exception;
            }
        }
    }


    @Override
    public void download(final String              inputFilesPath,
                         final String[]            inputFilesExtensions,
                         final String              outputFilesPath,
                         final Handler             handler,
                         final boolean             spawnThread,
                         final Map<String, String> httpHeaders) throws Exception
    {
        Runnable runnable;
        Thread thread;

        if (mDownloadCallback == null)
        {
            return;
        }

        if (spawnThread)
        {
            runnable = new Runnable()
            {


                @Override
                public void run()
                {

                    try
                    {
                        downloadFiles(inputFilesPath, inputFilesExtensions, outputFilesPath, handler, spawnThread, httpHeaders);
                    }

                    catch (Exception exception)
                    {
                    }
                }
            };

            thread = new Thread(runnable);
            thread.start();
        }
        else
        {
            downloadFiles(inputFilesPath, inputFilesExtensions, outputFilesPath, handler, spawnThread, httpHeaders);
        }
    }


    private void uploadFiles(String              inputFilesPath,
                             String[]            inputFilesExtensions,
                             String              outputFilesPath,
                             Handler             handler,
                             boolean             spawnedThread,
                             Map<String, String> httpHeaders) throws Exception
    {
        final AtomicReference<Exception> atomicReference;
        File file;
        List<File> contents;
        final AtomicInteger totalUploadedItems;
        final ExecutorService executorService;

        atomicReference = new AtomicReference<Exception>();

        try
        {
            file = new File(inputFilesPath);

            if (!file.exists() &&
                !file.mkdirs())
            {
                throw new RuntimeException("Error accessing or creating directory (" + inputFilesPath + ")");
            }

            contents = new ArrayList<File>();

            for (File content : file.listFiles())
            {
                if ((inputFilesExtensions == null) ||
                    CommonUtilities.isFileKnown(content.getName(), inputFilesExtensions))
                {
                    contents.add(content);
                }
            }

            mUploadCallback.onUploadStart(contents.size());

            if (contents.size() == 0)
            {
                mUploadCallback.onUploadStop();
                return;
            }

            totalUploadedItems = new AtomicInteger(0);

            executorService = Executors.newFixedThreadPool(Math.min(contents.size(), getUploadThreadPoolSize()));
            for (int i = 0;
                 i < contents.size();
                 i ++)
            {
                executorService.execute(new UploadFileRunnable(inputFilesPath,
                                                               outputFilesPath,
                                                               contents.get(i).getName(),
                                                               contents.size(),
                                                               totalUploadedItems,
                                                               atomicReference,
                                                               executorService,
                                                               httpHeaders));
            }

            handler.post(new Runnable()
            {


                @Override
                public void run()
                {

                    executorService.shutdown();
                }
            });

            while (!executorService.isTerminated())
            {
            }

            if (atomicReference.get() == null)
            {
                mUploadCallback.onUploadStop();
            }
            else
            {
                if (spawnedThread)
                {
                    mUploadCallback.onUploadError(atomicReference.get());
                }
                else
                {
                    throw atomicReference.get();
                }
            }
        }

        catch (Exception exception)
        {
            Log.e(getClass().getSimpleName(), "Unexpected error " + exception);
            exception.printStackTrace();

            if (spawnedThread)
            {
                mUploadCallback.onUploadError(exception);
            }
            else
            {
                throw exception;
            }
        }
    }


    @Override
    public void upload(final String              inputFilesPath,
                       final String[]            inputFilesExtensions,
                       final String              outputFilesPath,
                       final Handler             handler,
                       final boolean             spawnThread,
                       final Map<String, String> httpHeaders) throws Exception
    {
        Runnable runnable;
        Thread thread;

        if (mUploadCallback == null)
        {
            return;
        }

        if (spawnThread)
        {
            runnable = new Runnable()
            {


                @Override
                public void run()
                {

                    try
                    {
                        uploadFiles(inputFilesPath, inputFilesExtensions, outputFilesPath, handler, spawnThread, httpHeaders);
                    }

                    catch (Exception exception)
                    {
                    }
                }
            };

            thread = new Thread(runnable);
            thread.start();
        }
        else
        {
            uploadFiles(inputFilesPath, inputFilesExtensions, outputFilesPath, handler, spawnThread, httpHeaders);
        }
    }


    protected abstract String getAccessTokenTag();
    protected abstract int getDownloadThreadPoolSize();
    protected abstract int getUploadThreadPoolSize();
    protected abstract List<String> getFileNamesList(String   inputFilesPath,
                                                     String[] inputFilesExtensions) throws Exception;
    protected abstract void downloadFile(String              inputFilesPath,
                                         String              outputFilesPath,
                                         String              fileName,
                                         int                 totalItems,
                                         AtomicInteger       totalTransferredItems,
                                         Map<String, String> httpHeaders) throws Exception;
    protected abstract void uploadFile(String              inputFilesPath,
                                       String              outputFilesPath,
                                       String              fileName,
                                       int                 totalItems,
                                       AtomicInteger       totalTransferredItems,
                                       Map<String, String> httpHeaders) throws Exception;
}