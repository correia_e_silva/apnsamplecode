package internal.cloudManager;


import android.content.Context;
import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


import common.CommonConstants;
import common.commonUtilities.CommonUtilities;
import common.messaging.CommonHttpClient;


public abstract class BaseCustomCloudManager extends AbstractCloudManager implements CommonConstants
{
    protected static Map<String, String> HTTP_HEADERS_SEND_MESSAGE = new HashMap<String, String>()
    {
        {
            put("Content-Type", "application/json;charset=utf-8");
            put("X-Requested-With", "XMLHttpRequest");
        }
    };

    protected static Map<String, String> HTTP_HEADERS_UPLOAD_FILE = new HashMap<String, String>()
    {
        {
            put("Connection", "Keep-Alive");
            put("Cache-Control", "no-cache");
            put("Content-Type", "multipart/form-data;HTTP_STRING_BOUNDARY=" + HTTP_STRING_BOUNDARY);
        }
    };


    private MessageCallback              mMessageCallback;


    protected BaseCustomCloudManager(Context context)
    {

        super(context);
    }


    @Override
    public void setCallbacks(SessionCallback  sessionCallback,
                             MessageCallback  messageCallback,
                             DeleteCallback   deleteCallback,
                             DownloadCallback downloadCallback,
                             UploadCallback   uploadCallback)
    {

        super.setCallbacks(sessionCallback, downloadCallback, uploadCallback);

        mMessageCallback = messageCallback;
    }


    private void signalMessageReceived(String message)
    {

        if (mMessageCallback != null)
        {
            mMessageCallback.onMessageReceived(message);
        }
    }


    private void signalMessageError(Exception exception)
    {

        if (mMessageCallback != null)
        {
            mMessageCallback.onMessageError(exception);
        }
    }


    @Override
    public Future<String> receiveMessage()
    {

        return Executors.newSingleThreadExecutor().submit(new Callable<String>()
        {


            @Override
            public String call()
            {
                String message;

                try
                {
                    message = CommonHttpClient.receiveMessage(CommonUtilities.getPreference(mContext, CLOUD_SERVER_ADDRESS_TAG));
                }

                catch (Exception exception)
                {
                    Log.e(getClass().getSimpleName(), "Unexpected error " + exception);
                    exception.printStackTrace();
                    signalMessageError(exception);
                    throw new RuntimeException("Error receiving message " + exception);
                }

                signalMessageReceived(message);

                return message;
            }
        });
    }


    @Override
    public Future<Boolean> sendMessage(final String message)
    {

        return Executors.newSingleThreadExecutor().submit(new Callable<Boolean>()
        {


            @Override
            public Boolean call()
            {

                try
                {
                    CommonHttpClient.sendMessage(CommonUtilities.getPreference(mContext, CLOUD_SERVER_ADDRESS_TAG), message, HTTP_HEADERS_SEND_MESSAGE);
                }

                catch (Exception exception)
                {
                    Log.e(getClass().getSimpleName(), "Unexpected error " + exception.toString());
                    exception.printStackTrace();
                    signalMessageError(exception);
                    return false;
                }

                return true;
            }
        });
    }


    @Override
    public Future<Boolean> delete(String fileName)
    {

        throw new UnsupportedOperationException();
    }
}